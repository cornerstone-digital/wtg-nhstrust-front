/*
 * Webpack development server configuration
 *
 * This file is set up for serving the webpack-dev-server, which will watch for changes and recompile as required if
 * the subfolder /webpack-dev-server/ is visited. Visiting the root will not automatically reload.
 */
var webpack = require('webpack');

var pjson = require('./package.json');

module.exports = {

  cache: true,

  debug: true,

  devtool: 'sourcemap',

  entry: {
    vendor: [
      'react',
      'react-router',
      'is-ie8'
    ],
    app: [
      'webpack/hot/only-dev-server',
      './src/js/app.js'
    ]
  },

  historyApiFallback: false,

  output: {
    filename: 'js/app.js',
    publicPath: '/assets/'
  },

  stats: {
    colors: true,
    reasons: true
  },

  resolve: {
    extensions: ['', '.js', '.jsx'],
    alias: {
      'styles': __dirname + '/src/assets/styles/',
      'mixins': __dirname + '/src/js/mixins/',
      'components': __dirname + '/src/js/components/',
      'pages': __dirname + '/src/js/components/pages',
      'common': __dirname + '/src/js/components/common',
      'stores': __dirname + '/src/js/stores/',
      'actions': __dirname + '/src/js/redux/modules/',
      'constants': __dirname + '/src/js/constants/',
      'reducers': __dirname + '/src/js/reducers/',
      'config': __dirname + '/src/js/config/',
      'utils': __dirname + '/src/js/components/common/utils/utils.js',
      'promise-js': __dirname + '/src/js/components/common/utils/promise-js-proxy.js'
    },
    root: 'nhstrust-front/src/main/nhstrust/src/js'
  },
  module: {
    preLoaders: [{
      test: /\.(js|jsx)$/,
      exclude: /node_modules/,
      loader: 'eslint-loader'
    },{
      test: /\.json$/,
      loader: "json-loader"
    }],
    loaders: [{
      test: /\.(js|jsx)$/,
      exclude: /node_modules/,
      loader: 'react-hot!babel-loader'
    }, {
      test: /\.(rb.js|rb.jsx)$/,
      loader: 'react-hot!babel-loader'
    }, {
      test: /\.scss/,
      loader: 'style-loader!css-loader!sass-loader?outputStyle=expanded'
    }, {
      test: /\.css$/,
      loader: 'style-loader!css-loader'
    }, {
      test: /\.(png|jpg|woff|woff2|eot|ttf|svg)$/,
      loader: 'url-loader?limit=8192'
    }]
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.optimize.CommonsChunkPlugin('vendor', 'js/vendors.js', Infinity),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('development'),
        'Version': JSON.stringify(pjson.version)
      }
    })
  ]

};
