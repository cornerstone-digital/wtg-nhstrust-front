
let nuxeoConfig = {};

/**
 * Production config for NHS trust Intranet
 */
if (process.env.NODE_ENV === 'production') {

  //Set our host origin in IE browsers
  if (!window.location.origin) {
    window.location.origin = window.location.protocol + '//' + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
  }

  nuxeoConfig = {
    baseURL: `${window.location.origin}/nuxeo`,
    restPath: '/site/api/v1/',
    automationPath: 'site/api/v1/automation/',
    elasticSearchPath: '/site/es',
  };

}

/**
 * Dev config for NHS trust Intranet
 *
 * Available Users:
 *   Administrator
 *   Executive
 *   Assurer
 *   Policylead
 *   Truststaff
 *
 * Password
 *   Qazxsw21!
 *
 */
if (process.env.NODE_ENV === 'development') {
  nuxeoConfig = {
    // baseURL: 'http://localhost:8080/nuxeo',
    baseURL: 'http://10.100.8.177:18080/nuxeo',
    // baseURL: 'http://10.100.9.94:18080/nuxeo',
    restPath: '/site/api/v1/',
    automationPath: 'site/api/v1/automation/',
    elasticSearchPath: '/site/es',
    auth: {
      method: 'basic',
      // get the current username from API=>User.Get, to make switching users easier during development
      // username: 'Truststaff',
      password: 'Qazxsw21!'
    }
  };
}

export default nuxeoConfig;

export const nuxeoDomain = 'Intranet';

export const configVars = {
  MyPermissions_OLD: [{
    groups: ['policyleads', 'policyadmins'],
    commentType: 'OriginatorComment'
  }, {
    groups: ['assurance'],
    commentType: 'AssuranceComment'
  }, {
    groups: ['emt'],
    commentType: 'EMTComment'
  }, {
    groups: ['approvers'],
    commentType: 'ApproverComment'
  }],
  CommentPermissions: [{
    taskName: ['EMT Review', 'Review Scope Change'],
    commentType: 'EMTComment'
  }, {
    taskName: ['Revise policy', 'Revise scope', 'Update claim', 'Resubmit', 'Publish'],
    commentType: 'OriginatorComment'
  }, {
    taskName: ['Approve Claim'],
    commentType: 'ApproverComment'
  }, {
    taskName: ['Assurance Review'],
    commentType: 'AssuranceComment'
  }],
  WorkflowFileRelation: [{
    name: 'ClaimApproval',
    docTypes: ['File']
  }, {
    name: 'PolicyReviewApproval',
    docTypes: ['TrustPolicy']
  }, {
    name: 'SerialDocumentReview',
    docTypes: ['File', 'Audio', 'Video', 'Note']
  }, {
    name: 'ClaimApprovalFinance',
    docTypes: ['TrustFinanceClaim', 'TrustExpensesClaim']
  }, {
    name: 'ScopeApprovalEMT',
    docTypes: ['TrustScopingForm']
  }, {
    name: 'ParallelDocumentReview',
    docTypes: ['File']
  }]

};

export const allowedDocTypes = [{
  id: 'File',
  name: 'File'
}, {
  id: 'Folder',
  name: 'Folder'
}, {
  id: 'Workspace',
  name: 'Workspace'
}, {
  id: 'PolicyWorkspace',
  name: 'Policy Workspace'
}, {
  id: 'Note',
  name: 'Note'
}, {
  id: 'TrustExpensesClaim',
  name: 'Trust Expenses Claim'
}, {
  id: 'TrustPolicy',
  name: 'Trust Policy'
}, {
  id: 'TrustScopingForm',
  name: 'Trust Scoping Form'
}, {
  id: 'Picture',
  name: 'Picture'
}, {
  id: 'Video',
  name: 'Video'
}, {
  id: 'Audio',
  name: 'Audio'
}];

export const fileTypes = `.JPG,.PNG,.GIF,.RAW,.JPG2000,.TIF,.BMP,.PSD,.AI,.EPS,.PS,.PCX,.PICT,.SVG,
.WMF,.EMF,.QXD,.INDD,.ogg,.avi,.mp4,.flv,.fla,.QT,.mov,.mpeg,.mpg,.mpe,.WMV,.swf,.ogg,.mp3,.wav,.m4a,.aac,
.ac3,.aif,.aiff,.aifc,.au,.snd,.asf,.pdf,.DOC,.DOCX,.DOT,.DOCM,.DOTX,.DOTM,.DOCB,.XLS,.XLT,.XLM,.XLSX,.XLSM,
.XLL,.XLW,.PPT,.POT,.PPS,.PPTX,.PPTM,.POTX,.POTM,.PPAM,.PPSX,.PPSM,.SLDX,.SLDM,.ODT,.ODS,.ODP,.RTF,.MDB,
.ACCDB,.ACCDE,.ACCDT,.ACCDR,.PUB,.TXT,.HTML,.MSG`;

export const allowedMediaTypes = {
  file: fileTypes,
  picture: '.JPG,.JPEG,.PNG,.GIF',
  video: '.avi,.mp4',
  audio: '.mp3,.wav'
};
