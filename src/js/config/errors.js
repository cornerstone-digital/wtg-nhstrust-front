let ErrorConfig = {};

switch (process.env.NODE_ENV) {
  case 'production':
    ErrorConfig = {
      sentry: {
        key: 'f62347b07eb64a81a13d439d94033247',
        id: 64976
      },
      raygun: {
        apiKey: 'tjwL1CttFHRkPVlZ4Uc4eA=='
      }
    };
    break;
  case 'staging':
    ErrorConfig = {
      sentry: {
        key: '',
        id: 0
      },
      raygun: {
        apiKey: 'uMIc5GdawbDk/Bk4F0fq2g=='
      }
    };
    break;
  case 'uat':
    ErrorConfig = {
      sentry: {
        key: '6fea9fa488414d77b9eec0fdebbd2f75',
        id: 64974
      },
      raygun: {
        apiKey: '1idemlb/2TkWRp3bNX0+kw=='
      }
    };
    break;
  default: // development
    ErrorConfig = {
      sentry: {
        key: '9247bfe49fe249889581bf80446c357d',
        id: 64937
      },
      raygun: {
        apiKey: '83J+5fP0aZDbh1CzJAWy+A=='
      }
    };
    break;
}

export default ErrorConfig;
