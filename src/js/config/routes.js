import React from 'react';
import { IndexRoute, Route, Redirect } from 'react-router';

import MainContainer from 'pages/mainContainer/mainContainer';
import DashboardPage from 'pages/dashboardPage/dashboardPage';
import InfoPage from 'pages/infoPage/infoPage';

import DocManagementPage from 'pages/docManagementPage/docManagementPage';
import DocListingPage from 'pages/docListingPage/docListingPage';
import DocCreatePage from 'pages/docCreatePage/docCreatePage';

import YourAppsPage from 'pages/yourAppsPage/yourappsPage';
import YourTasksPage from 'pages/yourTasksPage/yourtasksPage';
import ProfilePage from 'pages/profilePage/profilePage';

import RoomBookerApp from 'nhstrustRoombooker/src/components/Main.rb';
import RoomDetails from 'nhstrustRoombooker/src/components/room-details.rb';
import BookingDetails from 'nhstrustRoombooker/src/components/booking-details.rb';
import BookingReport from 'nhstrustRoombooker/src/components/booking-report.rb';
import Admin from 'nhstrustRoombooker/src/components/admin/admin.rb';
import AdminRooms from 'nhstrustRoombooker/src/components/admin/admin-rooms.rb';
import ManageRoom from 'nhstrustRoombooker/src/components/admin/manage-room.rb';

import DocContainer from 'pages/docContainer/docContainer';
import DocDetail from 'pages/docContainer/docDetail/docDetail';
import DocEdit from 'pages/docContainer/docEdit/docEdit';
import DocHistory from 'pages/docContainer/docHistory/docHistory';

import DocPath from 'pages/docNavigation/docPath';
import DocSearch from 'pages/docNavigation/docSearch';
import DocFavourites from 'pages/docNavigation/docFavourites';

const routes = (
  <Route>
    <Redirect from="/" to="/dashboard"/>
    <Route path="/" component={MainContainer} >

      <Route parent="profile" path="profile" component={ProfilePage} />

      <Route parent="dashboard" path="dashboard" component={DashboardPage} />
      <Route parent="dashboard" path="yourapps" component={YourAppsPage} />
      <Route parent="dashboard" path="info/:docId" component={InfoPage} />

      <Route parent="yourtasks" path="yourtasks" component={YourTasksPage} />

      <Route parent="documentmanagement" path="documentmanagement" component={DocManagementPage} />
      {
      // <Route parent="documentmanagement" path="doc" component={DocContainer}>
      //   <Route path="/doc/:docId" component={DocDetail} />
      //   <Route path="/doc/:docId/edit" breadCrumbTitle="Edit" component={DocEdit} />
      //   <Route path="/doc/:docId/history" breadCrumbTitle="History" component={DocHistory} />
      // </Route>
      }
      <Route parent="documentmanagement" path="new/:docType/:parentId" component={DocCreatePage} />
      {
      // <Route parent="documentmanagement" path="workspace/:workspaceId" component={DocListingPage} />
      // <Route parent="documentmanagement" path="folder/:workspaceId" component={DocListingPage} />
      // <Route parent="documentmanagement" path="section/:workspaceId" component={DocListingPage} />
      // <Route parent="policies" path="policies/:workspaceId" component={DocListingPage} />
      }

      <Redirect from="yourFavorite" to="favourites?folderish=false" />
      <Redirect from="yourPinnedWorkspaces" to="favourites?folderish=true" />
      <Redirect from="yourdocuments" to="search" />
      <Redirect from="doc/:id/history" to="path/:id|history"/>
      <Redirect from="doc/:id/edit" to="path/:id|edit"/>
      <Redirect from="doc/:id" to="path/:id"/>
      <Redirect from="workspace/:id" to="path/:id"/>
      <Redirect from="folder/:id" to="path/:id"/>
      <Redirect from="section/:id" to="path/:id"/>
      <Redirect from="policies/:id" to="path/:id"/>

      <Redirect from="path" to="path/Intranet"/>
      <Route parent="documentmanagement" path="path" component={DocPath}>
        <Route parent="documentmanagement" path="" components={{ list: DocListingPage, single: DocContainer }}>
          <Route path="**|edit" breadCrumbTitle="Edit" component={DocEdit} />
          <Route path="**|history" breadCrumbTitle="History" component={DocHistory} />
          <Route path="**" component={DocDetail} />
        </Route>
      </Route>
      <Route path="search" component={DocSearch}>
        <IndexRoute parent="documentmanagement" components={{ list: DocListingPage }} />
      </Route>
      <Route path="favourites" component={DocFavourites}>
        <IndexRoute parent="dashboard" components={{ list: DocListingPage }} />
      </Route>

      <Redirect from="/roombooker/roomdetails" to="/roombooker/cal" />
      <Route parent="roombooker" path="/roombooker/cal" component={RoomBookerApp} />
      <Route parent="roombooker" path="/roombooker/cal/:viewType" component={RoomBookerApp} />
      <Route parent="roombooker" path="/roombooker/cal/:viewType/:locationName" component={RoomBookerApp} />
      <Route parent="roombooker" path="/roombooker/cal/:viewType/:locationName/:siteName" component={RoomBookerApp} />
      <Route parent="roombooker" path="/roombooker/roomdetails/:roomId" component={RoomDetails}/>
      <Route parent="roombooker" path="/roombooker/roomdetails/:roomId/:locationName/:siteName" component={RoomDetails}/>
      <Route parent="roombooker" path="/roombooker/bookingdetails/:bookingId" component={BookingDetails}/>
      <Route parent="roombooker" path="/roombooker/bookingdetails/:bookingId/:locationName/:siteName" component={BookingDetails}/>
      <Route parent="roombooker" path="/roombooker/bookingreport" component={BookingReport}/>
      <Route parent="roombooker" path="/roombooker/admin" component={Admin}/>
      <Route parent="roombooker" path="/roombooker/admin/rooms" component={AdminRooms}/>
      <Route parent="roombooker" path="/roombooker/admin/manageroom" component={ManageRoom}/>
      <Route parent="roombooker" path="/roombooker/admin/manageroom/:roomId" component={ManageRoom}/>
    </Route>
  </Route>
  );

export default routes;
