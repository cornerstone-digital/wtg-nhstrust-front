/*eslint-disable */
import React, {PropTypes} from 'react';

const TreeView = React.createClass({

  propTypes: {
    collapsed: PropTypes.bool,
    defaultCollapsed: PropTypes.bool,
    nodeLabel: PropTypes.node.isRequired,
    onClick: PropTypes.func,
    className: PropTypes.string,
    children: PropTypes.array,
    collapsedIcon: PropTypes.object,
    expandedIcon: PropTypes.object,
    hasChildren: PropTypes.bool
  },

  getInitialState() {
    return {collapsed: this.props.defaultCollapsed};
  },

  handleClick(...args) {
    this.setState({collapsed: !this.state.collapsed});
    if (this.props.onClick) {
      this.props.onClick(...args);
    }
  },

  render() {
    const {
      collapsed = this.state.collapsed,
      className = '',
      nodeLabel,
      collapsedIcon,
      expandedIcon,
      hasChildren,
      children,
      ...rest,
    } = this.props;

    let containerClassName = 'tree-view_children';
    let icon = expandedIcon;

    if (collapsed) {
      containerClassName += ' tree-view_children-collapsed';
      icon = collapsedIcon;
    }

    const iconWrap = (
      <div
        {...rest}
        className={className + ' icon'}
        onClick={this.handleClick}>
        {icon}
      </div>
      );


    return (
      <div className="tree-view">
        <div className="tree-view_item ">
          {iconWrap}
          {nodeLabel}
        </div>
        <div className={containerClassName}>
          {children}
        </div>
      </div>
    );
  },
});

export default TreeView;
/*eslint-enable */
