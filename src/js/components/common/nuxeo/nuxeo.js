import VendorNuxeo from 'nuxeo';
import nuxeoConfig, { nuxeoDomain } from 'config/nuxeo';
import jQuery from 'jquery';
import elasticsearch from 'elasticsearch/src/elasticsearch';
import utils from 'common/utils/utils';

class Nuxeo {
  defaultEnrichers = [
    'acls',
    'children',
    'publishDocEnricher',
    'breadcrumb',
    'permissions',
    'allDocumentsWorkflow',
    'getDocumentTags',
    'allowedDocumentTypes'
  ]


  constructor() {
    this.client = new VendorNuxeo(nuxeoConfig);
  }

  get userId() {
    return this.uid;
  }

  set userId(value) {
    this.uid = value;
  }

  /**
   * Fetches all information relating to the
   * logged in user and sets the userId
   * @return {Promise} user information
   */
  init() {
    return this.client
    .operation('User.Get')
    .execute()
    .then((userData) => {
      //If user is anomynous log out
      if (userData.uid === 'Guest') {
        return this.logout();
      }

      this.userId = userData.uid;

      this.client._auth.username = userData.uid;

      const esUrl = document.createElement('a');
      esUrl.href = nuxeoConfig.baseURL.replace(/\/?$/, '/') + nuxeoConfig.elasticSearchPath.replace(/^\/?/, '');

      this.esClient = new elasticsearch.Client({
        hosts: {
          protocol: esUrl.protocol,
          host: esUrl.hostname,
          port: esUrl.port,
          path: esUrl.pathname,
          headers: {
            Authorization: 'Basic ' + btoa(this.client._auth.username + ':' + this.client._auth.password)
          }
        }
      });

      return this.client
        .request(`user/${userData.uid}`)
        .execute();
    });
  }

  logout() {
    window.location.href = `${nuxeoConfig.baseURL}/login.jsp`;
  }

  lockDocument(user, path) {
    return this.client
      .header('X-NXfetch.document', 'lock')
      .operation('Document.Lock')
      .input(`doc:${path}`)
      .params({
        owner: user
      })
      .execute();
  }

  unlockDocument(path) {
    return this.client
      .operation('Document.Unlock')
      .input(`doc:${path}`)
      .execute();
  }

  getTasksWaiting() {
    return this.client
      .operation('Workflow.UserTaskPageProvider')
      .params({
        pageSize: 1000
      })
      .execute();
  }

  getOpenTasks(path) {
    return this.client
      .operation('Workflow.GetOpenTasks')
      .input(`doc:${path}`)
      .execute();
  }

  startWorkflow(path, workflowId) {
    return this.client
      .operation('Context.StartWorkflow')
      .params({
        id: workflowId,
        start: true
      })
      .input(`doc:${path}`)
      .execute();
  }

  cancelWorkflow(workflowId) {
    return this.client
      .operation('WorkflowInstance.Cancel')
      .params({
        id: workflowId
      })
      .execute();
  }

  getFavorites() {
    return this.client
      .operation('Favorite.GetDocuments')
      .input(`/${nuxeoDomain}/UserWorkspaces/${this.userId}`)
      .execute();
  }

  addToFavorites(docId) {
    return this.client
      .operation('Document.AddToCollection')
      .input(`${docId}`)
      .params({
        collection: `/${nuxeoDomain}/UserWorkspaces/${this.userId}/Favorites`
      })
      .execute();
  }

  getPinnedStatus(docId) {
    return this.client
      .operation('Favorite.GetDocuments')
      .input(`/${nuxeoDomain}/UserWorkspaces/${this.userId}`)
      .execute()
      .then(res => {
        const isFavorite = res.entries.find((item) => {
          return item.uid === docId;
        });

        return !!isFavorite;
      });
  }

  removeFromFavorites(docId) {
    return this.client
      .operation('Collection.RemoveFromCollection')
      .input(`${docId}`)
      .params({
        collection: `/${nuxeoDomain}/UserWorkspaces/${this.userId}/Favorites`
      })
      .execute();
  }


  deletePermission(pathId, username) {
    return this.client
      .header('X-NXenrichers.document', 'acls')
      .operation('Document.RemovePermission')
      .input(`doc:${pathId}`)
      .params({
        user: username
      })
      .execute();
  }

  addMultiplePermissions(pathId, users, permissionType) {
    return users.reduce((chain, user) => {
      return chain.then(responses => {
        return this.client
          .header('X-NXenrichers.document', 'acls')
          .operation('Document.AddPermission')
          .input(`doc:${pathId}`)
          .params({
            username: user,
            permission: permissionType
          })
          .execute()
          .then(response => {
            return responses.concat([response]);
          });
      });
    }, Promise.resolve([]));
  }

  getSectionByPath(docPath) {
    return this.client
      .header('X-NXenrichers.document', 'acls')
      .header('X-NXproperties', '*')
      .request(`/path/${docPath}/`)
      .execute();
  }

  getSectionChildren(docId) {
    return new Promise((resolve) => {
      this.client
        .operation('Document.GetChildren')
        .input(`doc:${docId}`)
        .execute()
        .then((res) => {
          resolve(res);
        })
        .catch(() => {
          resolve({});
        });
    });
  }

  getDocVersions(docId) {
    return this.client
      .operation('Document.GetVersions')
      .input(`doc:${docId}`)
      .execute();
  }

  publishDoc(docId, sectionId, override) {
    return this.client
      .header('X-NXenrichers.document', 'acls, publishDocEnricher')
      .operation('Document.PublishToSection')
      .input(`doc:${docId}`)
      .params({
        target: sectionId,
        override: override
      })
      .execute();
  }

  deletePublishedDocument(docPath) {
    return new Promise((resolve) => {
      this.client
        .operation('Document.Delete')
        .input(`doc:${docPath}`)
        .execute()
        .then(() => {
          resolve(true);
        })
        .catch(() => {
          resolve([]);
        });

    });
  }

  deleteDocument(path) {
    return this.deleteDocuments([path])
    .then(responses => responses[0]);
  }

  deleteDocuments(paths) {
    return paths.reduce((chain, path) => {
      return chain.then(responses => {
        return this.client
          .operation('Document.Delete')
          .input(`doc:${path}`)
          .execute()
          .then(response => {
            return responses.concat([response]);
          });
      });
    }, Promise.resolve([]));
  }

  getTasksByDocId(docId) {
    return this.client
      .request(`id/${docId}/@task`)
      .execute();
  }

  actionTask(taskId, action, body) {
    return this.client
      .request(`task/${taskId}/${action}`)
      .put({
        body: body
      });
  }

  getDocumentByUrlPath(dirtyPath) {
    const path = (dirtyPath ? dirtyPath : '');
      // todo: simplify routes by removing /Intranet without breaking top directory route
      // .replace(/^\/?/, '/Intranet/'); // ensure leading /Intranet/

    return this.getDocumentByPath(path);
  }

  getDocumentByPath(dirtyPath) {
    const path = encodeURIComponent((dirtyPath ? dirtyPath : '')
      .replace(/^\/?/, '/') // ensure leading slash
      .replace(/\/?$/, '')) // remove trailing slash
      .replace(/%2F/gi, '/');

    return this.client
      .header('X-NXenrichers.document', this.defaultEnrichers.join(','))
      .header('X-NXfetch.document', 'lock')
      .header('X-NXproperties', '*')
      .request(`/path/${path}`)
      .execute();
  }

  search(options = {}) {
    const query = {
      query: {
        bool: {
          must: [
            {
              prefix: {
                'ecm:path': '/Intranet/'
              }
            }
          ],
          must_not: [
            {
              term: {
                'ecm:currentLifeCycleState': 'deleted'
              }
            },
            {
              term: {
                'ecm:mixinType': 'HiddenInNavigation'
              }
            },
            {
              terms: {
                'ecm:primaryType': ['Favorites', 'DocumentRoute', 'Section']
              }
            }
          ]
        }
      }
    };

    if (options.path) {
      const path = options.path.replace(/^\/?/, '/').replace(/\/?$/, '/');

      if (options.deep) {
        query.query.bool.must.push({
          prefix: {
            'ecm:path': path
          }
        });
      } else {
        query.query.bool.must.push({
          regexp: {
            'ecm:path': `${path}[^/]*`
          }
        });
      }
    }

    if (options.collection) {
      query.query.bool.must.push({
        term: {
          'collectionMember:collectionIds': options.collection
        }
      });
    }

    if (options.search && options.search.length > 0) {
      if (utils.isUuid(options.search)) {
        query.query.bool.must.push({
          term: {
            _id: {
              value: options.search,
            }
          }
        });
      } else {
        query.query.bool.must.push({
          bool: {
            should: [
              {
                common: {
                  'dc:title.fulltext': {
                    query: options.search,
                    low_freq_operator: 'and',
                    high_freq_operator: 'or',
                    cutoff_frequency: 0.001,
                    boost: 1.5
                    // minimum_should_match: {
                    //   low_freq : 2,
                    //   high_freq : 3
                    // }
                  }
                }
              },
              {
                common: {
                  _all: {
                    query: options.search,
                    low_freq_operator: 'and',
                    high_freq_operator: 'or',
                    cutoff_frequency: 0.001,
                    // minimum_should_match: {
                    //   low_freq : 2,
                    //   high_freq : 3
                    // }
                  }
                }
              }
            ]
          }
        });
      }

      query.highlight = {
        pre_tags: [
          '<strong>'
        ],
        post_tags: [
          '</strong>'
        ],
        fields: {
          'dc:title.fulltext': {
            index_options: 'offsets'
          },
          'ecm:binarytext': {
            fragment_size: 250,
            number_of_fragments: 1,
            index_options: 'offsets'
          }
        }
      };
    }

    if (options.types && options.types.length > 0) {
      query.query.bool.must.push({
        bool: {
          should: options.types.map(type => {
            return {
              term: {
                'ecm:primaryType': type
              }
            };
          })
        }
      });
    }

    if (utils.typeOf(options.folderish) === 'boolean') {
      const section = options.folderish ? 'must' : 'must_not';
      query.query.bool[section].push({
        term: {
          'ecm:mixinType': 'Folderish'
        }
      });
    }

    if (options.orderByAttribute) {
      query.sort = [
        {
          [options.orderByAttribute]: (options.orderByDirection || 'asc').toLowerCase()
        }
      ];
    }

    return this.elasticSearch(query);
  }

  getAllDocuments() {
    return this.search();
  }

  ElasticSearchResponseError = ElasticSearchResponseError

  elasticSearch(options) {
    const body = {
      from: 0,
      size: 10000,
      fields: ['_id', '_source'],
      ...options
    };

    return this.esClient.search({
      index: 'nuxeo',
      body: body
    })
    .then(response => {
      // elasticsearch errors still have an HTTP status code of 200,
      // but the body of the response will contain a status attribute
      if (typeof(response.hits) !== 'object') {
        const error = new ElasticSearchResponseError();
        error.status = response.status;
        error.message = response.error;
        error.response = response;
        throw error;
      }

      return response.hits;
    });
  }

  getSectionIds() {
    const query = `SELECT * FROM Document
    WHERE ecm:path = '/${nuxeoDomain}/sections/Intranet Site/Policies/'
    OR ecm:path = '/${nuxeoDomain}/sections/Intranet Site/Forms/'
    ORDER BY ecm:primaryType`;

    return this.client
      .request(`query?query=${query}`)
      .execute();
  }

  getAllApps() {
    const query = `SELECT * FROM Document
    WHERE ecm:path STARTSWITH '/${nuxeoDomain}/sections/Intranet Site/Apps/'
    AND ecm:primaryType = 'Application'`;

    return this.client
      .header('X-NXproperties', '*')
      .request(`query?query=${query}`)
      .execute();
  }

  getDocumentsById(docIds) {
    if ((docIds || []).length < 1) {
      return Promise.resolve({});
    }

    const query = `SELECT * FROM Document
    WHERE ecm:uuid IN ( '${docIds.join(`', '`)}' )`;

    return this.client
      .header('X-NXenrichers.document', 'acls, children, permissions')
      .header('X-NXproperties', '*')

      .request(`query?query=${query}`)
      .execute();
  }

  getLastModified() {
    return this.search({
      orderByAttribute: 'dc:modified',
      orderByDirection: 'desc'
    });
  }

  getUserProfile() {
    return this.client
      .header('X-NXenrichers.document', 'thumbnail')
      .header('X-NXproperties', '*')
      .request(`user/${this.userId}`)
      .execute();
  }

  getDomainFileTypes() {
    return this.client
        .header('X-NXproperties', '*')
        .request(`/path/default-domain`)
        .execute();
  }

  getDocType() {
    const query = `SELECT * FROM Document
    WHERE ecm:path STARTSWITH '/${nuxeoDomain}/'
    AND ecm:currentLifeCycleState != 'deleted'
    AND ecm:mixinType != 'NotCollectionMember'
    AND ecm:mixinType != 'HiddenInNavigation'
    AND ecm:primaryType != 'Workspace'
    AND ecm:primaryType != 'Favorites' ORDER BY dc:modified DESC`;

    return this.client
      .header('X-NXenrichers.document', 'breadcrumb')
      .header('X-NXproperties', '*')
      .request(`query?query=${query}&maxResults=1`)
      .execute();
  }

  getDocumentInSectionByDate(createdDate) {
    const query = `SELECT * FROM Document
    WHERE ecm:path STARTSWITH '/${nuxeoDomain}/sections/'
    AND dc:created = '${createdDate}'
    OR ecm:path STARTSWITH '/default-domain/sections/'
    AND dc:created = '${createdDate}'`;

    return this.client
      .header('X-NXenrichers.document', 'acls')
      .header('X-NXproperties', '*')
      .request(`query?query=${query}`)
      .execute();
  }


  getBreadcrumbs(docId) {
    return this.client
      .header('X-NXenrichers.document', 'breadcrumb')
      .request(`/id/${docId}/`)
      .execute()
      .then(res => {
        return res.contextParameters.breadcrumb.entries;
      });
  }

  /**If any modifications exist for document, find changes
   *
   * @param  {object} docId
   * @return {[array]} arrayDocumentHistory
   */
  getDocumentHistory(docId) {
    return this.client
      .header('X-NXenrichers.document', 'acls, publishDocEnricher, breadcrumb')
      .header('X-NXfetch.document', 'lock')
      .header('X-NXproperties', '*')
      .request(`/id/${docId}/@audit`)
      .execute();
  }

  getDocumentRelations(docId) {
    const query = `SELECT * FROM Relation
    WHERE ecm:primaryType = 'DefaultRelation'
    AND relation:source = '${docId}' OR relation:target = '${docId}'`;

    return this.client
      .request(`query?query=${query}`)
      .execute();
  }

  getDocumentTags(docId) {
    // SELECT * FROM Relation WHERE ecm:primaryType = 'Tagging' AND relation:source = '823db98a-6283-4c52-9a62-0bb737f21776'
    const query = `SELECT * FROM Relation
    WHERE ecm:primaryType = 'Tagging'
    AND relation:source = '${docId}'`;

    return this.client
      .request(`query?query=${query}`)
      .execute();
  }

  getTagSuggestions(search) {
    return jQuery.ajax(`${this.client._baseURL}/site/automation/Tag.Suggestion`, {
      method: 'POST',
      data: JSON.stringify({
        params: {
          searchTerm: search
        }
      }),
      headers: {
        Authorization: 'Basic ' + btoa(this.client._auth.username + ':' + this.client._auth.password),
      },
      processData: false,
      contentType: 'application/json+nxrequest'
    });
  }

  addDocumentTags(docId, tags) {
    return this.toggleDocumentTags(docId, tags, true);
  }

  removeDocumentTags(docId, tags) {
    return this.toggleDocumentTags(docId, tags, false);
  }

  toggleDocumentTags(docId, tags, toggle) {
    const endpoint = toggle ? 'Services.TagDocument' : 'Services.UntagDocument';

    return jQuery.ajax(`${this.client._baseURL}/site/automation/${endpoint}`, {
      method: 'POST',
      data: JSON.stringify({
        input: docId,
        params: {
          tags: tags
        }
      }),
      headers: {
        Authorization: 'Basic ' + btoa(this.client._auth.username + ':' + this.client._auth.password),
      },
      processData: false,
      contentType: 'application/json+nxrequest'
    });
  }

  getDocumentById(docId) {
    return this.client
      .header('X-NXenrichers.document', this.defaultEnrichers.join(','))
      .header('X-NXfetch.document', 'lock')
      .header('X-NXproperties', '*')
      .request(`/id/${docId}/`)
      .execute();
  }

  getWorkflowModels() {
    return this.client
      .request(`/workflowModel`)
      .execute();
  }

  getSchemas(docType) {
    return this.client
      .header('X-NXproperties', '*')
      .request(`/config/types/${docType}`)
      .execute();
  }


  getRunningWorkflowsOnDoc(docId) {
    return this.client
      .request(`/id/${docId}/@workflow`)
      .execute();
  }

  getAllRunningWorkflowsOnDoc(docId) {
    return this.client
      .header('X-NXenrichers.document', 'allDocumentsWorkflow')
      .request(`/id/${docId}`)
      .execute();
  }

  getBlob(docPath) {
    return this.client
      .request(`/path/${docPath}/@blob/file:files`)
      .execute();
  }

  /**
   * Generates a batch ID we use for uploading to nuxeo
   * @return {object} promise that returns generated batchId
   */
  generateBatch() {
    return this.client
      .request(`/upload`)
      .post({})
      .then(batch => {
        return batch.batchId;
      });
  }

  /**
   * Generates a file batch reference which we append to the document
   * @param  {Object} fileData File Object captured from a file input
   * @return {Promise}         Batch JSON on completion
   */
  uploadFiles(options) {

    const formData = new FormData();
    formData.append('file', options.fileData);

    return new Promise((resolve) => {
      jQuery.ajax(`${this.client._baseURL}/api/v1/upload/${options.batchId}/${options.index}`, {
        method: 'POST',
        data: formData,
        headers: {
          Authorization: 'Basic ' + btoa(this.client._auth.username + ':' + this.client._auth.password),
          'X-Batch-Id': options.batchId,
          'X-File-Idx': options.index,
          'X-File-Name': options.fileData.name
        },
        processData: false,
        contentType: false,
        xhr: handleUploadProgress
      })
        .then(() => {
          this
            .getBatch(options.batchId)
            .then(res => resolve(res));
        });
    });

    /**
     *  Callback for AJAX Progress on file
     */
    function handleUploadProgress() {
      const xhr = new window.XMLHttpRequest();
      xhr.upload.addEventListener('progress', (evt) => {
        if (evt.lengthComputable) {
          options.onProgress({
            upload: evt.loaded / evt.total
          });
        }
      }, false);
      xhr.addEventListener('progress', () => {
      }, false);
      return xhr;
    }
  }

  getBatch(batchId) {

    return new Promise((resolve) => {
      this.client
        .request(`/upload/${batchId}`)
        .execute()
        .then((batchFiles) => resolve(formatBatch(batchFiles)));
    });

    function formatBatch(batchFiles) {
      return batchFiles.map((file, index) => {
        return {
          'file': {
            'upload-batch': batchId,
            'upload-fileId': index.toString()
          },
          'filename': file.name
        };
      });
    }
  }

  createDocument(opt) {
    const doc = {
      'entity-type': 'document',
      'type': opt.type,
      'name': opt.properties['dc:title'],
      'properties': {
        ...opt.properties,

        // If the versionType exists we need to remove it from a docs properties
        // or it will break nuxeo
        versionType: undefined
      }
    };

    return this.client.repository()
      .create(opt.path, doc);
  }

  getWorkspaceTemplates() {
    return this.client
      .header('X-NXproperties', '*')
      .operation('Document.GetChildren')
      .input(`doc:/Intranet/Templates`)
      .execute();
  }

  /**
   * Creates a set of documents based on a template
   * Only applies to create workspace types
   */
  createDocumentFromTemplate(opt) {

    // Remove redundant doc properties before saving to nuxeo
    const docTemplate = opt.properties.createFromTemplate;
    delete opt.properties.createFromTemplate;

    return this.client
      .header('X-NXproperties', '*')
      .operation('Document.Copy')
      .input(docTemplate)
      .params({
        target: opt.path
      })
      .execute()
      .then(copiedDoc => {
        return this.updateDocument(copiedDoc, opt);
      });
  }

  updateDocument(doc, newVals) {
    const versionType = doc.properties.versionType;
    const cleanDoc = {
      ...doc,
      ...newVals,
      properties: {
        ...doc.properties,
        ...newVals.properties,
        versionType: undefined
      }
    };

    return this.client
      .request(`/id/${doc.uid}/`)
      .put({
        body: cleanDoc
      })
      .then((updatedDoc) => {
        if (versionType && versionType !== 'skip') {
          return this.client
            .operation('Document.CreateVersion')
            .input(`doc:${updatedDoc.uid}`)
            .params({
              increment: versionType
            })
            .execute();
        }

        return updatedDoc;
      });
  }

  getLayout(docType) {

    return new Promise((resolve) => {
      jQuery
        .ajax(`${this.client._baseURL}/site/layout-manager/layouts/json?layoutName=${docType}`, {
          method: 'GET',
          dataType: 'JSON',
          headers: {
            Authorization: 'Basic ' + btoa(this.client._auth.username + ':' + this.client._auth.password)
          },
          error: () => {
            resolve({
              widgets: []
            });
          },
          success: response => resolve(response)
        });
    });
  }


  getFileTasksLayout() {
    return new Promise((resolve) => {
      jQuery
        .ajax(`${this.client._baseURL}/site/layout-manager/layouts/json?layoutName=Task255@taskLayout`, {
          method: 'GET',
          dataType: 'JSON',
          headers: {
            Authorization: 'Basic ' + btoa(this.client._auth.username + ':' + this.client._auth.password)
          },
          error: () => {
            resolve({
              taskLayout: []
            });
          },
          success: response => resolve(response)
        });
    });
  }

  getHelp() {
    return new Promise((resolve) => {
      resolve([
        'How do I edit my profile?',
        'How do I book a room?',
        'How do I upload documents?',
        'Where to find policies and procedures?',
        'How do I edit documents?',
        'How do I use the collaberative area?'
      ]);
    });
  }

  getNews() {

    return new Promise((resolve) => {

      this.client
        .header('X-NXproperties', '*')
        .operation('Document.GetChildren')
        .input(`doc:/Intranet/sections/Intranet Site/News`)
        .execute()
        .then(handleSuccess);

      function handleSuccess(newsList) {
        const news = newsList.entries.map(newsItem => {
          return {
            uid: newsItem.uid,
            created: newsItem.properties['dc:created'],
            featured: newsItem.properties['nhstrust:featured'],
            header: newsItem.properties['nhstrust:header'].File1,
            title: newsItem.properties['dc:title'],
            description: newsItem.properties['dc:description'],
          content: newsItem.properties['note:note']};
        });
        resolve(news);
      }

    });
  }

  getUserDirectory() {
    return new Promise((resolve) => {
      jQuery
        .ajax(`${this.client._baseURL}/api/v1/directory/userDirectory`, {
          method: 'GET',
          dataType: 'JSON',
          headers: {
            Authorization: 'Basic ' + btoa(this.client._auth.username + ':' + this.client._auth.password)
          },
          error: () => {
            resolve([]);
          },
          success: response => {
            getUserList(response);
          }
        });

      function getUserList(directory) {
        const userList = directory.entries.map((item) => {
          return {
            label: item.properties.firstName + ' ' + item.properties.lastName,
            value: item.properties.username
          };
        });
        resolve(userList);
      }

    });
  }

  getGroupDirectory() {
    return new Promise((resolve) => {
      jQuery
        .ajax(`${this.client._baseURL}/api/v1/directory/groupDirectory`, {
          method: 'GET',
          dataType: 'JSON',
          headers: {
            Authorization: 'Basic ' + btoa(this.client._auth.username + ':' + this.client._auth.password)
          },
          error: () => {
            resolve([]);
          },
          success: response => {
            getGroupList(response);
          }
        });

      function getGroupList(directory) {
        const groupList = directory.entries.map((item) => {
          return {
            label: item.properties.grouplabel,
            value: item.properties.groupname
          };
        });
        resolve(groupList);
      }

    });
  }

  getTasksByUser() {

  }

}

class ElasticSearchResponseError extends Error {

}

var nuxeo = new Nuxeo();
export default nuxeo;
