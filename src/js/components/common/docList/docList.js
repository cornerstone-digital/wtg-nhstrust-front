import React from 'react';
import Time from 'react-time';
import utils from 'utils';
import './docList.scss';

const DocList = ({documents}) => (
<div className="list">
  <table className="table table-hover">
    <thead>
      <tr>
        <td className="small">
          <label htmlFor="select-all" className="sr-only">
            Select all
          </label>
        </td>
        <td className="small"></td>
        <td id="name">
          Name <span className="caret"></span>
        </td>
        <td id="owner">
          Owner
        </td>
        <td id="type">
          Type
        </td>
        <td id="owner">
          Status
        </td>
        <td id="last-modified">
          Last modified
        </td>
        <td></td>
      </tr>
    </thead>
    <tbody>
      <If condition={documents.length}>
        {documents.map((item, index) => (
          <tr key={index}>
            <td>
            </td>
            <td>
              <span className={utils.getDocIcon(item._source['ecm:primaryType'])}></span>
            </td>
            <td>
              <a href={utils.getDocLink(item)} title={item._source['dc:title']} alt={item._source['dc:title']}>{item._source['dc:title']}</a>
            </td>
            <td>
              {item._source['dc:lastContributor']}
            </td>
            <td>
              {item._source['ecm:primaryType']}
            </td>
            <td>
              {item._source['ecm:currentLifeCycleState']}
            </td>
            <td>
              <Time value={item._source['dc:modified']} format="Do MMM 'YY" />
            </td>
            <td></td>
            </tr>))}
        <Else/>
        <tr>
          <td colSpan="6">
            <h4 className="text-muted text-center state-empty"><em>contains no documents.</em></h4>
          </td>
        </tr>
      </If>
    </tbody>
  </table>
</div>
);

export default DocList;
