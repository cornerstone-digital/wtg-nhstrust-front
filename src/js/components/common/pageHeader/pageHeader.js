import React from 'react';

const PageHeader = ({listingPage, pageTitle, children}) => (
<section>
  <div id="page-title">
      <h1 className={listingPage && 'small'}>{pageTitle}</h1>
      {children}
  </div>
</section>
);

export default PageHeader;
