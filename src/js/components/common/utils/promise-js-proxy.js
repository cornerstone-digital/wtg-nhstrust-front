import es6Promise from 'es6-promise';

es6Promise.polyfill();

export default Promise;
