import React from 'react';
import Time from 'react-time';
import './utils.scss';
import nuxeoConfig, { allowedDocTypes } from 'config/nuxeo';
import dateFormat from 'dateformat';

const utils = {

  /**
   * Formats number to proper filesize.
   * @param  {[bytes]} the filesize
   * @return {[bytes]}
   */
  bytesToSize(bytes) {
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes === 0) return '0 Byte';
    const count = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)), 10);
    return Math.round(bytes / Math.pow(1024, count), 2) + ' ' + sizes[count];
  },

  /**
   * Encodes a string to SQL friendly format
   * @param  {string} str the given string
   * @return {string}     encoded string
   */
  /*eslint-disable */
  nxqlEncode(str) {
    return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, char => {
      switch (char) {
        case "\0":
          return "\\0";
        case "\x08":
          return "\\b";
        case "\x09":
          return "\\t";
        case "\x1a":
          return "\\z";
        case "\n":
          return "\\n";
        case "\r":
          return "\\r";
        case "\"":
        case "'":
        case "\\":
        case "%": return "\\" + char;
      }
    });
  },
  /*eslint-enable */

  /**
   * Formats the fieldContent based on a given field type
   * @param  {[fieldtype]} the fieldtype
   * @param  {[fieldContent]} the fieldContent
   * @return {[string]}
   */
  formatFieldsPerType(fieldtype, fieldContent) {
    if (fieldContent) {
      switch (fieldtype) {
        case 'text':
        case 'selectOneDirectory':
        case 'textarea':
          return fieldContent;
        case 'datetime':
          return <Time value={fieldContent} format="MM/DD/YYYY hh:mm" />;
        case 'list':
          return fieldContent;
        case 'file':
          return '';
        default: return '';
      }
    }
  },

  /**
   * Gets a icon given a specific fileName
   * @param  {string} filename the filename
   * @return {string}          the icon class
   */
  getFileIcon(fileName) {
    const ext = this.getFileExtension(fileName);
    switch (ext) {
      case 'docx':
        return 'fa fa-file-word-o';
      case 'xlsx':
        return 'fa fa-file-excel-o';
      case 'pdf':
        return 'fa fa-file-pdf-o';
      case 'ppt':
        return 'fa fa-file-powerpoint-o';
      case 'xls':
        return 'fa fa-file-excel-o';
      case 'doc':
        return 'fa fa-file-word-o';
      default: return 'fa fa-file-text-o';
    }
  },

  /**
   * Returns the file extension from file.
   * @param  {string} filename the filename
   * @return {string}          the file extension.
   */
  getFileExtension(fileName) {
    const re = /(?:\.([^.]+))?$/;
    return re.exec(fileName)[1];
  },

  getDocTypeLabel(docTypeId) {

    const docType = allowedDocTypes
      .find(item => item.id === docTypeId);

    if (!docType) {
      return docTypeId;
    }

    return docType.name;
  },

  getUrlSafeDocPath(dirtyPath) {
    const path = encodeURIComponent((dirtyPath ? dirtyPath : '')
      .replace(/^(\/)?/, ''))
      .replace(/%2F/gi, '/');

    return path;
  },

  /**
   * gets a valid link for a given docType
   * @param  {string} docType  the docType
   * @return {string}          A link
   */
  getDocLink(item) {
    const type = item.type || (item._source || {})['ecm:primaryType'];
    const uid = item.uid || item._id;
    const dirtyPath = item.path || (item._source || {})['ecm:path'];

    if (dirtyPath) {
      const path = this.getUrlSafeDocPath(dirtyPath);
      // todo: simplify routes by removing /Intranet without breaking top directory route
      // .replace(/^(\/Intranet)?(\/)?/, ''));

      return `#/path/${path}`;
    }

    switch (type) {
      case 'File':
        return `#doc/${uid}`;
      case 'Folder':
        return `#/folder/${uid}`;
      case 'Workspace':
        return `#/workspace/${uid}`;
      case 'PolicyWorkspace':
        return `#/workspace/${uid}`;
      default: return `#doc/${uid}`;
    }
  },

  getDocLinkFromUid(uid, type = 'File') {
    return this.getDocLink({
      uid: uid,
      type: type,
    });
  },

  // finds the right-hand-side of a relation
  getRelatedDocUid(relation, left) {
    const uids = [
      relation.properties['relation:source'],
      relation.properties['relation:target']
    ];

    return uids.find(uid => {
      return uid !== left;
    });
  },

  /**
   * Sorts a list of documents by Workspace
   *                           and by folder
   */
  sortByFolderish(item) {
    const type = item.type || item._source['ecm:primaryType'];
    const mixins = item.facets || item._source['ecm:mixinType'];

    if (type === 'Workspace') {
      return -1;
    }

    if (this.isFolderish(mixins)) {
      return -1;
    }
    return 1;
  },

  /**
    * Checkes if this doc is a folderish document
    * @param  {string}  schema a single schema
    * @return {Boolean}        returns true if there is a match
    */
  isFolderish(facets) {
    return facets.indexOf('Folderish') !== -1;
  },

  /**
   * Checks if documment is published in a section by looking at Immutable facet
   * @param  {string} facets the facets
   * @return {boolean} true if it is published
   */
  checkPublishedDoc(facets) {
    if (facets.indexOf('Immutable') >= 0) {
      return true;
    }
    return false;
  },


  /**
   * Determines if there is a fileName that exists
   * in the given document
   * @param  {object} doc document object
   * @return {string}     the filename or false
   */
  getFileName(doc) {
    const source = doc.properties || doc._source;

    if (source) {
      if (source['file:filename']) {
        return source['file:filename'];
      }

      if (source['file:content'] && source['file:content'].name) {
        return source['file:content'].name;
      }
    }

    return false;
  },

  /**
   * Determines which icon to render in search results
   * @return {string} the icon classname for the given document
   */
  renderIcon(doc) {
    const fileName = this.getFileName(doc);
    const type = doc.type || doc._source['ecm:primaryType'];
    let icon;

    if (fileName && type === 'File') {
      icon = utils.getFileIcon(fileName);
    } else {
      icon = utils.getDocIcon(type);
    }

    return icon;
  },

  /**
   * Determines if this user can create a document in folderish document
   * @return {object} the document in questions
   */
  canCreate(doc) {

    if (!doc.contextParameters) {
      return false;
    }

    if (!this.isFolderish(doc.facets)) {
      return false;
    }

    const createPermissions = ['Write', 'Read/Write', 'Everything'];
    return !!doc.contextParameters.permissions
      .filter(permission => !createPermissions.indexOf(permission)).length;
  },

  /**
     * get the permission right for a user in a workspace or document
     * @param  {object} docAcls the permissions object
     * @param  {object} user the current logged in user
     * @return {string}         the allowed permission right (read, write or/and everything)
     */
  getUserRightsToDoc(docAcls, user) {
    const permissionLevel = [];
    docAcls.map((item) => {
      item.ace.map((item2) => {

        if (item2.username === user.id) {
          permissionLevel.push(item2.permission);
        }
        user.extendedGroups.forEach((group) => {
          //
          if (group.name === item2.username) {

            permissionLevel.push(item2.permission);
          }
        });
      });
    });

    // order of permissions from high to low
    if (permissionLevel.indexOf('Everything') > -1) {
      return 'Everything';
    } else if (permissionLevel.indexOf('ReadRemove') > -1) {
      return 'ReadRemove';
    } else if (permissionLevel.indexOf('ReadWrite') > -1) {
      return 'ReadWrite';
    } else if (permissionLevel.indexOf('Write') > -1) {
      return 'Write';
    } else if (permissionLevel.indexOf('Read') > -1) {
      return 'Read';
    }
  },

  download(data, filename, mimetype = 'text/plain', charset = 'utf-8') {
    const a = document.createElement('a');

    a.download = filename;
    a.href = 'data:' + mimetype + ';charset=' + charset + ',' + encodeURIComponent(data);

    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  },

  // extract a list of property values
  pluck(arr, attr) {
    return Object.keys(arr).map(k => arr[k][attr]);
  },

  // returns the values of `a` which are not present in `b`
  difference(a, b) {
    return a.filter(val => {
      return b.indexOf(val) === -1;
    });
  },

  types: {
    'Audio': {
      icon: 'fa fa-file-audio-o',
      label: 'Audio',
    },
    'File': {
      icon: 'fa fa-paperclip',
      label: 'File',
    },
    'Folder': {
      icon: 'fa fa-folder-open',
      label: 'Folder',
    },
    'PolicyWorkspace': {
      icon: 'fa fa-folder',
      label: 'Policy Workspace',
    },
    'Application': {
      icon: 'fa fa-rocket',
      label: 'Application',
    },
    'SectionRoot': {
      icon: 'fa fa-folder-o',
      label: 'Section Root',
    },
    'Section': {
      icon: 'fa fa-folder-o',
      label: 'Section',
    },
    'Form': {
      icon: 'fa fa-file-text',
      label: 'Form',
    },
    'Note': {
      icon: 'fa fa-sticky-note-o',
      label: 'Note',
    },
    'Picture': {
      icon: 'fa fa-file-image-o',
      label: 'Picture',
    },
    'Template': {
      icon: 'fa fa-file-code-o',
      label: 'Template',
    },
    'TrustExpensesClaim': {
      icon: 'fa fa-book',
      label: 'Trust Expenses Claim',
    },
    'TrustPolicy': {
      icon: 'fa fa-file-text-o',
      label: 'Trust Policy',
    },
    'TrustScopingForm': {
      icon: 'fa fa-files-o',
      label: 'Trust Scoping Form',
    },
    'Video': {
      icon: 'fa fa-file-video-o',
      label: 'Video',
    },
    'Web Template': {
      icon: 'fa fa-columns',
      label: 'Web Template',
    },
    'News': {
      icon: 'fa fa-newspaper-o',
      label: 'News',
    },
    'Workspace': {
      icon: 'fa fa-folder-open',
      label: 'Workspace',
    },
  },

  /**
   * Gets a icon given a specific docType
   * @param  {string} docType  the docType
   * @return {string}          the icon class
   */
  getDocIcon(docType) {
    const info = this.types[docType];

    const icon = info ? info.icon : 'fa fa-file-o';

    return icon;
  },

  getFileTypeLabel(docType) {
    const info = this.types[docType];

    const label = info ? info.label : 'File';

    return label;
  },

  groupBy(arr, key) {
    return arr.reduce((groups, val) => {
      const groupKey = typeof(key) === 'function' ? key(val) : val[key];
      const group = groups[groupKey] = groups[groupKey] || [];

      group.push(val);

      return groups;
    }, {});
  },

  getExtensionFromMimeType(mimeType) {
    switch (mimeType) {
      case 'text/x-web-markdown':
        return '.md';
      case 'text/xml':
        return '.xml';
      case 'text/plain':
        return '.txt';
      case 'text/html':
        return '.html';
      default:
        return '';
    }
  },

  getImageUrls(doc) {
    const docId = doc.uid || doc._id;
    const docType = doc.type || doc._source['ecm:primaryType'];
    const repository = doc.repository || doc._source['ecm:repository'];
    const dateModified = doc.dateModified || doc._source['dc:modified'];
    let images;

    if (docType === 'Picture') {
      images = {
        thumbnail: `${nuxeoConfig.baseURL}/nxpicsfile/${repository}/${docId}/Thumbnail:content/${dateFormat(dateModified, 'ddd mmm dd HH:MM:ss Z yyyy')}`,
        small: `${nuxeoConfig.baseURL}/nxpicsfile/${repository}/${docId}/Small:content/${dateFormat(dateModified, 'ddd mmm dd HH:MM:ss Z yyyy')}`,
        medium: `${nuxeoConfig.baseURL}/nxpicsfile/${repository}/${docId}/Medium:content/${dateFormat(dateModified, 'ddd mmm dd HH:MM:ss Z yyyy')}`,
        original: `${nuxeoConfig.baseURL}/nxpicsfile/${repository}/${docId}/OriginalJpeg:content/${dateFormat(dateModified, 'ddd mmm dd HH:MM:ss Z yyyy')}`
      };
    }

    return images;
  },

  getDocumentUrl(doc) {
    switch (doc.type) {
      case 'Picture': {
        return `${nuxeoConfig.baseURL}/nxpicsfile/${doc.repository}/${doc.uid}/Medium:content/${dateFormat(doc.dateModified, 'ddd mmm dd HH:MM:ss Z yyyy')}`;
      }
      default: {
        return '';
      }
    }
  },

  getDocumentPath(doc) {
    let extension;

    switch (doc.type) {
      case 'Note': {
        extension = this.getExtensionFromMimeType(doc.properties['note:mime_type']);
        return `${nuxeoConfig.baseURL}/nxfile/${doc.repository}/${doc.uid}/blobholder:0/${doc.title}${extension}`;
      }
      default: {
        return `${nuxeoConfig.baseURL}/nxfile/${doc.repository}/${doc.uid}/blobholder:0/${doc.properties['file:filename']}`;
      }
    }
  },

  /*
  http://stackoverflow.com/a/28475133
  typeOf(); //undefined
  typeOf(null); //null
  typeOf(NaN); //number
  typeOf(5); //number
  typeOf({}); //object
  typeOf([]); //array
  typeOf(''); //string
  typeOf(function () {}); //function
  typeOf(/a/) //regexp
  */
  typeOf(val) {
    return {}.toString.call(val).split(' ')[1].slice(0, -1).toLowerCase();
  },

  inArray(haystack, needle) {
    return haystack.indexOf(needle) !== -1;
  },

  isEmpty(val) {
    return this.inArray(['undefined', 'null'], this.typeOf(val));
  },

  delay(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  },

  /*
   * Given a query object like { foo: 'bar', bar: 'baz' }
   * Will return a string like 'foo=bar&bar=baz'
   */
  queryToString(search) {
    return Object.keys(search).map(key => {
      return [key, search[key]].map(part => {
        return encodeURIComponent(part);
      }).join('=');
    }).join('&');
  },

  isUuid(text) {
    return (text + '').match(/^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}$/);
  }
};

export default utils;
