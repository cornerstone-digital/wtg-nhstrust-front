import React, { Component, PropTypes } from 'react';
import { Form } from 'react-bootstrap-validation';

import NuxeoDefaultForm from './defaultForm/defaultForm';
import NuxeoFormElements from './formElements/formElements';
import NuxeoFileUploader from './formElements/formFileUploader';
import NuxeoVersioner from './formElements/formVersioner';
import NuxeoTemplateChooser from './formElements/formTemplateChooser';

import FormButtons from './formElements/formButtons';
import DateTimePicker from 'react-bootstrap-datetimepicker';
import moment from 'moment';
import jQuery from 'jquery';

@NuxeoFormElements
class FormBuilder extends Component {

  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    layout: PropTypes.object.isRequired,
    initialData: PropTypes.object,
    nuxeoElements: PropTypes.object.isRequired,
    buttonLabel: PropTypes.string.isRequired,
    versionable: PropTypes.bool,
    workspaceTemplates: PropTypes.array,
    schemas: PropTypes.array
  }

  state = {
    versionType: 'skip'
  }

  /**
   * Set a red Asterisk for any
   * required field
   */
  componentDidMount() {
    jQuery('.isRequired')
      .prev('label')
      .addClass('isRequired');
  }

  /**
   * Adds a file reference to our state
   * @param  {array} fileBatch a object containing the
   *                             batchId and fileIndex
   */
  handleFormFileInput(fileBatch) {

    //if user cancels the upload
    if (!fileBatch) {
      this.setState({
        'file:content': null,
        'file:filename': null
      });
    }

    //if upload is successful
    if (fileBatch) {
      this.setState({
        'file:content': fileBatch ? fileBatch[0].file : null,
        'file:filename': fileBatch ? fileBatch[0].filename : null
      });
    }
  }

  /**
   * Bumps the document version number
   * @param  {object} event the radio button event change
   */
  handleVersionChange(event) {
    this.setState({
      'versionType': event.target.value
    });
  }

  /**
   * Sets a datetime field to the document
   * @param  {string} propertyName the target property
   * @param  {string} date
   */
  handleFormChange(propertyName, propertyValue) {
    this.setState({
      [propertyName]: propertyValue
    });
  }

  /**
   * Determines if we create this document from a given
   * template
   * @param  {object} event on select box change
   */
  handleTemplateChooser(event) {
    this.setState({
      createFromTemplate: event.target.value
    });
  }

  /**
   * Set our State on the form and submit
   * @param  {object} values all values found in the form
   */
  handleValidSubmit(values) {

    const formData = Object.assign(values, this.state);

    if (formData['file:content'] === null) {
      delete formData['file:content']
      ;
      delete formData['file:filename']
      ;
    }

    this.props.onSubmit(formData);
  }

  /**
   * Renders all our Nuxeo Elements to the form
   * @param  {string} item  a form widget (text, textarea etc..)
   * @param  {number} index current index in loop
   * @param  {function} callback Sets the a File to the FormBuilder State
   * @return {object}       React Component
   */
  renderFormElementsFromLayout() {

    const initialData = this.props.initialData;

    return this.props.layout.widgets.map((layoutItem, index) => {

      const nuxeoInput = this.props.nuxeoElements[layoutItem.type];
      const inputPropertyName = layoutItem.fields && layoutItem.fields[0].propertyName;
      let initialPropertyValue = initialData && initialData[inputPropertyName];

      //We hide the uid field by default
      if (inputPropertyName === 'uid:uid') {
        return false;
      }

      //If it's a file input - lets use the nuxeoFileUploader
      if (layoutItem.type === 'file') {
        return (
          <NuxeoFileUploader
            key={index}
            layout={layoutItem}
            initialFile={initialPropertyValue}
            onUpload={this.handleFormFileInput.bind(this)} />);
      }

      //If a date field, render a Bootstrap Date Picker
      if (layoutItem.type === 'datetime') {

        initialPropertyValue = initialPropertyValue || moment();

        return (
          <div className="form-group">
            <label className="control-label">
              {layoutItem.labels.any}
            </label>
            <DateTimePicker
              dateTime={moment(initialPropertyValue).format('YYYY-MM-DD')}
              format="YYYY-MM-DD"
              inputFormat="DD/MM/YYYY"
              mode="date"
              onChange={this.handleFormChange.bind(this, inputPropertyName)}
              />
          </div>
        );
      }

      //If its anything else Render a validated input field.
      if (nuxeoInput) {
        return nuxeoInput(layoutItem, index, initialPropertyValue);
      }

    });
  }

  /**
   * Renders a custom layout. If no layout exists use default
   * layout which relies on schemas.
   * @return {object} React Component
   */
  render() {
    const initialData = this.props.initialData;
    return (
      <If condition={this.props.layout.widgets.length}>
        <Form onValidSubmit={this.handleValidSubmit.bind(this)} className="form-styled form-builder">

          {this.renderFormElementsFromLayout()}

          <If condition={this.props.workspaceTemplates}>
            <NuxeoTemplateChooser onChange={this.handleTemplateChooser.bind(this)} workspaceTemplates={this.props.workspaceTemplates} />
          </If>

          <If condition={this.props.versionable && initialData}>
            <NuxeoVersioner
              initialVersion={`${initialData['uid:major_version'] || 0}.${initialData['uid:minor_version'] || 0}`}
              checkBoxValue={this.state.versionType}
              onChange={this.handleVersionChange.bind(this)} />
          </If>

          <FormButtons label={this.props.buttonLabel} onCancel={this.props.onCancel} />

        </Form>
        <Else />
        <NuxeoDefaultForm
          initialData={initialData}
          onValidSubmit={this.handleValidSubmit.bind(this)}
          onFileUpload={this.handleFormFileInput.bind(this)}
          onCancel={this.props.onCancel}
          buttonLabel={this.props.buttonLabel}
          versionable = {this.props.versionable}
          onVersionChange={this.handleVersionChange.bind(this)}
          versionType={this.state.versionType}
          schemas={this.props.schemas}
          onFormChange= {this.handleFormChange.bind(this)}
          />
      </If>
      );
  }
}

export default FormBuilder;
