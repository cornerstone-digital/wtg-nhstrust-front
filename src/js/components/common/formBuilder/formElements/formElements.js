import React, { Component } from 'react';
import { ValidatedInput} from 'react-bootstrap-validation';
import './formElements.scss';

/**
 * Attaches a validation rule to an input
 * @param  {object} layoutItem A widget as defined in nuxeo
 * @return {object}            The validation rule
 */
function _getValidationScheme(layoutItem) {
  const validationRules = layoutItem.propertiesByWidgetMode;

  if (!validationRules || !validationRules.edit) {
    return false;
  }

  if (validationRules.edit.required) {
    return {
      className: 'isRequired',
      validate: 'required',
      errorHelp: {required: `Please enter a ${layoutItem.labels.any}`}
    };
  }
}

/**
 * Attaches generic props to an input
 * @param  {object} layoutItem A widget as defined in nuxeo
 * @param  {number} index the current index inside a map
 * @return {object}            A list of props
 */
function _getInputTemplate(layoutItem, index) {
  return {
    key: index,
    label: layoutItem.labels.any,
    name: layoutItem.fields[0].propertyName,
    rows: 5,
  };
}

const NuxeoText = (layoutItem, index, initialValue) => (
<ValidatedInput
  type="input"
  defaultValue={initialValue}
  {..._getInputTemplate(layoutItem, index)}
  {..._getValidationScheme(layoutItem)} />
);

const NuxeoTextArea = (layoutItem, index, initialValue) => (
<ValidatedInput
  type="textarea"
  defaultValue={initialValue}
  {..._getInputTemplate(layoutItem, index)}
  {..._getValidationScheme(layoutItem)} />
);

const NuxeoSelectMenu = (layoutItem, index, initialValue) => (
<ValidatedInput
  type="select"
  defaultValue={initialValue}
  {..._getInputTemplate(layoutItem, index)}
  {..._getValidationScheme(layoutItem)}>
  {layoutItem.selectOptions.map((opt, optIndex) => (
   <option key={optIndex} value={opt.itemValue}>
     {opt.itemLabel}
   </option>)
   )}
</ValidatedInput>);


/**
 * We need to wrap these elements in a Higher Order Component
 * To fit the right context
 * See - http://stackoverflow.com/questions/32228905/react-bootstrap-validation-use-validated-input-in-sub-component
 * @param  {object} ComposedComponent The FormBuilder Class
 * @return {object}                   A wrapped version of that class
 */
const NuxeoFormElements = ComposedComponent => class extends Component {

  elements = {
    text: NuxeoText,
    textarea: NuxeoTextArea,
    htmltext: NuxeoTextArea,
    selectOneDirectory: NuxeoSelectMenu
  };

  render() {
    return <ComposedComponent {...this.props} nuxeoElements={this.elements} />;
  }

};

export default NuxeoFormElements;
