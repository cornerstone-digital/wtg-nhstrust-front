import React from 'react';
import { Button, ButtonToolbar } from 'react-bootstrap';

const FormButtons = ({label, onCancel}) => (
<ButtonToolbar className="pull-right">
  <Button
    bsSize="large"
    value="Cancel"
    className="white"
    onClick={onCancel}>
    Cancel
  </Button>
  <Button type="submit" bsSize="large">
    {label}
  </Button>
</ButtonToolbar>
);

export default FormButtons;
