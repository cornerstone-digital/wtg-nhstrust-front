import React, { Component, PropTypes } from 'react';
import Dropzone from 'common/dropzone/dropzone';
import utils from 'utils';

class NuxeoFileUploader extends Component {

  static propTypes = {
    layout: PropTypes.object,
    initialFile: PropTypes.object,
    onUpload: PropTypes.func.isRequired,
    fileType: PropTypes.string
  }

  state = {
    checkBoxValue: 'keep'
  }


  /**
   * Change checkbox state, cancel any uploads
   * if a user checks the keep radio button
   * @param  {object} event the clicked radio button
   */
  handleCheckboxChange(event) {

    if (event.target.value === 'keep') {
      this.props.onUpload(false);
    }

    this.setState({
      checkBoxValue: event.target.value
    });

  }


  render() {

    const layoutWidget = this.props.layout;

    /**
     * if no file exists - show a basic uploader
     * @param  {object} !this.props.initialFile the current file
     *                                          in document
     */
    if (!this.props.initialFile) {
      return (
        <div className="input-group">
          <label className="control-label">
            {layoutWidget ? layoutWidget.labels.any : 'Upload File'}
          </label>
          <Dropzone
            fileType={this.props.fileType}
            maxFiles={1}
            name="formContent"
            disabled={false}
            uploadHandler={this.props.onUpload} />
        </div>);
    }

    const initialFileName = this.props.initialFile.name;

    return (
      <div className="form-styled">
        <div className="input-group">
          <label className="active">
            {layoutWidget ? layoutWidget.labels.any : 'Upload File'}
          </label>
          <ul>
            <li>
              <label htmlFor="keep">
                <input
                  type="radio"
                  name="keep"
                  value="keep"
                  onChange={this.handleCheckboxChange.bind(this)}
                  checked={this.state.checkBoxValue === 'keep'} /> Keep
              </label>
              <span className="document"><i className={utils.getFileIcon(initialFileName)}></i> {initialFileName}</span>
            </li>
            <li>
              <label htmlFor="new">
                <input
                  type="radio"
                  name="new"
                  value="new"
                  onChange={this.handleCheckboxChange.bind(this)}
                  checked={this.state.checkBoxValue === 'new'} /> Upload New
              </label>
              <Dropzone
                fileType={this.props.fileType}
                maxFiles={1}
                name="formContent"
                disabled={this.state.checkBoxValue !== 'new'}
                uploadHandler={this.props.onUpload} />
            </li>
          </ul>
        </div>
      </div>
      );
  }

}

export default NuxeoFileUploader;
