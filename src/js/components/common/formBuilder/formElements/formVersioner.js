import React from 'react';

const NuxeoVersioner = ({checkBoxValue, onChange, initialVersion}) => (
<div className="form-styled">
  <div className="input-group">
    <label>
      Version
    </label>
    <div className="input">
      {initialVersion}
    </div>
  </div>
  <div className="input-group">
    <label className="active">
      Update Versions
    </label>
    <ul>
      <li>
        <label htmlFor="skip">
          <input
            type="radio"
            name="skip"
            value="skip"
            onChange={onChange}
            checked={checkBoxValue === 'skip'} /> Skip version increment
        </label>
      </li>
      <li>
        <label htmlFor="minor">
          <input
            type="radio"
            name="minor"
            value="minor"
            onChange={onChange}
            checked={checkBoxValue === 'minor'} /> Increment minor version
        </label>
      </li>
      <li>
        <label htmlFor="major">
          <input
            type="radio"
            name="major"
            value="major"
            onChange={onChange}
            checked={checkBoxValue === 'major'} /> Increment major version
        </label>
      </li>
    </ul>
  </div>
</div>
);

export default NuxeoVersioner;
