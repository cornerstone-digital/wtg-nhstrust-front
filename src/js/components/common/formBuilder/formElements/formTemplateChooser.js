import React from 'react';
import { Input, Col, Row } from 'react-bootstrap';

const FormTemplateChooser = ({onChange, workspaceTemplates}) => (
<Row>
  <Col
    xs={12}
    md={8}
    lg={6}>
    <Input
      type="select"
      label="Template to Base Creation On"
      placeholder="select"
      onChange={onChange}>
        <option value={null}> - </option>
        {workspaceTemplates.map((template, index) => (
         <option key={index} value={template.path}>
           {template.title}
         </option>
         ))}
    </Input>
  </Col>
</Row>
);

export default FormTemplateChooser;
