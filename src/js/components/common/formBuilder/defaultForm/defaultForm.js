import React, { Component, PropTypes } from 'react';
import { Form, ValidatedInput } from 'react-bootstrap-validation';
import NuxeoFileUploader from 'common/formBuilder/formElements/formFileUploader';
import FormButtons from 'common/formBuilder/formElements/formButtons';
import FormVersioner from 'common/formBuilder/formElements/formVersioner';
import HTMLEditor from 'common/htmlEditor/htmlEditor';
import utils from 'utils';

/**
 *  Determines what type of files can be uploaded
 *  for the given docType schemas
 */
const getFileType = (schemas) => {
  if (utils.inArray(schemas, 'video')) {
    return 'video';
  }
  if (utils.inArray(schemas, 'audio')) {
    return 'audio';
  }
  if (utils.inArray(schemas, 'picture')) {
    return 'picture';
  }
  return 'file';
};

/**
 * If no layout is found for a given document
 * we render a generic form and display elements
 * based on a document's schema
 */
class DefaultForm extends Component {

  static propTypes = {
    throwError: PropTypes.func,
    initialData: PropTypes.func,
    onFormChange: PropTypes.func,
    onValidSubmit: PropTypes.func,
    onCancel: PropTypes.func,
    onFileUpload: PropTypes.func,
    buttonLabel: PropTypes.func,
    versionable: PropTypes.func,
    onVersionChange: PropTypes.func,
    versionType: PropTypes.func,
    schemas: PropTypes.func
  };

  render() {
    return (
      <Form onValidSubmit={this.props.onValidSubmit}>
        <If condition={utils.inArray(this.props.schemas, 'dublincore')}>
          <div className="form-styled">
            <ValidatedInput
              type="text"
              label="Title"
              name="dc:title"
              defaultValue={this.props.initialData && this.props.initialData['dc:title']}
              className="isRequired"
              validate="required"
              errorHelp={{required: 'Please enter a title'}} />
            <div className="clearfix"></div>
          </div>
        </If>
        <If condition={utils.inArray(this.props.schemas, 'dublincore')}>
          <div className="form-styled">
            <ValidatedInput
              type="textarea"
              name="dc:description"
              defaultValue={this.props.initialData && this.props.initialData['dc:description']}
              label="Description"
              rows="5" />
            <div className="clearfix"></div>
          </div>
        </If>
        <If condition={utils.inArray(this.props.schemas, 'note')}>
          <HTMLEditor
            label="Note Content"
            value={this.props.initialData && this.props.initialData['note:note']}
            onChange={event => this.props.onFormChange('note:note', event.target.getContent())} /> />
        </If>
        <If condition={utils.inArray(this.props.schemas, 'file')}>
          <div className="form-styled">
            <NuxeoFileUploader
              initialFile={this.props.initialData && this.props.initialData['file:content']}
              onUpload={this.props.onFileUpload}
              fileType={getFileType(this.props.schemas)} />
          </div>
        </If>
        <If condition={this.props.versionable && this.props.initialData}>
          <FormVersioner
            initialVersion={`${this.props.initialData['uid:major_version']}.${this.props.initialData['uid:minor_version']}`}
            checkBoxValue={this.props.versionType}
            onChange={this.props.onVersionChange} />
        </If>
        <div className="form-styled">
          <FormButtons label={this.props.buttonLabel} onCancel={this.props.onCancel} />
        </div>
      </Form>
      );
  }
}

export default DefaultForm;
