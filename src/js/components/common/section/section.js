import React from 'react';
import './spinner.scss';

const Section = ({loaded, children}) => (
<section>
  <If condition={!loaded}>
    <div id="load-spinner">
      <img src="assets/img/ajax-loader.gif" alt="Loading" />
    </div>
    <Else />
    {children}
  </If>
  <div className="clearfix"></div>
</section>
);

export default Section;
