import React, { PropTypes } from 'react';
import BreadcrumbItem from './breadcrumbItem';

import utils from 'utils';
import './breadcrumbs.scss';

/**
 * Builds the route title based on the prop defines in routes.js
 * @param  {string} name  the given name of the root page
 */
const getRootName = rootLink => {
  const rootPages = {
    documentmanagement: 'Document Management',
    dashboard: 'Dashboard',
    policies: 'Policies',
    profile: 'Profile'
  };
  return rootPages[rootLink];
};

/**
 * Builds the route href to add to the breadcrumb
 * @param  {object} could be an document or a custom
 *                        object to fake breadcrumbs
 */
const getRouteLink = breadcrumb => {
  if (!breadcrumb.type) {
    return `#/${breadcrumb.route}`;
  }
  return utils.getDocLink(breadcrumb);
};

/**
 * Main component for rendering breadcrumbs
 * @param  {array}  props.breadcrumbData array of docs
 * @param  {string} props.breadcrumbRoot root path defined in routes.js
 * @param  {bool}   props.disabled       disables links on breadcrumbs
 * @return {object}                      jsx output
 */
const Breadcrumbs = ({breadcrumbData, breadcrumbRoot, disabled}) => (
  <ol className="breadcrumb">
    <BreadcrumbItem link={disabled ? 'javascript:;' : `#/${breadcrumbRoot}`} title={getRootName(breadcrumbRoot)} />
    {breadcrumbData
       .map((breadcrumb, index) => (
       <BreadcrumbItem
         key={index}
         title={breadcrumb.title}
         link={index === breadcrumbData.length - 1 ? 'javascript:;' : getRouteLink(breadcrumb)}
         className={index === breadcrumbData.length - 1 ? 'active' : ''} />
       ))}
  </ol>
);

Breadcrumbs.propTypes = {
  breadcrumbData: PropTypes.array.isRequired,
  breadcrumbRoot: PropTypes.string,
  disabled: PropTypes.bool
};

export default Breadcrumbs;
