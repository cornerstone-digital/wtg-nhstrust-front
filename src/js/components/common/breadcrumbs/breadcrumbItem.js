import React, { PropTypes } from 'react';

/**
 * A single breadcrumb item
 * @param  {string} props.link       href link to route
 * @param  {string} props.title      name of the route
 * @param  {string} props.className  add .active on last breadcrumb
 * @param  {number} props.key        key/index in array
 * @return {object}                  jsx output
 */
const BreadcrumbItem = ({link, title, className, key}) => (
<li className={className} key={key}>
  <a href={link}>
    {title}
  </a>
</li>
);

BreadcrumbItem.propTypes = {
  link: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  className: PropTypes.string,
  key: PropTypes.number
};

export default BreadcrumbItem;
