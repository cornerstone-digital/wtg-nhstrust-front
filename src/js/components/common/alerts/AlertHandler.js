import React, { Component } from 'react';
import AlertBox from 'components/common/alerts/AlertBox';
import merge from 'object-assign';

const alertDefaults = {

};

class AlertHandler extends Component {
  constructor() {
    super();
    this.state = {
      alerts: []
    };
  }

  addAlert(alert) {
    const _alert = merge({}, alertDefaults, alert);
    const alerts = this.state.alerts;

    if (isNaN(parseInt(alert.dismissAfter, 10))) {
      alert.dismissAfter = 5000;
    }

    alerts.push(_alert);

    this.setState({
      alerts: alerts
    });
  }

  renderAlerts() {
    return this.state.alerts.map((alert, i) => {
      return (<AlertBox alert={alert} key={i} />);
    });
  }

  render() {
      return (
        <div>
          {this.renderAlerts()}
        </div>
      );
  }
}

export default AlertHandler;
