import React, { Component, PropTypes } from 'react';
import {Alert, Button} from 'react-bootstrap';
import {findDOMNode} from 'react-dom';
import $ from 'jquery';

class AlertBox extends Component {
  static propTypes = {
    alert: PropTypes.object.isRequired
  };

  state = {
    alertVisible: true,
    iconClass: 'fa fa-exclamation-triangle'
  };

  handleAlertDismiss() {
    $(findDOMNode(this)).slideUp(400, () => {
      this.setState({alertVisible: false});
    });
  }

  setIconClass() {
    switch (this.props.alert.type) {
      case 'success': {
        this.setState({
          iconClass: 'fa fa-check-circle'
        });
        break;
      }
      case 'info': {
        this.setState({
          iconClass: 'fa fa fa-info-circle'
        });
        break;
      }
      case 'warning': {
        this.setState({
          iconClass: 'fa fa-exclamation-triangle'
        });
        break;
      }
      case 'danger': {
        this.setState({
          iconClass: 'fa fa-exclamation-triangle'
        });
        break;
      }
      default: {
        this.setState({
          iconClass: 'fa fa-info-circle'
        });
        break;
      }
    }
  }

  getDismissAfter() {
    return this.props.alert.dismissAfter | 0;
  }

  componentDidMount() {
    this.setIconClass();
  }

  renderButtons() {
    if (this.props.alert.buttons) {
      return (
        <p className="btn-toolbar">
          {this.props.alert.buttons.map((button, i) => {
            return (
              <Button
                bsStyle={button.style}
                bsSize={button.size}
                href={button.href}
                onClick={this.handleAlertDismiss.bind(this)}
                key={i}>
                {button.text}
              </Button>
            );
          })}
        </p>
      );
    }

    return false;
  }

  render() {
    if (this.state.alertVisible) {

      return (
        <Alert
          bsStyle={this.props.alert.type}
          onDismiss={this.handleAlertDismiss.bind(this)}
          {...this.getDismissAfter() > 0 ? {dismissAfter: this.getDismissAfter()} : {}}
          dismissAfter={this.getDismissAfter()}
        >
          <strong><i className={this.state.iconClass}></i> {this.props.alert.title}</strong>
          <p>{this.props.alert.text}</p>
          {this.renderButtons()}
        </Alert>
      );
    }

    return <div></div>;
  }
}

export default AlertBox;
