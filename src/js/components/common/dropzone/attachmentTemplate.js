import React from 'react';
import ReactDOMServer from 'react-dom/server';
import './dropzone.css';

const template = {
  autoProcessQueue: false,
  uploadMultiple: true,
  forceFallback: false,
  previewTemplate: ReactDOMServer.renderToStaticMarkup(
    <div className="files" id="attachment-previews">
      <div id="attachment-template" className="file-row">
        <div className="info">
          <p className="name" data-dz-name></p>
          <p className="size hint" data-dz-size></p>
          <button data-dz-remove className="btn btn-danger delete">
            <i className="fa fa-trash-o"></i>
          </button>
        </div>
        <p className="error text-danger" data-dz-errormessage></p>
        <div className="upload-bar">
          <div className="progress"
               role="progressbar"
               aria-valuemin="0"
               aria-valuemax="100"
               aria-valuenow="0">
            <div className="progress-bar progress-bar-success" data-dz-uploadprogress></div>
          </div>
        </div>
      </div>
    </div>
  )
};

export default template;
