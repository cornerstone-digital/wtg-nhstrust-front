import React, { PropTypes, Component } from 'react';
import ReactDOMServer from 'react-dom/server';
import DropzoneComponent from 'react-dropzone-component';

import jquery from 'jquery';
import nuxeo from 'common/nuxeo/nuxeo';
import { allowedMediaTypes } from 'config/nuxeo';

import './dropzone.scss';

class Dropzone extends Component {

  static propTypes = {
    uploadHandler: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    disabled: PropTypes.bool,
    maxFiles: PropTypes.number,
    fileType: PropTypes.string.isRequired
  }

  /**
   * Initalise Nuxeo Batch ID before uploading files
   * Disable uploader if disable property is set
   */
  componentDidMount() {
    this.init();
    this.enableDropzone(!!this.props.disabled);
  }

  /**
   * when the disable property changes, lets check
   *                  if we can enable the dropzone
   * @param  {[type]} nextProps props thats updated from it's parent
   */
  componentWillReceiveProps(nextProps) {
    if (this.props.disabled !== nextProps.disabled) {
      this.enableDropzone(!!nextProps.disabled);
    }
  }

  init() {
    nuxeo
      .generateBatch()
      .then(generatedBatchId => this.setState({
        batchId: generatedBatchId
      }));
  }

  /**
   * Checks if we should enable the Dropzone
   * @param  {Boolean} isDisabled prop from the parent
   */
  enableDropzone(isDisabled) {

    if (isDisabled) {
      this.dropzone.disable();
    }

    if (!isDisabled) {
      this.dropzone.enable();
    }

    jquery(this.dropzone.element)
      .toggleClass('is-disabled', isDisabled);
  }

  /**
   * Defines the styles when we are uploading a doc
   * @type {Object}
   */
  templateConfig = {
      autoProcessQueue: false,
      uploadMultiple: true,
      forceFallback: false,
      maxFilesize: 15,
      acceptedFiles: this.props.fileType ? allowedMediaTypes[this.props.fileType] : allowedMediaTypes.file,
      maxFiles: this.props.maxFiles || null,
      dictFileTooBig: 'Error: File size too large. Max 15mb',
      dictMaxFilesExceeded: 'Error: You are limited to one upload in this document',
      dictInvalidFileType: 'Error: You can not upload files of this type',
      previewTemplate: ReactDOMServer.renderToStaticMarkup(
        <div className="files" id="attachment-previews">
          <div id="attachment-template" className="file-row">
            <div className="info">
              <p className="name" data-dz-name></p>
              <p className="size hint" data-dz-size></p>
              <button data-dz-remove="" className="btn btn-danger delete">
                <i className="fa fa-trash-o"></i>
              </button>
            </div>
            <p className="error text-danger" data-dz-errormessage></p>
            <div className="upload-bar">
              <div className="progress">
                <div className="progress-bar progress-bar-success" data-dz-uploadprogress></div>
              </div>
            </div>
          </div>
        </div>
      )
    };

  /**
   * Generic Thumbnail config
   * @type {Object}
   */
  componentConfig = {
      postUrl: 'http://jsonplaceholder.typicode.com/posts',
      dictMaxFilesExceeded: 'Warning: You are limited to one upload in this document',
      thumbnailWidth: 80,
      thumbnailHeight: 80,
      parallelUploads: 20
    };

  /**
   * Main Event Handlers for DropZone
   * @type {Object}
   */
  eventHandlers = {
      init: dropzone => {
        //Expose dropzone to window for E2E testing
        window.dropzone = window.dropzone || {};
        window.dropzone[this.props.name] = dropzone;

        this.dropzone = dropzone;
      },
      addedfile: (file) => this.addFile(file, this.dropzone.files),
      removedfile: (file) => this.removeFile(file, this.dropzone.files),
      error: () => this.dropzone.hasError = true,
      maxfilesexceeded: () => this.dropzone.hasError = true
    };

  removeFile() {
    this.props.uploadHandler(null);
    this.init();
  }

  /**
   * Upload that file to nuxeo!
   * @param {object} file     file object to be uploaded
   * @param {array}  fileList list of file objects
   */
  addFile(file, fileList) {

    setTimeout(() => upload(this), 500);

    function upload(self) {
      const $progressBar = jquery(file.previewElement).find('.progress-bar');

      if (self.dropzone.hasError) {
        self.dropzone.hasError = false;
        return $progressBar.html('Upload Failed');
      }

      nuxeo
        .uploadFiles({
          name: file.name,
          fileData: file,
          index: fileList.length - 1,
          batchId: self.state.batchId,
          onProgress: progress => $progressBar.width(`${progress.upload * 100}%`)
        })
        .then(function handleSuccess(batch) {
          self.props.uploadHandler(batch);
          $progressBar.width(`100%`);
          $progressBar
            .one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',
              () => $progressBar.html('Upload Complete'));
        });
    }
  }

  render() {
    return (
      <div id={this.props.maxFiles ? 'form-content-upload' : 'form-attachment-upload'}>
        <DropzoneComponent
          ref="dropzone"
          config={this.componentConfig}
          eventHandlers={this.eventHandlers}
          djsConfig={this.templateConfig}
          className="attachment-dropzone">
          <div className="dz-message" data-dz-message="">
            Drop files to upload.
            <br/>
            <span className="note">( or click )</span>
          </div>
        </DropzoneComponent>
      </div>
      );
  }

}

export default Dropzone;
