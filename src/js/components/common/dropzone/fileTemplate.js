import React from 'react';
import ReactDOMServer from 'react-dom/server';
import './dropzone.css';

const template = {
  autoProcessQueue: false,
  uploadMultiple: false,
  parallelUploads: false,
  forceFallback: false,
  previewTemplate: ReactDOMServer.renderToStaticMarkup(
    <div className="files" id="previews">
    <div id="template" className="file-row">
        <div className="info">
            <p className="name"></p>
            <p className="size hint"></p>
            <strong className="error text-danger"></strong>
        </div>
        <div className="upload-bar">
            <div className="progress" role="progressbar">
                <div className="progress-bar progress-bar-success"></div>
            </div>
        </div>
        <div className="buttons">
            <button className="btn btn-primary start">
                <i className="fa fa-upload"></i>
            </button>
            <button data-dz-remove className="btn btn-warning cancel">
                <i className="fa fa-ban"></i>
            </button>
            <button data-dz-remove className="btn btn-danger delete">
                <i className="fa fa-trash-o"></i>
            </button>
        </div>
    </div>
</div>
  )
};

export default template;
