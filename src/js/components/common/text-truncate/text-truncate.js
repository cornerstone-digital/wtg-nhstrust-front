/*eslint-disable */
import React, {Component, PropTypes} from 'react';
import jQuery from 'jquery';

/**
 * https://github.com/ShinyChang/React-Text-Truncate
 */
export default class TextTruncate extends Component {

    static propTypes = {
        text: PropTypes.string,
        truncateText: PropTypes.string,
        line: PropTypes.number,
        showTitle: PropTypes.bool
    };

    static defaultProps = {
        text: '',
        truncateText: '…',
        line: 1,
        showTitle: true
    };

    componentWillMount() {
        const canvas = document.createElement('canvas');
        const docFragment = document.createDocumentFragment();
        docFragment.appendChild(canvas);
        this.canvas = canvas.getContext('2d');
    }
    componentDidMount() {
        const style = window.getComputedStyle(this.refs.scope);
        const font = [];
        font.push(style['font-weight']);
        font.push(style['font-style']);
        font.push(style['font-size']);
        font.push(style['font-family']);
        this.canvas.font = font.join(' ');
        this.forceUpdate();
        window.addEventListener('resize', () => {
            this.forceUpdate();
        });
    }

    getRenderText() {
        let textWidth = this.measureWidth(this.props.text);
        let ellipsisWidth = this.measureWidth(this.props.truncateText);

        let scopeWidth = this.refs.scope.offsetWidth;

        if (this.props.icon) {
            scopeWidth = scopeWidth - 30;
        }

        if (this.props.offset) {
            scopeWidth = scopeWidth + this.props.offset;
        }

        if (scopeWidth >= textWidth) {
            return this.props.text;
        } else {
            let n = 0;
            let max = this.props.text.length;
            let text = '';
            let splitPos = 0;
            let startPos = 0;
            let line = this.props.line;
            while(line--) {
                let ext = line ? '' : this.props.truncateText;
                while(n <= max) {
                    n++;
                    text = this.props.text.substr(startPos, n);
                    if (this.measureWidth(text + ext) > scopeWidth) {
                        splitPos = text.lastIndexOf(' ');
                        if (splitPos === -1) {
                            splitPos = n - 1;
                        }
                        startPos += splitPos;
                        break;
                    }
                }
                if (n >= max) {
                    startPos = max;
                    break;
                }
                n = 0;
            }
            return startPos === max
                      ? this.props.text
                      : this.props.text.substr(0, startPos - 1) + this.props.truncateText;
        }
    }

    measureWidth(text) {
        return this.canvas.measureText(text).width;
    }

    render() {
        let text = '';

        if (this.refs.scope) {
            text = this.getRenderText();
        }

        const attrs = {
            ref: 'scope',
            className: this.props.float && 'is-floated'
        };

        return (
            <div {...attrs}>
            <If condition={this.props.icon}>
                <span>
                <i className={this.props.icon}></i>
                {text}
                </span>
            <Else/>
                {text}
            </If>
            </div>
        );
    }
};
