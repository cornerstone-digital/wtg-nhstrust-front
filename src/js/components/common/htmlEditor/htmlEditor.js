import React, { PropTypes } from 'react';
import TinyMCE from 'react-tinymce';
import './htmlEditor.scss';

const defaultFontFamily = `"Helvetica Neue",Helvetica,Arial,sans-serif`;

const tinyConfig = {
  height: 400,
  plugins: [
    'advlist autolink lists link image preview hr anchor pagebreak',
    'visualblocks visualchars code fullscreen',
    'nonbreaking contextmenu directionality',
    'emoticons paste textcolor colorpicker textpattern'
  ],
  allow_script_urls: true,
  toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  style_formats_merge: false,
  style_formats: [
    {title: 'Headers', items: [
        {title: 'Header 1', block: 'h1', styles: {'font-family': defaultFontFamily}},
        {title: 'Header 2', block: 'h2', styles: {'font-family': defaultFontFamily}},
        {title: 'Header 3', block: 'h3', styles: {'font-family': defaultFontFamily}},
        {title: 'Header 4', block: 'h4', styles: {'font-family': defaultFontFamily}},
        {title: 'Header 5', block: 'h5', styles: {'font-family': defaultFontFamily}},
        {title: 'Header 6', block: 'h6', styles: {'font-family': defaultFontFamily}}
    ]},
    {title: 'Inline', items: [
        {title: 'Bold', icon: 'bold', inline: 'strong', styles: {'font-family': defaultFontFamily}},
        {title: 'Italic', icon: 'italic', inline: 'em', styles: {'font-family': defaultFontFamily}},
        {title: 'Underline', icon: 'underline', inline: 'span', styles: {'text-decoration': 'underline'}},
        {title: 'Strikethrough', icon: 'strikethrough', inline: 'span', styles: {'text-decoration': 'line-through'}},
    ]},
    {title: 'Blocks', items: [
        {title: 'Paragraph', block: 'p', styles: {'font-family': defaultFontFamily}},
        {title: 'Blockquote', block: 'blockquote', styles: {'font-family': defaultFontFamily}},
        {title: 'Div', block: 'div', styles: {'font-family': defaultFontFamily}},
        {title: 'Pre', block: 'pre', styles: {'font-family': defaultFontFamily}}
    ]}
  ],
  imagetools_cors_hosts: ['youtube.com', 'youtube.co.uk'],
  content_css: [
    'assets/styles/bootstrap.min.css',
    'assets/styles/main.css'
  ]

};

const HTMLEditor = ({value, label, onChange}) => {
  return (
    <div className="form-group htmlEditor">
      <label className="control-label">
        {label}
      </label>
      <TinyMCE
        content={value}
        label={label}
        config={tinyConfig}
        onChange={onChange} />
    </div>
    );
};

HTMLEditor.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func
};

export default HTMLEditor;
