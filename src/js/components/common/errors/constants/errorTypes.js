export function NotFound(message) {
  this.prototype = Object.create(Error.prototype);
  this.name = 'NotFound';
  this.message = (message || '');
}

export function ServerError(message) {
    this.prototype = Object.create(Error.prototype);
    this.name = 'ServerError';
    this.message = (message || '');
}

export function Forbidden(message) {
  this.prototype = Object.create(Error.prototype);
  this.name = 'Forbidden';
  this.message = (message || '');
}

export function UnknownError(message) {
  this.prototype = Object.create(Error.prototype);
  this.name = 'UnknownError';
  this.message = (message || '');
}
