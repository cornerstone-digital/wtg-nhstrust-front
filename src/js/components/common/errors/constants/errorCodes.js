const ErrorCodes = new Map();

// 404: Page Not Found

ErrorCodes.set('Not Found', {
  title: '404: Page not found',
  level: 'error',
  message: 'The requested page could not be found. You have been redirected to your dashboard page.',
  autoDismiss: 10,
  redirect: '#/dashboard'
});

// 403: Access Denied
ErrorCodes.set('Forbidden', {
  title: '403: Forbidden',
  level: 'error',
  message: 'You do not have the correct permissions to perform this action.',
  autoDismiss: 10,
  redirect: '#/dashboard'
});

// 500: Server Error
ErrorCodes.set('Internal Server Error', {
  title: '500: Internal Server Error',
  level: 'error',
  message: 'There has been an internal server error.',
  autoDismiss: 10
});

//// Connection Issue
//ErrorCodes.set('Timeout', {
//  title: '408: Request Timeout',
//  level: 'error',
//  message: 'There appears to a connection issue. Please check your network connection and try again.',
//  autoDismiss: 10
//});


export default ErrorCodes;
