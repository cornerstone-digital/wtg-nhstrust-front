import ErrorConfig from 'config/errors';
import $ from 'jquery';

class RaygunProvider {
  constructor() {
    var oldOnLoad = window.onload;
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.src = '//cdn.raygun.io/raygun4js/raygun.min.js';
    $('head').append(s);


    if (typeof window.onload !== 'function') {
      // you can hook your function with it
      window.onload = this.initRaygun;
    } else { // someone already hooked a function
      window.onload = function call() {
        // call the function hooked already
        oldOnLoad();
        // call your awesome function
        this.initRaygun();
      };
    }
  }

  initRaygun() {
    var Raygun = window.Raygun;

    this.client = Raygun.init(ErrorConfig.raygun.apiKey);

    if (process.env.Version) {
      Raygun.setVersion(process.env.Version);
    }

    this.client.attach();
  }

  logException(e) {
    if (typeof this.client !== 'object') {
      this.initRaygun();
    }

    this.client.send(e);
  }

  logMessage(message) {
    if (typeof this.client !== 'object') {
      this.initRaygun();
    }

    this.client.send(message);
  }
}

var Provider = new RaygunProvider();

export default Provider;
