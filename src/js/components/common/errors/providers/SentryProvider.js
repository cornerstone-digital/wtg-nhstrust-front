import Raven from 'raven-js';
import ErrorConfig from 'config/errors';

class SentryProvider {
  client = Raven.config('https://' + ErrorConfig.sentry.key + '@app.getsentry.com/' + ErrorConfig.sentry.id);

  logException(e) {
    this.client.captureException(e);
  }

  logMessage(message) {
    this.client.captureMessage(message);
  }
}

var Provider = new SentryProvider();

export default Provider;
