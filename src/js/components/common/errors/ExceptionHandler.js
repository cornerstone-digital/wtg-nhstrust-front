//import Provider from './providers/SentryProvider';
import Provider from './providers/RaygunProvider';
import merge from 'object-assign';
import { NotFound, ServerError, Forbidden, UnknownError } from './constants/errorTypes';

export function logException(e, customData) {
  const error = merge({}, e, customData);

  console.error(e, (e || {}).stack, customData);

  Provider.logException(error);
}

export function logMessage(message) {
  console.log(message);

  Provider.logMessage(message);
}

export function raiseException(message) {
  switch (message) {
    case 'Not Found': {
      return new NotFound(message);
    }
    case 'Forbidden': {
      return new Forbidden(message);
    }
    case 'Internal Server Error': {
      return new ServerError(message);
    }
    default: {
      return new UnknownError(message);
    }
  }
}
