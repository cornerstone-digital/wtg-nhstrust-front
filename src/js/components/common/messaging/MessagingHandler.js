import io from 'socket.io-client';

class MessagingHandler {
  constructor() {
    const serviceUrl = 'https://wtg-pubsub-server.herokuapp.com';
    this.socket = io.connect(serviceUrl);
  }

  identify(data) {
    this.socket.emit('identify', data);
  }

  on(channel, callback) {

    this.socket.on(channel + '-notification', (data) => {
      callback(data);
    });

    this.socket.on(channel + '-alert', (data) => {
      callback(data);
    });

    this.socket.on(channel + '-message', (data) => {
      callback(data);
    });
  }

  subscribe(channel) {
    this.socket.emit('subscribe', channel);
  }

  unsubscribe(channel) {
    this.socket.emit('unsubscribe', channel);
  }

  publish(data) {
    this.socket.emit('publish', {
      channel: data.channel,
      action: data.action,
      initiatedBy: data.initiatedBy,
      content: data.content
    });
  }
}

var messagingHandler = new MessagingHandler();

export default messagingHandler;
