import React from 'react';
import './spinner.scss';

const Spinner = ({size}) => (
<section>
  <div id="load-spinner" className={size}>
    <img src="assets/img/ajax-loader.gif" alt="Loading" />
  </div>
  <div className="clearfix"></div>
</section>
);

export default Spinner;
