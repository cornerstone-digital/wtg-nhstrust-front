import React, { Component, PropTypes } from 'react';
import Breadcrumbs from 'common/breadcrumbs/breadcrumbs';
import PageHeader from 'common/pageHeader/pageHeader';
import nuxeo from 'common/nuxeo/nuxeo';
import ViewDocList from './viewDocList/viewDocList';
import DocListingNav from './docListingNav/docListingNav';
import { connect } from 'react-redux';
import utils from 'common/utils/utils';
import Spinner from 'common/spinner/spinner.js';
import ViewDocGrid from './viewDocGrid/viewDocGrid';
import './docListingPage.scss';
import { throwError } from 'actions/error';
import { refreshDocument, addPermissions, delPermission, toggleFavourite } from 'actions/document';
import { refreshList, requestList, deleteDocuments } from 'actions/list';
import DebounceInput from 'react-debounce-input';
import ga from 'react-ga';

@connect(state => ({
    user: state.user,
    list: state.list,
}), {
  throwError,
  refreshDocument,
  addPermissions,
  delPermission,
  toggleFavourite,
  refreshList,
  requestList,
  deleteDocuments,
})

class DocListingPage extends Component {

  static propTypes = {
    title: PropTypes.string.isRequired,
    document: PropTypes.object,
    list: PropTypes.object,
    breadcrumbs: PropTypes.array,
    history: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
    throwError: PropTypes.func,
    refreshDocument: PropTypes.func.isRequired,
    addPermissions: PropTypes.func.isRequired,
    delPermission: PropTypes.func.isRequired,
    toggleFavourite: PropTypes.func.isRequired,
    refreshList: PropTypes.func.isRequired,
    requestList: PropTypes.func.isRequired,
    deleteDocuments: PropTypes.func.isRequired,
  };

  static queryMap = {
    deep: {
      default: false,
      valid: [false, true]
    },
    search: {
      default: ''
    },
    layout: {
      default: 'list',
      valid: ['list', 'grid']
    },
    orderByAttribute: {
      default: 'dc:title',
      valid: ['dc:title', 'ecm:primaryType', 'ecm:currentLifeCycleState', 'dc:modified'],
      properties: {
        'dc:title': ['_source', 'dc:title'],
        'ecm:primaryType': ['_source', 'ecm:primaryType'],
        'ecm:currentLifeCycleState': ['_source', 'ecm:currentLifeCycleState'],
        'dc:modified': ['_source', 'dc:modified']
      }
    },
    orderByDirection: {
      default: 'ASC',
      valid: ['ASC', 'DESC']
    },
    grouping: {
      default: false,
      valid: [false, true]
    },
    types: {
      default: [],
      valid: {...utils.types}
    },
    folderish: {
      default: undefined,
      valid: [
        undefined, // both folderish and non-folderish
        false, // only non-folderish
        true // only folderish
      ]
    }
  }

  state = {
    isPinned: undefined,
  }

  componentDidMount() {
    const query = this.getQuery(this.props);

    if (this.props.document) {
      this.fetchPinnedStatus(this.props.document.uid);
    }

    this.props.requestList(query);

    if (query.search) {
      this.trackSearch(query.search);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.document) {
      const oldDoc = this.props.document;
      const newDoc = nextProps.document;

      if (newDoc && (!oldDoc || oldDoc.uid !== newDoc.uid)) {
        this.fetchPinnedStatus(newDoc.uid);
      }
    }

    const oldQuery = this.getQuery(this.props);
    const newQuery = this.getQuery(nextProps);

    // todo: is a serialised comparison really the best way?
    if (JSON.stringify(oldQuery) !== JSON.stringify(newQuery)) {
      this.props.requestList(newQuery);
    }

    if (newQuery.search && oldQuery.search !== newQuery.search) {
      this.trackSearch(newQuery.search);
    }
  }

  fetchPinnedStatus(docId) {
    nuxeo.getPinnedStatus(docId)
    .then(response => {
      this.setState({
        isPinned: response
      });
    })
    .catch((error) => {
      this.props.throwError(error);
    });
  }

  trackSearch(search) {
    return ga.event({
      category: 'Document List',
      action: 'Performed a search',
      label: search
    });
  }

  deletePermission(parentId, user) {
    return this.props.delPermission(user);
  }

  addPermissions(parentId, users, permissionType) {
    const usersArray = users.split(',');
    return this.props.addPermissions(usersArray, permissionType);
  }

  deleteDocuments(docIds) {
    return this.props.deleteDocuments(docIds);
  }

  getDocumentsById(docIds) {
    return nuxeo.getDocumentsById(docIds);
  }

  toggleFavorite(docId, toggle) {
    return this.props.toggleFavourite(toggle)
    .then(() => {
      this.fetchPinnedStatus(this.props.document.uid);
    });
  }

  tryParse(val) {
    try {
      return JSON.parse(val);
    } catch (err) {
      return val;
    }
  }

  isPartialQueryActive(partial) {
    const keys = Object.keys(partial);
    const current = this.getQuery();

    const active = keys.filter((key) => {
      return partial[key] === current[key];
    });

    return active.length >= keys.length;
  }

  // returns a valid query object with defaults
  getQuery(nextProps) {
    // todo: could use this.state instead of this.props, since it's merged in this.fetchData, is that better?
    const props = nextProps || this.props;
    const query = {};

    if (props.document) {
      if (props.document.type === 'Favorites') {
        query.collection = props.document.uid;
      } else {
        query.path = props.document.path;
      }
    }

    Object.keys(this.constructor.queryMap).forEach(key => {
      const map = this.constructor.queryMap[key];
      let val = props.location.query[key];

      switch (key) {
        case 'folderish':
        case 'deep':
        case 'grouping':
        case 'orderByAttribute':
        case 'orderByDirection':
        case 'layout':
          val = this.tryParse(val);
          if (map.valid.indexOf(val) >= 0) {
            query[key] = val;
          } else {
            query[key] = map.default;
          }
        break;
        case 'search':
          query[key] = val || '';
        break;
        case 'types':
          if (utils.typeOf(val) === 'array') {
            query[key] = val;
          } else if (utils.typeOf(val) !== 'undefined') {
            query[key] = [val];
          } else {
            query[key] = map.default;
          }

          const valid = Object.keys(map.valid);
          query[key] = query[key].filter(type => valid.indexOf(type) !== -1);
        break;
        default:
        // todo: fail better.
      }

      // query uses ORDER BY when fetching from API, but frontend mangles data into groups after response
      // because NXQL column differs from JSON response keys, use groupByAttribute for post-response mangling
      // groupByAttribute supports deep key references: { properties: { foo: bar }} is targeted like [ 'properties', 'foo' ]
      // only appears in getQuery because its presence in the URL is unnecessary
      if (key === 'orderByAttribute') {
        query.groupByAttribute = map.properties[val];
      }
    });

    return query;
  }

  // sets (or deletes if default) the query parameters passed in
  setQuery(newQuery) {
    const location = this.props.location;
    const pathname = location.pathname;
    const query = {...location.query};

    Object.keys(newQuery).forEach(key => {
      switch (key) {
        case 'folderish':
        case 'deep':
        case 'search':
        case 'grouping':
        case 'orderByAttribute':
        case 'orderByDirection':
        case 'layout':
          if (newQuery[key] === this.constructor.queryMap[key].default) {
            delete query[key];
          } else {
            query[key] = newQuery[key];
          }
        break;
        case 'types':
          if (this.constructor.queryMap[key].default.length < 1) {
            if (newQuery[key].length < 1) {
              delete query[key];
            } else {
              query[key] = newQuery[key];
            }
          } else {
            // todo: if default array has values, compare them. not required for now
          }
        break;
        default:
        // todo: fail better.
      }
    });

    this.props.history.push(this.props.history.createPath(pathname, query));
  }

  refreshPage() {
    return Promise.all([
      this.props.document && this.props.refreshDocument(),
      this.props.refreshList(),
    ]);
  }

  getTypeFilters() {
    return utils.types;
  }

  setSearch(e) {
    this.setQuery({
      search: e.target.value
    });
  }

  setDeep(e) {
    this.setQuery({
      deep: !!e.target.checked
    });
  }

  getExternalSearchUrl() {
    const query = this.getQuery();
    const a = document.createElement('a');
    a.href = 'https://www.google.com/search';

    a.search = utils.queryToString({
      q: query.search
    });

    return a.href;
  }

  toggleType(key, toggle) {
    let types = [...this.getQuery().types];

    const prevOn = types.indexOf(key) !== -1;
    const nextOn = typeof(toggle) !== 'undefined' ? !!toggle : !prevOn;

    if (nextOn) {
      types = types
        .concat([key]) // add our option to the array
        .reduce((prev, cur) => { // ensure our option isn't in there twice by getting unique values
          return prev.indexOf(cur) === -1 ? prev.concat([cur]) : prev;
        }, []);
    } else {
      types = types
        .filter(thisKey => { // remove our option
          return thisKey !== key;
        });
    }

    this.setQuery({
      types: types
    });
  }

  render() {
    const document = this.props.document;
    const breadcrumbs = document ? document.contextParameters.breadcrumb.entries : this.props.breadcrumbs;
    const query = this.getQuery();
    const types = this.constructor.queryMap.types.valid;
    const folderish = [
      { key: undefined, label: 'All' },
      { key: true, label: 'Only folderish' },
      { key: false, label: 'Only non-folderish' },
    ];

    return (
      <section>
        <Breadcrumbs breadcrumbData={breadcrumbs} breadcrumbRoot={this.props.route.parent} />
        <PageHeader pageTitle={this.props.title} listingPage>
          <DocListingNav
            setQuery={::this.setQuery}
            refreshPage={::this.refreshPage}
            isPartialQueryActive={::this.isPartialQueryActive}
            user={this.props.user}
            document={document}
            handleDeletePermission={this.deletePermission.bind(this)}
            handleSubmit={this.addPermissions.bind(this)}
            toggleFavorite={this.toggleFavorite.bind(this)}
            isPinned={this.state.isPinned}
            history={this.props.history}
            location={this.props.location} />
        </PageHeader>
        <div id="page-body">
          <div className="filters-container">
            <div className="panel panel-primary panel-filters">
              <div className="panel-heading">
                <h3 className="panel-title">Filters</h3>
              </div>
              <div className="panel-body">
                <form onSubmit={e => e.preventDefault()}>
                  <label htmlFor="keywords">Keywords</label>
                  <DebounceInput
                    debounceTimeout={750}
                    type="text"
                    id="filter-keywords"
                    value={query.search}
                    onChange={::this.setSearch} />

                  <label>Look for...</label>
                  {Object.keys(types).map(key => {
                    const type = types[key];
                    const checked = query.types.indexOf(key) !== -1;

                    return (
                      <div key={key} className="checkbox">
                        <label htmlFor={'search-type-filter-' + key}>
                          <input
                            type="checkbox"
                            id={'search-type-filter-' + key}
                            checked={checked}
                            onChange={() => this.toggleType(key, !checked)} />
                            {type.label}
                        </label>
                      </div>
                    );
                  })}

                  <hr />

                  {folderish.map(option => {
                    const checked = (option.key === query.folderish);

                    return (
                      <div key={option.key} className="radio">
                        <label htmlFor={'search-folderish-filter-' + option.key}>
                          <input
                            type="radio"
                            id={'search-folderish-filter-' + option.key}
                            checked={checked}
                            onChange={() => this.setQuery({ folderish: option.key })} />
                            {option.label}
                        </label>
                      </div>
                    );
                  })}

                  <If condition={document}>
                    <div>
                      <hr />

                      <div className="checkbox">
                        <label htmlFor="filter-deep">
                          <input
                            type="checkbox"
                            id="filter-deep"
                            checked={query.deep}
                            onChange={::this.setDeep} />
                            Deep (children of children)
                        </label>
                      </div>
                    </div>
                  </If>
                </form>
              </div>
            </div>
          </div>
          <If condition={!this.props.list.ready || utils.typeOf(this.props.list.entries) === 'undefined'}>
            <Spinner />
          <Else />
            <If condition={query.layout === 'grid'}>
              <ViewDocGrid
                currentworkspace={this.props.list.entries}
                history={this.props.history} />
            <Else />
              <ViewDocList
                currentworkspace={this.props.list.entries}
                deleteDocuments={::this.deleteDocuments}
                getDocumentsById={::this.getDocumentsById}
                history={this.props.history}
                isPartialQueryActive={::this.isPartialQueryActive}
                setQuery={::this.setQuery}
                getQuery={::this.getQuery} />
            </If>
          </If>
          <If condition={query.search.length > 0}>
            <a href={this.getExternalSearchUrl()} target="_blank">search on Google instead</a>
          </If>
        </div>
      </section>
      );
  }
}

export default DocListingPage;
