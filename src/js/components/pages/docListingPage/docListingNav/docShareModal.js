import React, { Component, PropTypes } from 'react';
import { Modal, Button } from 'react-bootstrap';
import Select from 'react-select';
import { Dropdown, MenuItem } from 'react-bootstrap';
import nuxeo from 'common/nuxeo/nuxeo';
import './shareWorkspace.scss';

class DocShareModal extends Component {

  static propTypes = {
    permissions: PropTypes.object,
    userDirectory: PropTypes.object,
    document: PropTypes.object.isRequired,
    user: PropTypes.string,
    handleDeletePermission: PropTypes.func,
    handleSubmit: PropTypes.func,
    show: PropTypes.bool,
    onClose: PropTypes.func,
    permissionType: PropTypes.string,
    validate: PropTypes.string,

  }

  state = {
    show: this.props.show,
    onClose: this.props.onClose,
    permissions: this.props.permissions,
    userDirectory: [],
    groupDirectory: [],
    value: '',
    permissionType: 'ReadWrite',
    validate: 'hidden',
    inputClass: 'input-group'

  }

  componentDidMount() {
     Promise.all([
      nuxeo.getUserDirectory(),
      nuxeo.getGroupDirectory()
    ])
      .then((response) => {
        this.setState({
          ready: true,
          userDirectory: response[0],
          groupDirectory: response[1]
        });
      });
  }


  /**
   * get a list of local permissions, groups and users for a document
   * @param  {object} docAcls the permissions object
   * @return {string}         the list of names and type
   */
  getPermissionsList(docAcls, permType) {
    const permissionList = docAcls.map((item) => {
      if (permType === item.name) {
        const eachPermission = item.ace.map((item2, index2) => {
          const username = item2.username;
          const permission = item2.permission;
          let permissionText;
            switch (permission) {
            case 'Everything':
              permissionText = 'Manage everything';
              break;
            case 'ReadWrite':
              permissionText = 'Read & Write';
              break;
            case 'ReadRemove':
              permissionText = 'Read & Remove';
              break;
            case 'Read':
              permissionText = 'View';
              break;
            default:
              permissionText = 'View';
              break;
          }


          const label = this.state.userDirectory.map((item3) => {
            if (username === item3.value) {
              return item3.label;
            }
          });

          const groupLabel = this.state.groupDirectory.map((item4) => {
            if (username === item4.value) {
              return item4.label;
            }
          });

          switch (permType) {
            case 'local':
              return (
                <li key={index2}>
                <a href="#" className="name"><i className="fa fa-user"></i>{label}{groupLabel}</a>
                <button onClick={this.handleDeletePermission.bind(this, this.props.document.uid, username)}><span className="sr-only">remove</span><i className="fa fa-trash-o"></i></button>
                <span className="access">{permissionText}</span>
              </li>
              );
            default:
              return (
                <span className="profile-tag">
                  <a href="profile-page.html" className="name"><i className="fa fa-user"></i> {label}{groupLabel} - <span className="access">{permission}</span></a>
                  </span>
              );
          }

        });
        return eachPermission;
      }
    });
    return permissionList;
  }

  /*eslint no-param-reassign:0*/
  loadOptions(input, callback) {
    const rtn = {
      options: [],
      complete: false
    };

    if (this.state.ready) {
      const fullDirectory = this.state.userDirectory.concat(this.state.groupDirectory);

      rtn.options = fullDirectory;
      rtn.complete = true;
    }

    callback(null, rtn);
  }

permissionLabel(permissionType) {
    switch (permissionType) {
      case 'Everything':
        return <span><i className="fa fa-cog"></i> Can Manage</span>;
      case 'ReadWrite':
        return <span><i className="fa fa-pencil"></i> Can Edit</span>;
      case 'ReadRemove':
        return <span><i className="fa fa-trash"></i> Can Remove</span>;
      case 'Read':
        return <span><i className="fa fa-eye"></i> Can View</span>;
      default:
        return <span><i className="fa fa-eye"></i> Can View</span>;
    }
  }

  handleDeletePermission(docId, user) {
    if (docId === undefined) {
      docId = this.props.document.uid;
    }
    this.props.handleDeletePermission(docId, user);
  }

  handleSubmit(docId, users, permissionType) {
    if (docId === undefined) {
      docId = this.props.document.uid;
    }

    if (users.length <= 0) {
      this.setState({
        validate: 'hint has-error visible',
        inputClass: 'input-group has-error'
      });

    } else {
      this.props.handleSubmit(docId, users, permissionType);
      this.props.onClose(this, 'share');
      this.setState({
        value: '',
        validate: 'hidden',
        inputClass: 'input-group'
      });
    }


  }

  handleChange(event, permissionType) {
    this.setState({
      permissionType: permissionType,
    });
  }

  handleSelectChange(people) {
    this.setState({
      value: people,
      validate: 'hidden',
      inputClass: 'input-group'
    });
  }

  formData(docId, users, permissionType) {
    const data = [{
      path: docId,
      user: users,
      permission: permissionType
    }];
    return data;
  }


  render() {

    const localPermList = this.getPermissionsList(this.props.permissions.acls, 'local');
    const inheritedPermList = this.getPermissionsList(this.props.permissions.acls, 'inherited');
    return (
      <Modal show={this.props.show} onHide={this.props.onClose} id="permissionsmodal" >
      <Modal.Header closeButton>
        <Modal.Title>
          Share with others
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="block advanced collapse in" id="access-block">
          <p>Who has access</p>
          <div className="has-access">
            <ul>
              {localPermList}
            </ul>
          </div>
        </div>
        <div className="block advanced collapse in" id="access-block">
          <p>Inherited access</p>
          <div>
            {inheritedPermList}
          </div>
        </div>
        <div className="block">
          <label htmlFor="invite">People or groups</label>
          <div className={this.state.inputClass}>
            <Select
              asyncOptions={this.loadOptions.bind(this)}
              autoload={false}
              cacheAsyncResults={false}
              className="select2-results"
              multi
              name="form-field-name"
              onChange={this.handleSelectChange.bind(this)}
              placeholder=""
              searchPromptText="Please start typing to search"
              value={this.state.value} />
            <div className="input-group-btn">
              <Dropdown id="dropdown-custom-1" pullRight >
                <Dropdown.Toggle>
                  {this.permissionLabel(this.state.permissionType)}
                </Dropdown.Toggle>
                <Dropdown.Menu onSelect={this.handleChange.bind(this)}>
                  <MenuItem eventKey="Everything" name="Everything" >{this.permissionLabel('Everything')}</MenuItem>
                  <MenuItem eventKey="ReadRemove" name="ReadRemove" >{this.permissionLabel('ReadRemove')}</MenuItem>
                  <MenuItem eventKey="ReadWrite" name="ReadWrite" >{this.permissionLabel('ReadWrite')}</MenuItem>
                  <MenuItem eventKey="Read" name="Read" >{this.permissionLabel('Read')}</MenuItem>
                </Dropdown.Menu>
              </Dropdown>
            </div>
          </div>
          <div className={this.state.validate}>Select a group or person</div>
          <div className="hint">To search for a group or person, please start typing</div>

      </div>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={this.props.onClose}>
          Cancel
        </Button>
        <Button className="btn-primary" onClick={this.handleSubmit.bind(this, this.props.document.uid, this.state.value, this.state.permissionType)}>
          Share
        </Button>
      </Modal.Footer>
    </Modal>
    );
  }
}

export default DocShareModal;
