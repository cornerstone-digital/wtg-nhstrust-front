import React, { Component, PropTypes } from 'react';
import { Button, ButtonGroup, Glyphicon, Dropdown, MenuItem } from 'react-bootstrap';
import './docListingNav.scss';
import DocCreateModal from './docCreateModal';
import DocShareModal from './docShareModal';
import utils from 'common/utils/utils';

class DocListingNav extends Component {

  static propTypes = {
    setQuery: PropTypes.func,
    refreshPage: PropTypes.func,
    isPartialQueryActive: PropTypes.func,
    document: PropTypes.object,
    user: PropTypes.object,
    handleDeletePermission: PropTypes.func,
    handleSubmit: PropTypes.func,
    toggleFavorite: PropTypes.func,
    isPinned: PropTypes.bool,
    history: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired
  }

  state = {
    showAddModal: false,
    showShareModal: false
  };

  close(modalName) {
    switch (modalName) {
      case 'addNew':
        return this.setState({
          showAddModal: false
        });
      case 'share':
        return this.setState({
          showShareModal: false
        });
      default:
        return null;
    }
  }

  open(modalName) {

    switch (modalName) {
      case 'addNew':
        return this.setState({
          showAddModal: true
        });
      case 'share':
        return this.setState({
          showShareModal: true
        });
      default:
        return null;
    }
  }

  canWrite(permission) {
    switch (permission) {
      case 'Everything':
      case 'ReadWrite':
        return true;
      default:
        return false;
    }
  }

  canEverything(permission) {
    return permission === 'Everything';
  }

  refreshPage() {
    return this.props.refreshPage();
  }

  setQuery(event, query) {
    return this.props.setQuery(query);
  }

  layoutOptions = [
    {
      name: 'list',
      icon: <Glyphicon glyph="glyphicon glyphicon-th-list" />
    },
    {
      name: 'grid',
      icon: <Glyphicon glyph="glyphicon glyphicon-th" />
    }
  ]

  orderByOptions = [
    {
      label: 'name (normal)',
      query: {
        grouping: false,
        orderByAttribute: 'dc:title',
        orderByDirection: 'ASC'
      }
    },
    {
      label: 'name (reversed)',
      query: {
        grouping: false,
        orderByAttribute: 'dc:title',
        orderByDirection: 'DESC'
      }
    },
    {
      label: 'type (normal)',
      query: {
        grouping: false,
        orderByAttribute: 'ecm:primaryType',
        orderByDirection: 'ASC'
      }
    },
    {
      label: 'type (reversed)',
      query: {
        grouping: false,
        orderByAttribute: 'ecm:primaryType',
        orderByDirection: 'DESC'
      }
    },
    {
      label: 'status (normal)',
      query: {
        grouping: false,
        orderByAttribute: 'ecm:currentLifeCycleState',
        orderByDirection: 'ASC'
      }
    },
    {
      label: 'status (reversed)',
      query: {
        grouping: false,
        orderByAttribute: 'ecm:currentLifeCycleState',
        orderByDirection: 'DESC'
      }
    },
    {
      label: 'last modified (oldest first)',
      query: {
        grouping: false,
        orderByAttribute: 'dc:modified',
        orderByDirection: 'ASC'
      }
    },
    {
      label: 'last modified (latest first)',
      query: {
        orderByAttribute: 'dc:modified',
        orderByDirection: 'DESC'
      }
    }
  ]

  groupByOptions = [
    {
      label: 'type (normal)',
      query: {
        grouping: true,
        orderByAttribute: 'ecm:primaryType',
        orderByDirection: 'ASC',
      }
    },
    {
      label: 'type (reversed)',
      query: {
        grouping: true,
        orderByAttribute: 'ecm:primaryType',
        orderByDirection: 'DESC',
      }
    },
    {
      label: 'status (normal)',
      query: {
        grouping: true,
        orderByAttribute: 'ecm:currentLifeCycleState',
        orderByDirection: 'ASC',
      }
    },
    {
      label: 'status (reversed)',
      query: {
        grouping: true,
        orderByAttribute: 'ecm:currentLifeCycleState',
        orderByDirection: 'DESC',
      }
    }
  ]

  render() {
    const document = this.props.document;

    if (document) {
      return this.renderToolbarWithParent();
    }

    return this.renderToolbarWithoutParent();
  }

  renderToolbarWithoutParent() {
    return (
      <div className="toolbar inline pull-right">
        <ButtonGroup>
          {this.getDocumentlessActions()}
        </ButtonGroup>
      </div>
    );
  }

  renderToolbarWithParent() {
    const document = this.props.document;

    let permission;
    if (document.type === 'Section') {
      permission = 'Read';
    } else {
      permission = utils.getUserRightsToDoc(document.contextParameters.acls, this.props.user);
    }

    return (
      <span>
        <DocCreateModal
          show={this.state.showAddModal}
          onClose={this.close.bind(this, 'addNew')}
          docTypes={document.contextParameters.allowedDocumentTypes}
          document={document} />
        <DocShareModal
          show={this.state.showShareModal}
          onClose={this.close.bind(this, 'share')}
          document={document}
          permissions={document.contextParameters}
          handleDeletePermission={this.props.handleDeletePermission}
          handleSubmit={this.props.handleSubmit} />
        <div className="toolbar inline pull-right">
          <If condition={document && this.canWrite(permission)}>
            <Button className="blue-btn" name="addNew" onClick={this.open.bind(this, 'addNew')}>
              Add New
            </Button>
          </If>
          <ButtonGroup>
            {this.getDocumentlessActions()}
            <If condition={document}>
              <span>
                <Button
                  onClick={() => this.props.toggleFavorite(this.props.document.uid, !this.props.isPinned)}
                  disabled={utils.typeOf(this.props.isPinned) !== 'boolean'}>
                  <If condition={this.props.isPinned}>
                    <span className="fa fa-thumb-tack selected"></span>
                  <Else />
                    <span className="fa fa-thumb-tack"></span>
                  </If>
                </Button>
                <If condition={this.canEverything(permission)}>
                  <Button name="share" onClick={this.open.bind(this, 'share')}>
                    <span className="fa fa-share-alt"></span>
                  </Button>
                </If>
                <If condition={this.canWrite(permission)}>
                  <Dropdown id="edit-dropdown">
                    <Dropdown.Toggle noCaret>
                      <span className="fa fa-ellipsis-v fa-1x"></span>
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      <MenuItem
                        eventKey="Overview"
                        name="Overview"
                        href={utils.getDocLink(this.props.document)}>
                        <i className="fa fa-file"></i> Overview
                      </MenuItem>
                      <MenuItem
                        eventKey="Edit"
                        name="Edit"
                        href={`${utils.getDocLink(this.props.document)}|edit`}>
                        <i className="fa fa-pencil-square-o"></i> Edit
                      </MenuItem>
                      <If condition={this.canEverything(permission)}>
                        <MenuItem
                          eventKey="History"
                          name="History"
                          href={`${utils.getDocLink(this.props.document)}|history`}>
                          <i className="fa fa-clock-o"></i> History
                        </MenuItem>
                      </If>
                    </Dropdown.Menu>
                  </Dropdown>
                </If>
              </span>
            </If>
          </ButtonGroup>
        </div>
      </span>
    );
  }

  getDocumentlessActions() {
    return (
      <span>
        {this.layoutOptions.map((option, index) => (
          <Button
            key={index}
            eventKey={{layout: option.name}}
            onClick={e => this.setQuery(e, { layout: option.name })}
            className={this.props.isPartialQueryActive({layout: option.name}) ? 'selected' : ''}>
            {option.icon}
          </Button>
        ))}
        <Button onClick={::this.refreshPage}>
          <Glyphicon glyph="glyphicon glyphicon-refresh" />
        </Button>

        <Dropdown id="sort-dropdown">
          <Dropdown.Toggle noCaret>
            <span className="fa fa-sort-alpha-asc"></span>
          </Dropdown.Toggle>
          <Dropdown.Menu>
            <MenuItem header>
              Sort by...
            </MenuItem>

            {this.orderByOptions.map((option, index) => (
              <MenuItem
                key={index}
                eventKey={option.query}
                onSelect={::this.setQuery} >
                <If condition={this.props.isPartialQueryActive(option.query)}>
                  <span className="fa fa-check-square-o"></span>
                <Else />
                  <span className="fa fa-square-o"></span>
                </If>
                {option.label}
              </MenuItem>
            ))}

            <MenuItem divider />

            <MenuItem header>
              Group by...
            </MenuItem>

            {this.groupByOptions.map((option, index) => (
              <MenuItem
                key={index}
                eventKey={option.query}
                onSelect={::this.setQuery} >
                <If condition={this.props.isPartialQueryActive(option.query)}>
                  <span className="fa fa-check-square-o"></span>
                <Else />
                  <span className="fa fa-square-o"></span>
                </If>
                {option.label}
              </MenuItem>
            ))}
          </Dropdown.Menu>
        </Dropdown>

        <Button disabled>
          <span className="fa fa-filter"></span>
        </Button>
      </span>
    );
  }
}

export default DocListingNav;
