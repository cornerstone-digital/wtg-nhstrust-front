import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import utils from 'utils';

const DocCreateModal = ({docTypes, show, onClose, document}) => (
<Modal show={show} onHide={onClose}>
  <Modal.Header closeButton>
    <Modal.Title>
      Create new document
    </Modal.Title>
  </Modal.Header>
  <Modal.Body>
    <p>
      Select the type of document from the list below
    </p>
    <ul className="row document-picker">
      {docTypes.map((item, index) => (
       <li key={index}>
         <a href={`#/new/${item.id}/${document.uid}`} title="">
           <Button>
             <span><i className={utils.getDocIcon(item.id)}></i></span>
             <p>{item.label}</p>
           </Button>
         </a>
       </li>
       ))}
    </ul>
  </Modal.Body>
  <Modal.Footer>
    <Button onClick={onClose}>
      Cancel
    </Button>
  </Modal.Footer>
</Modal>
);

export default DocCreateModal;
