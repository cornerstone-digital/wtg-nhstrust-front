import React, { Component, PropTypes } from 'react';
import Time from 'react-time';
import { connect } from 'react-redux';
import utils from 'utils';
import BulkActionsToolbar from './bulkActionsToolbar';

@connect(state => ({
    user: state.user
}))

class ViewDocList extends Component {

  static propTypes = {
    currentworkspace: PropTypes.array.isRequired,
    deleteDocuments: PropTypes.func,
    getDocumentsById: PropTypes.func,
    listCheckValue: PropTypes.array,
    handleListCheckboxChange: PropTypes.func,
    isPartialQueryActive: PropTypes.func,
    setQuery: PropTypes.func,
    getQuery: PropTypes.func,
    user: PropTypes.object
  };

  state = {
    listCheckValue: {},
    permissions: {},
    stateList: {},
    hasChildren: {},
    aclsList: {}
  }

  getCaretForOrderBy(column) {
    const active = this.props.isPartialQueryActive({
      grouping: false,
      orderByAttribute: column
    });

    if (!active) {
      return null;
    }

    const direction = this.props.isPartialQueryActive({
      orderByDirection: 'ASC'
    });

    return direction ? <span className="fa fa-caret-up"></span> : <span className="fa fa-caret-down"></span>;
  }

  handleColumnClick(column) {
    const reverse = this.props.isPartialQueryActive({
      grouping: false,
      orderByAttribute: column,
      orderByDirection: 'ASC'
    });

    this.props.setQuery({
      grouping: false,
      orderByAttribute: column,
      orderByDirection: reverse ? 'DESC' : 'ASC'
    });
  }

  handleListCheckboxChange(doc, event) {
    const listCheckValue = {...this.state.listCheckValue};

    if (event.target.checked) {
      listCheckValue[doc._id] = true;
    } else {
      delete listCheckValue[doc._id];
    }

    this.setState({
      listCheckValue: listCheckValue
    });
  }

  resetCheckValue() {
    this.setState({
      listCheckValue: {}
    });
  }

  renderWithoutGroups(items) {
    return (
      <tbody>
        {this.renderRows(items)}
      </tbody>
    );
  }

  renderGroups(groups, groupByAttribute) {
    return Object.keys(groups).map(key => {
      return this.renderGroup(groups[key], key, groupByAttribute);
    });
  }

  renderGroup(items, group, groupByAttribute) {
    let groupLabel;

    switch (groupByAttribute.join('.')) {
      case 'type':
        groupLabel = utils.getFileTypeLabel(group);
        break;
      default:
        groupLabel = group;
    }

    return (
      <tbody key={'group-' + group}>
        <tr>
          <td colSpan="7">
            {groupLabel}
          </td>
        </tr>
        {this.renderRows(items)}
      </tbody>
    );
  }

  renderRows(items) {
    return items.map(::this.renderRow);
  }

  renderRow(item, index) {
    return (
      <tr key={'row-' + index} type={item.type}>
        <td>
          <input
            type="checkbox"
            checked={!!this.state.listCheckValue[item._id]}
            value={item._id}
            onChange={this.handleListCheckboxChange.bind(this, item)} />
        </td>
        <td>
          <a
            href={utils.getDocLink(item)}
            title={item._source['dc:title']}
            alt={item._source['dc:title']}>
              <span className={utils.renderIcon(item)}></span>
          </a>
        </td>
        <td>
          <a
            href={utils.getDocLink(item)}
            title={item._source['dc:title']}
            alt={item._source['dc:title']}>
            <If condition={item.highlight}>
              <span dangerouslySetInnerHTML={{ __html: item.highlight['dc:title.fulltext'][0] }} />
            <Else />
              {item._source['dc:title']}
            </If>
          </a>
        </td>
        <td>
          {utils.getFileTypeLabel(item._source['ecm:primaryType'])}
        </td>
        <td>
          {item._source['dc:lastContributor']}
        </td>
        <td>
          {item._source['ecm:currentLifeCycleState']}
        </td>
        <td>
          <Time value={item._source['dc:modified']} format="Do MMM 'YY" />
        </td>
      </tr>
    );
  }

  render() {
    let items = [];

    const query = this.props.getQuery();
    if (this.props.currentworkspace.length < 1) {
      items = (
        <tbody>
          <tr>
            <td colSpan="7">
              <h4 className="text-muted text-center state-empty"><em>No documents found.</em></h4>
            </td>
          </tr>
        </tbody>
      );
    } else if (query.grouping === true && query.groupByAttribute) {

      const groups = utils.groupBy(this.props.currentworkspace, item => {
        let target = item;

        for (const i in query.groupByAttribute) {
          if (query.groupByAttribute.hasOwnProperty(i)) {
            target = target[query.groupByAttribute[i]];
          }
        }

        return target;
      });

      items = this.renderGroups(groups, query.groupByAttribute);
    } else {
      items = this.renderWithoutGroups(this.props.currentworkspace);
    }

    return (

      <div className="fluid-container workspace search-results">
        <div className="col-lg-12">
          <div className="button-toolbar hidden">
            <button type="button" className="btn btn-default">
              Copy
            </button>
            <button type="button" className="btn btn-default">
              Paste
            </button>
            <button type="button" className="btn btn-default">
              Download
            </button>
          </div>
          <div className="list">
            <table className="table table-hover">
              <thead>
                <tr>
                  <td className="small">
                    <label htmlFor="select-all" className="sr-only">
                      Select all
                    </label>
                  </td>
                  <td className="small"></td>
                  <td id="name" onClick={() => this.handleColumnClick('dc:title')}>
                    Name {this.getCaretForOrderBy('dc:title')}
                  </td>
                  <td id="type" onClick={() => this.handleColumnClick('ecm:primaryType')}>
                    Type {this.getCaretForOrderBy('ecm:primaryType')}
                  </td>
                  <td id="owner">
                    Owner
                  </td>
                  <td id="status" onClick={() => this.handleColumnClick('ecm:currentLifeCycleState')}>
                    Status {this.getCaretForOrderBy('ecm:currentLifeCycleState')}
                  </td>
                  <td id="last-modified" onClick={() => this.handleColumnClick('dc:modified')}>
                    Last modified {this.getCaretForOrderBy('dc:modified')}
                  </td>
                </tr>
              </thead>
              {items}
            </table>
          </div>
        </div>
        <div className="col-lg-12">
          <BulkActionsToolbar
            deleteDocuments={this.props.deleteDocuments}
            getDocumentsById={this.props.getDocumentsById}
            list={this.state.listCheckValue}
            reset={::this.resetCheckValue} />
        </div>
      </div>
      );
  }
}

export default ViewDocList;
