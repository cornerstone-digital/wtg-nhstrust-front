import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import utils from 'utils';

@connect(state => ({
    user: state.user
}))

class BulkActionsToolbar extends Component {

  static propTypes = {
    user: PropTypes.object,
    deleteDocuments: PropTypes.func,
    getDocumentsById: PropTypes.func,
    list: PropTypes.object, // so far, this needs only be an array (with what are now keys), but maybe future functionality will use the object value
    reset: PropTypes.func
  }

  state = {
    loading: true
  }

  componentDidMount() {
    this.fetchData();
  }

  componentWillReceiveProps(nextProps) {
    this.fetchData(nextProps);
  }

  fetchData(nextProps) {
    const props = nextProps || this.props;

    if (Object.keys(props.list).length < 1) {
      return this.setState({
        loading: false,
        expandedList: []
      });
    }

    this.setState({
      loading: true
    });

    return this.props.getDocumentsById(Object.keys(props.list))
      .then(response => {

        return this.setState({
          loading: false,
          expandedList: response.entries
        });
      });
  }

  filterDocsByDeletable(docs) {
    return docs.filter(doc => {

      // cannot delete an item which has children
      if (doc.lockOwner) {
        return false;
      }

      // cannot delete an item which has children
      if (doc.contextParameters.children.entries.length > 0) {
        return false;
      }

      // cannot delete if item has been approved
      if (doc.state === 'approved') {
        return false;
      }

      // cannot delete if user doesn't have permission
      const permission = utils.getUserRightsToDoc(doc.contextParameters.acls, this.props.user);
      if (['Everything', 'ReadRemove'].indexOf(permission) === -1) {
        return false;
      }

      return true;
    });
  }

  canDelete() {
    const selected = Object.keys(this.props.list);

    if (selected.length < 1) {
      return false;
    }

    const deletable = this.filterDocsByDeletable(this.state.expandedList);

    return deletable.length === selected.length;
  }

  handleDelete() {
    this.props.reset();
    return this.props.deleteDocuments(Object.keys(this.props.list));
  }

  render() {
    return (
      <div className="bulk-actions-toolbar">
        <If condition={this.state.loading}>
          <div>loading...</div>
        <Else />
          <button
            className="pull-left blue-btn btn btn-default"
            onClick={this.handleDelete.bind(this)}
            disabled={!this.canDelete()}>
            Delete
          </button>
        </If>
      </div>
    );
  }
}

export default BulkActionsToolbar;
