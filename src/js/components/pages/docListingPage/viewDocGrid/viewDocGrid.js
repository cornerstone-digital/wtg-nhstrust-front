import React, { Component, PropTypes } from 'react';
import utils from 'utils';

class ViewDocGrid extends Component {

  static propTypes = {
    currentworkspace: PropTypes.array.isRequired,
  };

  /**
   * Checkes if this document has the given facet
   * @param  {string}  facet a single facet
   * @return {Boolean}        returns true if there is a match
   */
  isFolderish(facets) {
    return utils.isFolderish(facets);
  }

  /**
   * Redirects to document
   */
  goToDoc(doc) {
    window.location.href = utils.getDocLink(doc);
  }

  /**
   * Renders a thumbail view of a document which is NOT a folderish doc
   */
  renderFiles() {
    const documents = this.props.currentworkspace.filter(doc => !this.isFolderish(doc._source['ecm:mixinType']));
    const fileExtensions = ['jpg', 'png', 'gif', 'tif'];
    const docExtensions = ['xls', 'pdf', 'doc'];

    return documents.map((item, index) => {

      if (item._source['file:content']) {
        const extension = (utils.getFileExtension(item._source['file:filename']) || '').toLowerCase();
        const hasExtension = fileExtensions.indexOf(extension) !== -1;
        const hasDocExtension = docExtensions.indexOf(extension) !== -1;

        return (
            <div
              className="item"
              style={{height: '156'}}
              onClick={this.goToDoc.bind(this, item)}
              key={index}>
              <span>
                  <div className="show-preview" data-toggle="modal" data-target=".doc-preview"></div>
                  <If condition={hasDocExtension}>
                      <span className={utils.getFileIcon(item._source['file:filename'])}/>
                      <Else/>
                      <If condition={hasExtension && item._source['ecm:primaryType'] !== 'TrustPolicy'}>
                          <span className="image-container">
                              <span className="image"><img src={(utils.getImageUrls(item) || {}).thumbnail} alt={item._source['dc:title']}/></span>
                          </span>
                          <Else/>
                          <span className={utils.getDocIcon(item._source['ecm:primaryType'])} alt={item._source['dc:title']}/>
                      </If>
                  </If>
                  <div className="info">
                      <p>
                          <a href={utils.getDocLink(item)}>
                              {item._source['dc:title']}
                          </a>
                      </p>
                      <div className="fileInfo">
                        <span className="pull-left">{utils.bytesToSize(item._source['common:size']) || ''}</span>
                        <span className="pull-right">{extension.toUpperCase()}</span>
                      </div>
                  </div>
              </span>
          </div>
        );
      }

      return (
        <div
          className="item"
          style={{height: '156'}}
          key={index}
          onClick={this.goToDoc.bind(this, item)}>
          <span>
            <span className={utils.getDocIcon(item._source['ecm:primaryType'])}></span>
            <div className="info">
              <p>
                <a href={utils.getDocIcon(item._source['ecm:primaryType'])}>
                  {item._source['dc:title']}
                </a>
              </p>
              <div className="fileInfo">
                <span className="pull-left">{item._source['common:size'] || ''}</span>
                <span className="pull-right">{utils.getFileExtension(item._source['file:filename']) || ''}</span>
              </div>
            </div>
          </span>
        </div>
      );
    });
  }

  renderGrid(documents) {
    if (documents.length !== 0) {
      return (
        <div className="folders">
        {documents.filter(doc => this.isFolderish(doc._source['ecm:mixinType'])).map((item, index) => (
          <div
            key={index}
            className="item"
            style={{height: '70px'}}
            onClick={this.goToDoc.bind(this, item)}>
            <span>
              <a
                href={utils.getDocLink(item)}
                title={item._source['dc:title']}
                alt={item._source['dc:title']}>{item._source['dc:title']}</a>
            </span>
          </div>
          ))}
        </div>
      );
    }

    if (documents.length === 0) {
        return (
          <table className="table table-hover">
            <tbody ref="documentList">
              <tr>
                <td colSpan="6">
                  <h4 className="text-muted text-center state-empty"><em>No documents found.</em></h4>
                </td>
              </tr>
            </tbody>
          </table>
        );
    }
  }

  render() {
    const documents = this.props.currentworkspace;
    return (
      <div className="fluid-container workspace search-results">
        <div className="col-lg-12">
          <div className="row grid">
          {/*  <div className="folders">
              {documents
                 .filter(doc => this.isFolderish(doc._source['ecm:mixinType']))
                 .map((item, index) => (
                 <div
                   key={index}
                   className="item"
                   style={{height: '70px'}}
                   onClick={this.goToDoc.bind(this, item)}>
                   <span>
                   <a
                     href={utils.getDocLink(item)}
                     title={item._source['dc:title']}
                     alt={item._source['dc:title']}>{item._source['dc:title']}</a>
                    </span>
                 </div>
                 ))}
            </div>*/}
            {this.renderGrid(documents)}
            <div className="files">
              {this.renderFiles()}
            </div>
          </div>
        </div>
      </div>
      );
  }
}

export default ViewDocGrid;
