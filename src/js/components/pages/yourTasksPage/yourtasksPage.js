import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Breadcrumbs from 'common/breadcrumbs/breadcrumbs';
import PageHeader from 'common/pageHeader/pageHeader';
import Spinner from 'common/spinner/spinner.js';
import TaskList from 'pages/yourTasksPage/taskList/taskList';
import nuxeo from 'common/nuxeo/nuxeo';
import { throwError } from 'actions/error';

@connect(state => ({
  user: state.user
}), {throwError})

class YourTasksPage extends Component {

  static propTypes = {
    route: PropTypes.object.isRequired,
    throwError: PropTypes.func
  }

  componentDidMount() {
    nuxeo.getTasksWaiting()
      .then((response) => {
        this.setState({
          taskswaiting: response.entries
        });
      })
      .catch((error) => {
        this.props.throwError(error);
      });
  }

  render() {

    if (!this.state) {
      return <Spinner />;
    }

    return (
      <section>
        <Breadcrumbs breadcrumbData={[{route: this.props.route.path, title: 'Your Tasks'}]} breadcrumbRoot={this.props.route.parent} />
        <PageHeader pageTitle="Your Tasks" />
        <div id="page-body">
          <div className="filters-container">
            <div className="panel panel-primary panel-filters">
              <div className="panel-heading">
                <h2 className="panel-title">Filters</h2>
              </div>
              <div className="panel-body">
              </div>
            </div>
          </div>
          <div className="fluid-container task-list">
            <div className="col-lg-12">
              <TaskList tasks={this.state.taskswaiting} />
            </div>
          </div>
        </div>
        <div className="clearfix"></div>
      </section>
      );
  }

}

export default YourTasksPage;
