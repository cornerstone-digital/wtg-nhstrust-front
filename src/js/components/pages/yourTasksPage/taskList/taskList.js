import React, {Component, PropTypes} from 'react';
import Time from 'react-time';
import 'pages/yourTasksPage/taskList/taskList.scss';

class TaskList extends Component {

  static propTypes = {
    tasks: PropTypes.array.isRequired,
  };

  isDue(dueDate) {
    const currentDate = new Date().getTime();
    const dueDateUTC = new Date(dueDate).getTime();
    if (dueDateUTC >= currentDate) {
      return false;
      }
    return true;
  }

  render() {
    const totalNumber = this.props.tasks.length;
    const items = this.props.tasks.map((item, index) => {
      return (
        <li className={this.isDue(item.dueDate) ? 'list-group-item upcoming' : 'list-group-item'} key={index}>
          <div className="row">
              <div className="col-md-6">
                  <h3>{item.documentTitle}</h3>
                  <span>{item.directive}</span>
              </div>
              <div className="col-md-4">
                <p className="date-wrapper">Due: <Time value={item.dueDate} format="Do MMM 'YY" /></p><br />
              </div>
              <div className="col-md-2">
                <a href={`#doc/${item.docref}`} >Take action</a>
              </div>
          </div>
        </li>
      );
    });

    return (
      <div className="panel panel-primary panel-task-list">
        <div className="panel-heading">
            <h3 className="panel-title">Tasks ({totalNumber})</h3>
        </div>
        <ul className="list-group">
          {items}
        </ul>
      </div>
    );
  }
}

export default TaskList;
