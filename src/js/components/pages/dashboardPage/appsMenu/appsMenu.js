import React, { Component, PropTypes } from 'react';
import AppList from 'pages/yourAppsPage/appList/appList';

class AppsMenu extends Component {

  static propTypes = {
    callApps: PropTypes.object
  };

  state = {
    size: 8
  }

  getApps() {
    if (this.props.callApps) {
      return this.props.callApps.entries.slice(0, this.state.size);
    }

    return [];
  }

  render() {
    return (
      <div className="panel panel-primary panel-myApps  no-bg">
        <div className="panel-heading">
          <h3 className="panel-title">Your Apps</h3>
          <a href="#/yourapps" className="view-all" title="view all"></a>
        </div>
        <div className="panel-body">
          <AppList items={this.getApps()} />
        </div>
      </div>
      );
  }
}

export default AppsMenu;
