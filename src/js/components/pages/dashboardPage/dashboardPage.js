import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import QuickLinks from 'pages/dashboardPage/quickLinks/quickLinks';
import AppsMenu from 'pages/dashboardPage/appsMenu/appsMenu';
import PinnedWorkspaces from 'pages/docManagementPage/pinnedWorkspaces/pinnedWorkspaces';
import Favorites from 'pages/docManagementPage/favorites/favorites';
import Help from 'pages/dashboardPage/help/help';
import YourTasks from 'pages/docManagementPage/yourtasks/yourtasks';
import News from 'pages/dashboardPage/news/news';
import nuxeo from 'common/nuxeo/nuxeo';

import Spinner from 'common/spinner/spinner.js';
import { throwError } from 'actions/error';

@connect(state => ({
  user: state.user
}), { throwError })

class DashboardPage extends Component {

  static propTypes = {
    throwError: PropTypes.func
  };

  componentDidMount() {
    Promise.all([
      nuxeo.getFavorites(),
      nuxeo.getTasksWaiting(),
      nuxeo.getNews(),
      nuxeo.getHelp(),
      nuxeo.getAllApps(),
      nuxeo.getSectionIds(),
    ])
      .then(response => this.setState({
        favorites: response[0].entries,
        taskswaiting: response[1].entries,
        news: response[2],
        helpDocs: response[3],
        apps: response[4],
        sectionLinks: response[5].entries
      }))
      .catch((error) => {
        this.props.throwError(error);
      });
  }

  render() {

    if (!this.state) {
      return <Spinner />;
    }

    return (
      <section>
        <div id="page-body">
          <div className="col-lg-8">
            <QuickLinks sectionLinks={this.state.sectionLinks} />
            <PinnedWorkspaces favorites={this.state.favorites} />
            <Favorites favorites={this.state.favorites} />
            <Help documents={this.state.helpDocs} />
          </div>
          <div className="col-lg-4">
            <AppsMenu callApps={this.state.apps}/>
            <YourTasks tasks={this.state.taskswaiting} />
            <News newsList={this.state.news} />
          </div>
        </div>
        <div className="clearfix"></div>
      </section>
    );
  }

}

export
default DashboardPage;
