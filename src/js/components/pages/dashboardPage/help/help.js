import React, {Component, PropTypes } from 'react';

class Help extends Component {

  static propTypes = {
    documents: PropTypes.array.isRequired
  };

  render() {

    var items = this.props.documents.map((item, index) => {
      return (
        <li className="col-sm-6 col-xs-12" key={index}>
          <a href="#"> {item}</a>
        </li>
      );
    });

    return (
      <div className="panel panel-primary panel-help">
        <div className="panel-heading">
          <h3 className="panel-title">Help</h3>
        </div>
        <div className="panel-body">
          <ul>
            {items}
          </ul>
        </div>
      </div>
    );
  }
}

export default Help;
