import React, { Component, PropTypes } from 'react';

class QuickLinks extends Component {

  static propTypes = {
    sectionLinks: PropTypes.array.isRequired,
  };

  getSectionsPathsByName(sectionName, sectionLinks) {
    let uid;
    sectionLinks.forEach((item) => {
      if (item.title === sectionName) {
        uid = item.uid;
      } 
    });
    return uid;
  }

  render() {

    const formPath = this.getSectionsPathsByName('Forms', this.props.sectionLinks);
    const policiesPath = this.getSectionsPathsByName('Policies', this.props.sectionLinks);

    return (
      <div className="quick-links" id="quick-links">
        <ul>
          <li>
            <a href={`#/section/${formPath}`}>
              <span className="icon">
                <img src="assets/img/i-forms.png" alt="Forms" />
              </span>
              <p>Forms</p>
            </a>
          </li>
          <li>
            <a href={`#/policies/${policiesPath}`}>
              <span className="icon">
                <img src="assets/img/i-policies.png" alt="Policies &amp; Procedures" />
              </span>
              <p>Policies &amp; Procedures</p>
            </a>
          </li>
          <li>
            <a href="#">
              <span className="icon">
                <img src="assets/img/i-holidays.png" alt="Holidays &amp; Expenses" />
              </span>
              <p>Annual Leave &amp; Expenses</p>
            </a>
          </li>
          <li>
            <a href="#">
              <span className="icon">
                <img src="assets/img/i-people.png" alt="People &amp; Places" />
              </span>
              <p>People &amp; Places</p>
            </a>
          </li>
          <li>
            <a href="#">
              <span className="icon">
                <img src="assets/img/i-training.png" alt="Room Booker" />
              </span>
              <p>Training</p>
            </a>
          </li>
        </ul>
      </div>
      );
  }
}

export default QuickLinks;
