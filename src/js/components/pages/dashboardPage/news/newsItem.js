import React from 'react';
import Time from 'react-time';
/**
 * Template for a basic news item
 * @type {object}
 */
const NewsItem = ({item}) => (
<div>
  <a className="item" href={`#/info/${item.uid}`}>
    <div className="date">
    <span className="large"><Time value={item.created} format="D" /></span>
    <span className="small"><Time value={item.created} format="MMM 'YY" /></span>
    </div>
    <div className="description">
      <h2>{item.title}</h2>
      <p> {item.description} </p>
    </div>
  </a>
</div>
);

export default NewsItem;
