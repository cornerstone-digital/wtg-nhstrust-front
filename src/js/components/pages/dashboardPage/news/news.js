import React from 'react';
import NewsItem from './newsItem';
import FeaturedNewsItem from './featuredNewsItem';

/**
 * Takes a list of news items and renders them
 * @param  {array} {newsList}) an list of documents
 *                             inside the news section

 * @return {object}            the news panel
 */
const News = ({newsList}) => {

  const featuredList = newsList
    .filter(newsItem => newsItem.featured)
    .sort((prev, next) => new Date(next.created) - new Date(prev.created))
    .filter((item, index) => index < 1)
    .map((newsItem, index) => <FeaturedNewsItem key={index} item={newsItem} />);

  const standardNewsList = newsList
    .filter(newsItem => !newsItem.featured)
    .sort((prev, next) => new Date(next.created) - new Date(prev.created))
    .filter((item, index) => index < 3)
    .map((newsItem, index) => <NewsItem key={index} item={newsItem} />);

  return (
    <div className="panel panel-primary panel-news">
      <div className="panel-heading">
        <h3 className="panel-title">News</h3>
      </div>
      <div className="panel-body no-padding">
        {featuredList}
        {standardNewsList}
      </div>
    </div>
    );

};

export default News;
