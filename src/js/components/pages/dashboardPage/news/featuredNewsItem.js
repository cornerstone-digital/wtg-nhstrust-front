import React from 'react';

/**
   * Template for a featured news item
   * which includes a header image
   * @type {object}
   */
const FeaturedNewsItem = ({item}) => (
<div className="featured">
  <a href={`#/info/${item.uid}`} className="thumb"><img src={item.header ? item.header.data : 'assets/img/featured-news.jpg'} alt="News Title" /></a>
  <div className="description">
    <h2><a href={`#/info/${item.uid}`}>{item.title}</a></h2>
    <p> {item.description} </p>
  </div>
</div>
);

export default FeaturedNewsItem;
