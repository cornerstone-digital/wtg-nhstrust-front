import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import nuxeo from 'common/nuxeo/nuxeo';

import Breadcrumbs from 'common/breadcrumbs/breadcrumbs';
import PageHeader from 'common/pageHeader/pageHeader';
import Spinner from 'common/spinner/spinner.js';
import DocNav from './docNav/docNav';
import utils from 'common/utils/utils';

import {throwError} from 'actions/error';

@connect(state => ({
    user: state.user
}), {throwError})

class DocContainer extends Component {

  static propTypes = {
    document: PropTypes.object.isRequired,
    params: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
    children: PropTypes.object.isRequired,
    throwError: PropTypes.func
  }

  componentDidMount() {
    this.fetchData();
  }

  componentWillReceiveProps(nextProps) {
    this.fetchData(nextProps);
  }

  fetchData(nextProps) {
    const props = nextProps || this.props;

    this.setState({
      document: props.document,
      breadcrumbs: props.document.contextParameters.breadcrumb.entries,
      isLocked: !!props.document.lockCreated,
    });

    nuxeo.getPinnedStatus(props.document.uid)
      .then((response) => {
        this.setState({
          isPinned: response
        });
      })
      .catch((error) => {
        props.throwError(error);
      });
  }

  componentWillUnmount() {
    this.isUnmounted = true;
  }

  handleDeletePermission(docId, user) {
    nuxeo.deletePermission(docId, user)
      .then((response) => {
        this.setState({
          document: response
        });
      });
  }

  handleSubmit(docId, users, permissionType) {
    const usersArray = users.split(',');
    nuxeo.addMultiplePermissions(docId, usersArray, permissionType)
      .then((response) => {
        this.setState({
          document: response
        });
      });
  }

  lockPage(user, path) {
    nuxeo.lockDocument(user, path)
      .then((res) => {
        this.setState({
          isLocked: true,
          document: res
        });
      });
  }

  unlockPage(path) {
    nuxeo.unlockDocument(path)
      .then((res) => {
        this.setState({
          isLocked: false,
          document: res
        });
      });
  }

  setFavorite(docId) {
    nuxeo.addToFavorites(docId)
      .then(() => {
        this.setState({
          isPinned: true
        });
      });
  }

  unsetFavorite(docId) {
    nuxeo.removeFromFavorites(docId)
      .then(() => {
        this.setState({
          isPinned: false
        });
      });
  }

  render() {
    if (!this.state) {
      return <Spinner />;
    }

    const breadcrumb = this.props.children.props.route.breadCrumbTitle;
    const breadcrumbTrail = this.state.breadcrumbs.concat(breadcrumb ? [{title: breadcrumb}] : []);

    return (
      <section>
        <Breadcrumbs breadcrumbData={breadcrumbTrail} breadcrumbRoot={this.props.route.parent} />
        <PageHeader pageTitle={this.state.document.title}>
          <DocNav
            document={this.state.document}
            docPath={this.state.document.path}
            docType={this.state.document.type}
            permissions={this.state.document.contextParameters}
            published={utils.checkPublishedDoc(this.state.document.facets)}
            handleDeletePermission={this.handleDeletePermission.bind(this)}
            handleSubmit={this.handleSubmit.bind(this)}
            userRights={utils.getUserRightsToDoc(this.state.document.contextParameters.acls, this.props.user)}
            userLock={this.lockPage.bind(this)}
            userUnlock={this.unlockPage.bind(this)}
            lockOwner={this.state.document.lockOwner}
            isLocked={this.state.isLocked}
            setFav={this.setFavorite.bind(this)}
            unsetFav={this.unsetFavorite.bind(this)}
            isPinned={this.state.isPinned} />
        </PageHeader>
        {React.cloneElement(this.props.children, {document: this.state.document})}
      </section>
      );
  }

}

export default DocContainer;
