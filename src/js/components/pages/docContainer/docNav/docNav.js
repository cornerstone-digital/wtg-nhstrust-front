import React, { Component, PropTypes } from 'react';
import { Button, ButtonGroup } from 'react-bootstrap';
import { Dropdown, MenuItem } from 'react-bootstrap';
import DocShareModal from '../../docListingPage/docListingNav/docShareModal';
import { connect } from 'react-redux';
import utils from 'common/utils/utils';


@connect(state => ({
    user: state.user
}))

class DocNav extends Component {

  static propTypes = {
    document: PropTypes.object.isRequired,
    permissions: PropTypes.object,
    handleDeletePermission: PropTypes.func,
    handleSubmit: PropTypes.func,
    docType: PropTypes.string,
    published: PropTypes.bool,
    userRights: PropTypes.string,
    userLock: PropTypes.func,
    userUnlock: PropTypes.func,
    lockOwner: PropTypes.string,
    user: PropTypes.object,
    docPath: PropTypes.string,
    isLocked: PropTypes.bool.isRequired,
    setFav: PropTypes.func,
    unsetFav: PropTypes.func,
    isPinned: PropTypes.bool
  }

  state = {
    showAddModal: false,
    showShareModal: false
  };

  close(modalName) {
    switch (modalName) {
      case 'addNew':
        return this.setState({
          showAddModal: false
        });
      case 'share':
        return this.setState({
          showShareModal: false
        });
      default:
        return null;
    }
  }

  open(modalName) {

    switch (modalName) {
      case 'addNew':
        return this.setState({
          showAddModal: true
        });
      case 'share':
        return this.setState({
          showShareModal: true
        });
      default:
        return null;
    }
  }

  canRead(permission) {
    return permission === 'read';
  }

  canWrite(permission) {
    switch (permission) {
      case 'Everything':
        return true;
      case 'ReadWrite':
        return true;
      case 'Write':
        return true;
      default:
        return false;
    }
  }

  canManage(permission) {
    return permission === 'Everything';
  }

  canEditDocument(permission) {

    if (this.props.isLocked && this.props.lockOwner !== this.props.user.id) {
      return false;
    }

    if (this.canWrite(permission) && !this.props.isLocked) {
      return true;
    }
  }

  defineToolbar(permission) {

    const lockedOwner = this.props.lockOwner;

    return (
      <div className="toolbar inline pull-right">
        <ButtonGroup>
          <If condition={!this.props.isLocked}>
            <Button name="doc-rid" onClick={this.props.userLock.bind(this, this.props.user.id, this.props.docPath)}>
              <span className="fa fa-lock"></span>
            </Button>
          </If>
          <If condition={this.props.isLocked && lockedOwner !== this.props.user.id}>
            <Button name="doc-rid">
              <span className="fa fa-lock active"></span>
            </Button>
          </If>
          <If condition={this.props.isLocked && lockedOwner === this.props.user.id}>
            <Button name="doc-rid" onClick={this.props.userUnlock.bind(this, this.props.docPath)}>
              <span className="fa fa-lock active"></span>
            </Button>
          </If>
          <If condition={this.props.isPinned}>
            <Button onClick={this.props.unsetFav.bind(this, this.props.docPath)}>
              <span className="fa fa-thumb-tack selected"></span>
            </Button>
            <Else />
            <Button onClick={this.props.setFav.bind(this, this.props.docPath)}>
              <span className="fa fa-thumb-tack"></span>
            </Button>
          </If>
          <If condition={this.canManage(permission)}>
            <Button name="doc-share" onClick={this.open.bind(this, 'share')}>
              <span className="fa fa-share-alt"></span>
            </Button>
          </If>
          <Dropdown id="doc-more">
            <Dropdown.Toggle noCaret>
              <span className="fa fa-ellipsis-v fa-1x"></span>
            </Dropdown.Toggle>
            <Dropdown.Menu id="doc-more">
              <MenuItem
                eventKey="Overview"
                name="Overview"
                href={utils.getDocLink(this.props.document)}>
                <i className="fa fa-file"></i> Overview
              </MenuItem>
              <If condition={this.canEditDocument(permission)}>
                <MenuItem
                  eventKey="Edit"
                  name="Edit"
                  href={`${utils.getDocLink(this.props.document)}|edit`}>
                  <i className="fa fa-pencil-square-o"></i> Edit
                </MenuItem>
              </If>
              <If condition={this.canManage(permission)}>
                <MenuItem
                  eventKey="History"
                  name="History"
                  href={`${utils.getDocLink(this.props.document)}|history`}>
                  <i className="fa fa-clock-o"></i> History
                </MenuItem>;
              </If>
            </Dropdown.Menu>
          </Dropdown>
        </ButtonGroup>
      </div>
      );
  }

  render() {
    return (
      <div>
        <DocShareModal
          show={this.state.showShareModal}
          onClose={this.close.bind(this, 'share')}
          document={this.props.document}
          permissions={this.props.permissions}
          onSubmit={this.props.handleSubmit}
          handleDeletePermission={this.props.handleDeletePermission}
          handleSubmit={this.props.handleSubmit} />
        <If condition={!this.props.published}>
          {this.defineToolbar(this.props.userRights)}
        </If>
      </div>
      );
  }
}

export default DocNav;
