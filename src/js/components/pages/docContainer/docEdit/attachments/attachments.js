import React, { PropTypes, Component } from 'react';
import Dropzone from 'common/dropzone/dropzone';
import TextTruncate from 'common/text-truncate/text-truncate';
import './attachments.scss';


class Attachments extends Component {

  static propTypes = {
    onUpload: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired,
    layout: PropTypes.object.isRequired,
    initialFiles: PropTypes.array
  }


  /**
   * Fetches a label from a Nuxeo Studio layout (if it exists)
   * @return {string} A label if found - or a default label
   */
  getLabel(widgets) {

    const filesWidget = widgets.filter(widget => widget.name === 'files')[0];

    if (!filesWidget) {
      return 'Edit Attachments';
    }

    if (filesWidget) {
      return filesWidget.labels.any;
    }
  }

  handleRemoveFile(fileIndex) {
    this.props.onRemove(fileIndex);
  }

  render() {
    return (
      <div className="panel panel-primary edit-attachments">
        <div className="panel-heading">
          <h3 className="panel-title">{this.getLabel(this.props.layout.widgets)}</h3>
        </div>
        <div className="panel-body">
          <Dropzone name="attachments" uploadHandler={this.props.onUpload} />
          <If condition={this.props.initialFiles && this.props.initialFiles.length}>
            <div>
              <h4>Current Attachments</h4>
              <div className="row">
                <hr/>
              </div>
              <ul className="attachment-list">
                {this.props.initialFiles.map((item, index) => (
                 <li key={index}>
                   <ul className="list-inline">
                     <li style={{width: '80%'}}>
                       <a href={item.file.data}>
                         <TextTruncate line={1} truncateText="…" text={item.filename} />
                       </a>
                     </li>
                     <li style={{width: '20%'}}>
                       <button className="remove" onClick={this.handleRemoveFile.bind(this, index)}>
                         <i className="fa fa-trash-o"></i>
                       </button>
                     </li>
                   </ul>
                 </li>
                 ))}
              </ul>
            </div>
          </If>
        </div>
      </div>
      );
  }

}

export default Attachments;
