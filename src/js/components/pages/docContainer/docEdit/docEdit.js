import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Attachments from './attachments/attachments';
import Spinner from 'common/spinner/spinner.js';
import FormBuilder from 'common/formBuilder/formBuilder.js';

import nuxeo from 'common/nuxeo/nuxeo';
import { throwError } from 'actions/error';

@connect(state => ({
  user: state.user
}), {throwError})

class DocEdit extends Component {

  static propTypes = {
    document: PropTypes.object.isRequired,
    params: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
    throwError: PropTypes.func
  }

  state = {
    loading: true,
    attachments: []
  }

  state = {
    loading: true,
    attachments: []
  }

  componentDidMount() {
    const doc = this.props.document;

    this.setState({
      document: doc
    });

    return Promise.all([
      nuxeo.getSchemas(doc.type),
      nuxeo.getLayout(`layout@${doc.type}-edit`)
    ])
    .then(data => {
      this.setState({
        schemas: data[0].schemas.map(schema => schema.name),
        layout: data[1],
        loading: false
      });
    })
    //Add error logic here
    .catch(e => console.error(e));
  }

  /**
   * Adds a file batch to the `Attachments` panel
   * @param  {array} fileBatch a object containing the
   *                             batchId and fileIndex
   */
  handleAddNewAttachments(fileBatch) {
    this.setState({
      attachments: this.state.attachments.concat(fileBatch)
    });
  }

  /**
   * Removes files from the files:files property on a document
   * @param  {integer} fileIndex The index in the file array
   */
  handleRemoveExisitngAttachments(fileIndex) {

    const doc = Object.assign({}, this.state.document);
    doc.properties['files:files'].splice(fileIndex, 1);

    if (!doc.properties['files:files'].length) {
      doc.properties['files:files'] = [];
    }

    this.setState({
      document: doc
    });
  }

  /**
   * On form submit - lets update a document!
   * @param  {objext} formData data from FormBuilder
   */
  handleSubmit(formData) {
    const newVals = { properties: formData };

    //Add any new attachments to the document
    if (this.state.attachments.length) {
      newVals.properties['files:files'] = this.state.document.properties['files:files'].concat(this.state.attachments);
    }

    nuxeo
      .updateDocument(this.state.document, newVals)
      .then(() => this.props.history.goBack())
      .catch((error) => {
        this.props.throwError(error);
      })
    ;
  }

  render() {

    if (this.state.loading) {
      return <Spinner size="small" />;
    }

    return (
      <div id="page-body">
        <div className="col-lg-8">
          <div className="panel panel-primary panel-main">
            <div className="panel-heading">
              <h3 className="panel-title">Editing {this.state.document.properties['dc:title']}</h3>
            </div>
            <div className="panel-body">
              <div className="form-group">
                <label className="control-label">UUID</label>
                <input className="form-control" type="input" disabled id="docId" value={this.state.document.uid} />
              </div>
              <FormBuilder
                workspaceTemplates={null}
                onSubmit={this.handleSubmit.bind(this)}
                onCancel={this.props.history.goBack}
                layout={this.state.layout}
                initialData={this.state.document.properties}
                buttonLabel="Save"
                versionable={this.state.document.facets.indexOf('Versionable') !== -1}
                schemas={this.state.schemas} />
            </div>
          </div>
        </div>
        <div className="col-lg-4">
          <If condition={this.state.document.hasOwnProperty('files:files')}>
            <Attachments
              layout={this.state.layout}
              onUpload={this.handleAddNewAttachments.bind(this)}
              onRemove={this.handleRemoveExisitngAttachments.bind(this)}
              initialFiles={this.state.document.properties['files:files']} />
          </If>
        </div>
      </div>
      );
  }
}

export default DocEdit;
