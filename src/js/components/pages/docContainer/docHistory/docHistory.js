import React, { Component, PropTypes } from 'react';
import toCSV from 'csv-stringify';
import nuxeo from 'common/nuxeo/nuxeo';
import Spinner from 'common/spinner/spinner';
import utils from 'common/utils/utils';
import DocHistoryHeader from './docHistoryHeader/docHistoryHeader';
import DocHistoryItem from './docHistoryItem/docHistoryItem';

class DocHistory extends Component {
  static propTypes = {
    document: PropTypes.object.isRequired,
    params: PropTypes.object.isRequired,
  }

  componentDidMount() {
    nuxeo.getDocumentHistory(this.props.document.uid)
      .then((response) => {
        this.setState({
          docChildren: response.entries,
        });
      });
  }

  renderCsv(callback) {
    // todo: this list shouldn't be declared twice, see docHistoryHeader and docHistoryItem
    const columns = [
      'eventId',
      'eventDate',
      'principalName',
      'category',
      'state'
    ];

    const data = [columns].concat(this.state.docChildren);

    toCSV(data, {
      columns: columns
    }, (err, csv) => {
      if (err) {
        throw err;
      }
      callback(csv);
    });
  }

  download() {
    this.renderCsv((data) => {
      utils.download(data, this.safeFilename(this.state.document.title) + '.csv');
    });
  }

  safeFilename(dirty) {
    let clean = ('' + dirty);

    // todo: use a more relaxed regexp to clean, this one is probably overly safe
    clean = clean.replace(/[^a-z0-9_. ]/gi, '-');

    return clean;
  }

  render() {

    if (!this.state) {
      return <Spinner size="small" />;
    }

    return (
      <div id="page-body">
        <div className="fluid-container workspace">
          <div className="col-lg-12">
            <div className="row">
              <div className="col-lg-12">
                <button className="btn btn-success pull-right" style={{marginBottom: '25px'}} onClick={this.download.bind(this)}>
                  Download as CSV
                </button>
                <div className="clearfix">
                </div>
              </div>
            </div>
            <div className="list">
              <table className="table table-hover">
                <DocHistoryHeader />
                <tbody>
                  <If condition={this.state.docChildren.length}>
                    {this.state.docChildren.map((item, index) => <DocHistoryItem historyItem={item} key={index} />)}
                    <Else/>
                    <tr>
                      <td colSpan="12">
                        <h4 className="text-muted text-center state-empty"><em>No History Found.</em></h4>
                      </td>
                    </tr>
                  </If>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      );
  }
}

export default DocHistory;
