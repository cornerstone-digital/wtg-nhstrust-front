import React, { Component } from 'react';

class DocHistoryHeader extends Component {
  render() {
    return (
      <thead>
        <tr>
          <td id="name">
            Performed Action
          </td>
          <td id="owner">
            Date
          </td>
          <td id="owner">
            Username
          </td>
          <td id="name">
            Category
          </td>
          <td id="name">
            Comment
          </td>
          <td id="name">
            State
          </td>
        </tr>
      </thead>
      );
  }
}

export default DocHistoryHeader;
