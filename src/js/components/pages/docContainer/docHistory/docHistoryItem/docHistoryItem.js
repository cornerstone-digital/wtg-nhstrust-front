import React, { Component, PropTypes } from 'react';
import Time from 'react-time';

class DocHistoryItem extends Component {

  static propTypes = {
    historyItem: PropTypes.object,
    key: PropTypes.number
  };

  render() {

    return (
      <tr key={this.props.key}>
      <td>
        {this.props.historyItem.eventId}
      </td>
      <td>
        <Time value={this.props.historyItem.eventDate} format="LLL" />
      </td>
      <td>
        {this.props.historyItem.principalName || '-'}
      </td>
      <td>
        {this.props.historyItem.category || '-'}
      </td>
      <td>
        {this.props.historyItem.comment || '-'}
      </td>
      <td>
        {this.props.historyItem.docLifeCycle || '-'}
      </td>
    </tr>
    );
  }
}

export default DocHistoryItem;
