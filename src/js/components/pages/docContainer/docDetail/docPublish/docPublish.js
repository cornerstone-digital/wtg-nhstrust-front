import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import nuxeo from 'common/nuxeo/nuxeo';
import TreeView from 'common/treeView/treeView';
import './docPublish.scss';
import utils from 'common/utils/utils';

@connect(state => ({
  user: state.user
}))

class Publish extends Component {

  static propTypes = {
    document: PropTypes.object,
    user: PropTypes.object.isRequired,
    handlePublish: PropTypes.func,
    displayPublished: PropTypes.func,
    value: PropTypes.string,
    sectionChildren: PropTypes.object,
    displayTreeDir: PropTypes.func,
    publishedDoc: PropTypes.object,
    publishedList: PropTypes.array
  }

  constructor(props) {
    super(props);
    this.state = {
      selectedPath: '',
      sectionChildren: [],
      publishedDoc: [],
      directoryDefaultAcls: [],
      directoryIntranetAcls: [],
      selectSectionDir: '',
      buttonLoading: false
    };
  }

  componentDidMount() {

    Promise.all([
      nuxeo.getSectionByPath('/Intranet/sections/'),
      nuxeo.getSectionByPath('/default-domain/sections/')
    ])
      .then((response) => {
        this.setState({
          directoryIntranetAcls: response[0],
          directoryDefaultAcls: response[1]
        });
      });

    this.setState({
      selectSectionDir: this.refs.selectSectionDir.value,
      childDirClass: 'node',
    });

  }
  

  buildPublishDropdown() {
    let selectIntranet;
    let selectDefault;

    if (Object.keys(this.state.directoryIntranetAcls).length > 1) {
      const directoryIntranetPermission1 = utils.getUserRightsToDoc(this.state.directoryIntranetAcls.contextParameters.acls, this.props.user);
      if (directoryIntranetPermission1 === 'Everything' || directoryIntranetPermission1 === 'Write' || directoryIntranetPermission1 === 'Read') {
        selectIntranet = <option value="/Intranet/sections/">Intranet sections</option>;
      }
    }
    if (Object.keys(this.state.directoryDefaultAcls).length > 1) {
      const directoryDefaultPermission = utils.getUserRightsToDoc(this.state.directoryDefaultAcls.contextParameters.acls, this.props.user);
      if (directoryDefaultPermission === 'Everything' || directoryDefaultPermission === 'Write' || directoryDefaultPermission === 'Read') {
        selectDefault = <option value="/default-domain/sections/">Domain sections</option>;
      }
    }
    return (
      <select ref="selectSectionDir" name="selectSectionDir" id="selectSectionDir" onChange={this.handleChange.bind(this)} value={this.props.value} >
        <option value="" hidden>Select a section</option>
        {selectDefault}
        {selectIntranet}    
      </select>
    );
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.handlePublish(this.props.document.uid, this.state.selectedPath, true);
    this.props.displayPublished(this.props.publishedList);
    this.setState({
      selectedPath: '',
      selectedSectionDir: '',
    });
  }

  handleChange(event) {
    event.preventDefault();
    const treeObject = [];

    nuxeo.getSectionChildren(event.target.value)
      .then((response) => {
        response.entries.map((entry) => {
          nuxeo.getSectionChildren(entry.path)
            .then((response2) => {
              const children = [];
              response2.entries.map((entry2) => {
                const permissions2 = utils.getUserRightsToDoc(entry2.contextParameters.acls, this.props.user);
                if (permissions2 === 'Everything' && entry2.state !== 'deleted') {
                  children.push({
                    'title': entry2.title,
                    'path': entry2.path,
                  });
                }
                return children;
              });
              
              if (Object.keys(children).length >= 1) {
                treeObject.push({
                  'title': entry.title,
                  'path': entry.path,
                  'children': children
                });
              }
              this.setState({
                sectionChildren: treeObject,
                collapsed: treeObject.map(() => true)
              });

            });
        });
      });

    this.setState({
      selectedPath: '',
      selectedSectionDir: event.target.value,
    });

  }

  handleClick(index) {
    const [...collapsed] = this.state.collapsed;
    collapsed[index] = !collapsed[index];
    this.setState({
      collapsed: collapsed
    });
  }

  handleSelect(path) {
    this.setState({
      selectedPath: path,
    });

  }

  loadFileTree(dataSource) {
    const collapsed = this.state.collapsed;
    const fullTree = dataSource.map((item, index) => {
      const label = <span className="node" onClick={this.handleClick.bind(this, index)}>{item.title}</span>;
      const children = item.children.map((child, index2) => {
        const label2 = <span className={this.state.selectedPath === child.path ? 'node selectedDir' : ''} onClick={this.handleSelect.bind(this, child.path)}>{child.title}</span>;
        return (
          <TreeView className={this.state.selectedPath === child.path ? 'node selectedDir' : ''} nodeLabel={label2} key={index2} collapsedIcon={<i className="fa fa-folder-open"></i>} expandedIcon={<i className="fa fa-folder-open"></i>} onClick={this.handleSelect.bind(this, child.path)} />
        );
      });

      return (
        <TreeView key={index} nodeLabel={label} collapsedIcon={<i className="fa fa-folder"></i>} expandedIcon={<i className="fa fa-folder-open"></i>} collapsed={collapsed[index]} onClick={this.handleClick.bind(this, index)}>
          {children}
        </TreeView>   
      );

    });

    return (
      <div className="input-group tree-group">
        <h4>Select a directory from the tree below: </h4>
          {fullTree}  
      </div>
      );
  }

  collapseAll(event) {
    event.preventDefault();
    this.setState({
      collapsedBookkeeping: this.state.collapsed.map(() => true),
    });
  }

  render() {
    const children = this.state.collapsed ? this.loadFileTree(this.state.sectionChildren) : '';
    const publishedDoc = this.props.displayPublished(this.props.publishedList);
    const selectOptions = this.buildPublishDropdown();
    let form = <div className="hidden">{selectOptions}</div>;
    if (this.props.document.state === 'approved') {
      form = (
        <form action="" className="form-styled">
        <div className="input-group">
          {selectOptions}
        </div>
          {children}
          <button className="pull-left" onClick={this.handleSubmit.bind(this)} disabled={!this.state.selectedPath} >Publish</button>
        </form>         
      );
    }
    return (
      <div className="panel panel-primary panel-extra">
      <div className="panel-body">
        <h2>Publish</h2>
          {form}
          {publishedDoc}          
      </div>
      </div>
    );
  }
}

export default Publish;
