import React, { Component, PropTypes } from 'react';
import utils from 'common/utils/utils';
import moment from 'moment';

class DynamicMetaData extends Component {

  static propTypes = {
    doc: PropTypes.object.isRequired,
    docLayout: PropTypes.object
  }

  formatValue(widget) {

    const docPropertyName = widget.fields[0].propertyName;
    const docPropertyValue = this.props.doc.properties[docPropertyName];

    switch (widget.type) {
      case 'selectOneDirectory':
        return widget.selectOptions.filter(option => option.itemValue === docPropertyValue)[0].itemLabel;
      case 'list':
        return <ul> {docPropertyValue.map((listItem, index) => <li key={index}> {listItem} </li>)} </ul>;
      case 'datetime':
        return docPropertyValue ? moment(docPropertyValue).format('Do MMM YY') : '-';
      case 'text':
        return docPropertyValue;
      default:
        return docPropertyValue;
    }

  }

  ignoreCommonMetaData(widget) {

    const commonMetaData = [
      'dc:title',
      'dc:description',
      'files:files',
      'file:content',
      'id',
      'type',
      'versionLabel'
    ];

    return commonMetaData.indexOf(widget.fields[0].propertyName) === -1;

  }


  render() {

    return (
      <tbody>
        {this.props.docLayout.widgets
           .filter(this.ignoreCommonMetaData)
           .map((widget, index) => (
           <tr key={index}>
             <td>
               {widget.labels.any}:
             </td>
             <td>
               {this.formatValue(widget)}
             </td>
           </tr>
           ))}
        <tr>
          <td>
            Document Type:
          </td>
          <td>
            {utils.getFileTypeLabel(this.props.doc.type)}
          </td>
        </tr>
        <tr>
          <td>
            Version:
          </td>
          <td>
            {`${this.props.doc.properties['uid:major_version'] || 0}.${this.props.doc.properties['uid:minor_version'] || 0}`}
          </td>
        </tr>
      </tbody>
      );
  }

}

export default DynamicMetaData;
