import React, { Component, PropTypes } from 'react';
import nuxeo from 'common/nuxeo/nuxeo';
import utils from 'common/utils/utils';
import nuxeoConfig from 'config/nuxeo';
import DynamicMetaData from './dynamicMetaData/dynamicMetaData';
import DefaultMetaData from './defaultMetaData/defaultMetaData';

class DocSummary extends Component {

  static propTypes = {
    document: PropTypes.object.isRequired,
    filesize: PropTypes.string,
    additionalfields: PropTypes.object,
    documentLayout: PropTypes.object,
    published: PropTypes.bool,
    userRights: PropTypes.string
  }

  getNoteExt(mimeType) {
    switch (mimeType) {
      case 'text/x-web-markdown':
        return '.md';
      case 'text/xml':
        return '.xml';
      case 'text/plain':
        return '.txt';
      case 'text/html':
        return '.html';
      default:
        return '';
    }
  }

  resolveFileLink() {
    const doc = this.props.document;
    let fileName;
    let docPath;
    let docTitle = <span><i className="text-muted">No main file attached</i></span>;

    let nuxeoEdit = this.props.published || this.props.userRights === 'Read' ? nuxeoEdit = '' : <a href={this.nuxeoDriveEdit()}>Edit in Nuxeo Drive</a>;

    if (this.props.filesize.length) {

      docPath = utils.getDocumentPath(doc);

      switch (doc.type) {
        case 'Note':
          const extension = this.getNoteExt(doc.properties['note:mime_type']) || '.html';
          docPath = `${nuxeoConfig.baseURL}/nxfile/${doc.repository}/${doc.uid}/blobholder:0/${doc.title}${extension}`;
          fileName = doc.title + extension;
          nuxeoEdit = '';
          break;
        default:
          docPath = doc.properties['file:content'].data;
          fileName = doc.properties['file:filename'] || doc.properties['file:content'].name;
          break;
      }

      const fileIcon = utils.getFileIcon(fileName);

      docTitle = (
        <div>
          <a className="document" href={docPath}><span className={fileIcon}></span>{fileName} <span className="hint">{this.props.filesize}</span></a>
          {nuxeoEdit}
        </div>
      );

    }

    return docTitle;
  }

  nuxeoDriveEdit() {
    const protocol = nuxeoConfig.baseURL.split(':');
    const base = nuxeoConfig.baseURL.replace(/.*?:\/\//g, '');
    let url = '';
    if (typeof this.props.document.properties['file:filename'] !== undefined) {
      url = encodeURI(`nxdrive://edit/${protocol[0]}/${base}/user/${nuxeo.userId}/repo/default/nxdocid/${this.props.document.uid}/filename/${this.props.document.properties['file:filename']}/downloadUrl/nxbigfile/default/${this.props.document.uid}/blobholder:0/`);
    }
    return url;
  }

  render() {
    return (
      <div className="panel panel-primary panel-main">
        <div className="panel-heading">
          <h3 className="panel-title">Main File</h3>
        </div>
        <div className="panel-body">
          <div className="row">
            <div className="col-md-6">
              <div className="table-responsive">
                <table className="table table-condensed">
                  <tbody>
                    <tr>
                      <td>
                        {this.resolveFileLink()}
                      </td>
                    </tr>
                    <tr>
                      <td>
                        {this.props.document.properties['dc:description']}
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="col-md-6">
              <div className="table-responsive">
                <table className="table table-condensed">
                  <If condition={Object.keys(this.props.documentLayout.widgets).length >= 1}>
                    <DynamicMetaData docLayout={this.props.documentLayout} doc={this.props.document} />;
                    <Else />
                    <DefaultMetaData doc={this.props.document} />
                  </If>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      );
  }
}

export default DocSummary;
