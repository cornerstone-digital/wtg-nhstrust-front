import React, { Component, PropTypes } from 'react';
import Time from 'react-time';

class DefaultMetaData extends Component {

  static propTypes = {
    doc: PropTypes.object.isRequired
  }

  render() {
    return (
      <tbody>
        <tr>
          <td>
            Created at:
          </td>
          <td>
            <Time value={this.props.doc.properties['dc:created'] || 0} format="Do MMM 'YY" />
          </td>
        </tr>
        <tr>
          <td>
            Modified at:
          </td>
          <td>
            <Time value={this.props.doc.properties['dc:modified'] || 0} format="Do MMM 'YY" />
          </td>
        </tr>
        <tr>
          <td>
            Creator:
          </td>
          <td>
            {this.props.doc.properties['dc:creator']}
          </td>
        </tr>
        <tr>
          <td>
            Last Contributor:
          </td>
          <td>
            {this.props.doc.properties['dc:lastContributor']}
          </td>
        </tr>
        <tr>
          <td>
            Document Type:
          </td>
          <td>
            {this.props.doc.type}
          </td>
        </tr>
        <tr>
          <td>
            Version:
          </td>
          <td>
            {`${this.props.doc.properties['uid:major_version'] || 0}.${this.props.doc.properties['uid:minor_version'] || 0}`}
          </td>
        </tr>
      </tbody>
      );
  }

}

export default DefaultMetaData;
