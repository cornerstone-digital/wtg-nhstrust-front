import React, { Component, PropTypes } from 'react';
import PicturePanel from './docTypePanels/picturePanel';
import VideoPanel from './docTypePanels/videoPanel';
import AudioPanel from './docTypePanels/audioPanel';
import './docTypeMetaData.scss';

class DocTypeMetaData extends Component {
  static propTypes = {
    document: PropTypes.object.isRequired
  }

  render() {
    switch (this.props.document.type) {
      case 'Picture':
      {
        return <PicturePanel document={this.props.document} />;
      }
      case 'Audio':
      {
        return <AudioPanel document={this.props.document} />;
      }
      case 'Video':
      {
        return <VideoPanel document={this.props.document} />;
      }
      default: {
        return false;
      }
    }
  }
}

export default DocTypeMetaData;
