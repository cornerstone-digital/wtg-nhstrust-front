import React, { Component, PropTypes } from 'react';
import utils from 'common/utils/utils';

class AudioPanel extends Component {
  static propTypes = {
    document: PropTypes.object.isRequired
  };

  render() {
    const docPath = utils.getDocumentPath(this.props.document);

    return (
      <div className="panel panel-primary panel-extra">
        <div className="panel-heading">
          <h3 className="panel-title">Preview</h3>
        </div>
        <div className="panel-body">
          <audio id="audio-player" preload="auto" controls="controls" style={{width: '100%'}}>
            <source src={docPath} />
          </audio>
        </div>
      </div>
    );
  }
}

export default AudioPanel;
