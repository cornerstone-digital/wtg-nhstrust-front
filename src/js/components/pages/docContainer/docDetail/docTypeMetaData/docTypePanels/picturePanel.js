import React, { Component, PropTypes } from 'react';
import utils from 'common/utils/utils';

class PicturePanel extends Component {
  static propTypes = {
    document: PropTypes.object.isRequired
  };

  informationPanel() {
    return (
        <div className="table-responsive">
          <h2>Information</h2>
          <table className="table table-condensed">
            <tbody>
              <tr>
                <td>
                  Dimensions:
                </td>
                <td>
                  {this.props.document.properties['picture:info'].width} x {this.props.document.properties['picture:info'].height}
                </td>
              </tr>
              <tr>
                <td>
                  Format:
                </td>
                <td>
                  {this.props.document.properties['picture:info'].format}
                </td>
              </tr>
              <tr>
                <td>
                  Colour Profile:
                </td>
                <td>
                  {this.props.document.properties['picture:info'].colorSpace}
                </td>
              </tr>
              <tr>
                <td>
                  Bit Depth:
                </td>
                <td>
                  {this.props.document.properties['picture:info'].depth}
                </td>
              </tr>
            </tbody>
          </table>
        </div>
    );
  }

  additionalFormats() {
    return (
        <div className="table-responsive table-picture-formats table-formats">
          <h2>Additional Formats</h2>
          <table className="table table-condensed">
            <tbody>
              {this.props.document.properties['picture:views'].map(function loop(format, index) {
                  return (
                    <tr key={index}>
                      <td>
                        {format.title}
                      </td>
                      <td>
                        {format.width} x {format.height}
                      </td>
                      <td>
                        {utils.bytesToSize(format.content.length)}
                      </td>
                      <td>
                        <span className="label label-primary label-metadata">{format.info.format}</span>
                      </td>
                      <td>
                        <a href={format.content.data}>
                          <i className="fa fa-download"></i>
                        </a>
                      </td>
                    </tr>
                  );
                })
              }
            </tbody>
          </table>
        </div>
    );
  }

  render() {
    const docUrl = utils.getDocumentUrl(this.props.document);
    return (
      <div className="panel panel-primary panel-extra">
        <div className="panel-heading">
          <h3 className="panel-title">Preview</h3>
        </div>
        <div className="panel-body">
            <div className="row">
                <div className="col-lg-8">
                  <img src={docUrl} width="100%" />
                </div>
                <div className="col-lg-4">
                  {this.informationPanel()}
                </div>
            </div>
            <div className="row">
              <div className="col-lg-12">
                {this.additionalFormats()}
              </div>
            </div>
        </div>

      </div>
    );
  }
}


export default PicturePanel;
