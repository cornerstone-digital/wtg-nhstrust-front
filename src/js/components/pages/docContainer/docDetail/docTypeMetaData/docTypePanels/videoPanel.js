import React, { Component, PropTypes } from 'react';
import utils from 'common/utils/utils';
//import { default as Video, Controls, Play, Mute, Seek, Fullscreen, Time, Overlay } from 'react-html5video';
import Video from 'react-html5video';
import 'react-html5video/dist/ReactHtml5Video.css';

class VideoPanel extends Component {
  static propTypes = {
    document: PropTypes.object.isRequired
  };

  informationPanel() {
    return (
      <div className="table-responsive">
          <h2>Information</h2>
          <table className="table table-condensed">
            <tbody>
            <tr>
              <td>
                Format:
              </td>
              <td>
                {this.props.document.properties['vid:info'].format}
              </td>
            </tr>
            <tr>
              <td>
               Duration:
              </td>
              <td>
                {this.props.document.properties['vid:info'].duration}
              </td>
            </tr>
            <tr>
              <td>
                Width:
              </td>
              <td>
                {this.props.document.properties['vid:info'].width}
              </td>
            </tr>
            <tr>
              <td>
                Height:
              </td>
              <td>
                {this.props.document.properties['vid:info'].height}
              </td>
            </tr>
            <tr>
              <td>
                Frame Rate:
              </td>
              <td>
                {this.props.document.properties['vid:info'].frameRate}
              </td>
            </tr>
            </tbody>
          </table>
        </div>
    );
  }

  render() {
    const docPath = utils.getDocumentPath(this.props.document);
    console.log(this.props.document);

    return (
      <div className="panel panel-primary panel-extra">
        <div className="panel-heading">
          <h3 className="panel-title">Preview</h3>
        </div>
        <div className="panel-body">
            <div className="row">
              <div className="col-lg-8">
                <Video controls loop width="100%">
                  <source src={docPath} type="video/mp4" />
                </Video>
              </div>
              <div className="col-lg-4">
                {this.informationPanel()}
              </div>
            </div>
         </div>
      </div>
    );
  }
}

export default VideoPanel;
