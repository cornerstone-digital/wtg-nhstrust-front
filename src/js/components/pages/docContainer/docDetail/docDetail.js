import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import numeral from 'numeral';

import DocTypeMetaData from './docTypeMetaData/docTypeMetaData';
import DocSummary from './docSummary/docSummary';
import Attachments from './attachments/attachments';
import Process from './process/process';
import StartProcess from './process/startprocess';
import CompleteProcess from './process/completeprocess';
import Publish from './docPublish/docPublish';
import DocTasks from './docTasks/docTasks';
import DocTags from './docTags/docTags';
import DocRelations from './docRelations/docRelations';
import nuxeo from 'common/nuxeo/nuxeo';
import Spinner from 'common/spinner/spinner.js';
import utils from 'common/utils/utils';
import { throwError } from 'actions/error';
import messagingHandler from 'common/messaging/MessagingHandler';

import './docDetail.scss';

@connect(state => ({
    user: state.user
}), {throwError})

class DocDetail extends Component {

  static propTypes = {
    document: PropTypes.object.isRequired,
    params: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
    throwError: PropTypes.func
  }

  state = {
    loading: true
  }

  componentDidMount() {
    this.fetchData();
  }

  componentWillReceiveProps(nextProps) {
    this.fetchData(nextProps);
  }

  fetchData(nextProps) {
    const props = nextProps || this.props;
    const docId = props.document.uid;

    this.setState({
      loading: true,
      document: props.document
    });

    Promise.all([
      nuxeo.getDocumentById(docId),
      nuxeo.getTasksByDocId(docId),
    ])
      .then((response) => {

        //If this is a folder - redirect to folder view
        if (utils.isFolderish(response[0].facets)) {
          return props.history.replaceState(null, `folder/${response[0].uid}`);
        }

        // If document has at least one open task, set layoutName to null
        const layoutName = response[1].entries[0] !== undefined ? response[1].entries[0].taskInfo.layoutResource.name : null;
        return Promise.all([
          nuxeo.getLayout(`layout@${response[0].type}-view`),
          nuxeo.getDocumentsById(utils.pluck(response[0].contextParameters.publishDocEnricher || [], 'id')),
          nuxeo.getPinnedStatus(response[0].uid),
          nuxeo.getLayout(layoutName),
          nuxeo.getWorkflowModels(),
          nuxeo.getRunningWorkflowsOnDoc(docId),
          nuxeo.getFileTasksLayout(),
          nuxeo.getDocumentRelations(docId),
        ])
          .then((response2) => {
            this.setState({
              loading: false,
              document: response[0],
              workflowmodels: response2[4].entries,
              runningworkflows: response2[5].entries,
              filesize: '',
              tasks: response[1].entries,
              taskLayout: response2[6],
              documentLayout: response2[0],
              publishedList: response2[1].entries,
              breadcrumbs: response[0].contextParameters.breadcrumb.entries,
              isLocked: !!response[0].lockCreated,
              isPinned: response2[2],
              allRunningWorkflows: response[0].contextParameters.allDocumentsWorkflow,
              relations: response2[7].entries
            });

          });
      })
      .catch((error) => {
        props.throwError(error);
      });
  }

  componentWillUnmount() {
    this.isUnmounted = true;
  }

  getContentSize(docType) {
    let docSize;
    let property;
    switch (docType) {
      case 'Note':
        property = this.state.document.properties['note:note'];
        docSize = property !== undefined && property !== null && property.length > 0 ? numeral(property.length).format('0 b') : '';
        break;
      default:
        property = this.state.document.properties['file:content'];
        docSize = property !== undefined && property !== null && property.length > 0 ? numeral(property.length).format('0 b') : '';
        break;
    }
    return docSize;
  }

  getProcessStatus(docState, runningWorkflows, allWorkflows) {
    let processStatus;
    if (runningWorkflows && runningWorkflows.length > 0) {
      processStatus = (
        <Process
          runningworkflows={this.state.runningworkflows}
          allWorkflows={allWorkflows}
          user={this.props.user}
          docstatus={this.state.document.state}
          handleWorkflowCancel={::this.handleWorkflowCancel} />
      );

    } else if (docState === 'draft') {
      processStatus = (
        <StartProcess
          document={this.state.document}
          workflowmodels={this.state.workflowmodels}
          docstatus={this.state.document.state}
          handleworkflowsubmit={::this.handleWorkflowSubmit} />
      );
    } else {
      // processStatus = 'completed';
      processStatus = <CompleteProcess docstatus={this.state.document.state} />;
    }
    return processStatus;
  }

  getAttachments(files) {

    if (!files) {
      return false;
    }

    if (files.find(item => !item.file) || !files.length) {
      return false;
    }

    return (
      <Attachments
        attachments={files}
        filesize={this.getContentSize(this.state.document.type)}
        layout={this.state.documentLayout} />
      );
  }

  handleWorkflowSubmit(docPath, selectedWorkflow) {
    nuxeo.startWorkflow(docPath, selectedWorkflow)
      .then(() => {
        return Promise.all([
          nuxeo.getRunningWorkflowsOnDoc(this.props.document.uid),
          nuxeo.getTasksByDocId(this.props.document.uid)
        ])
          .then((response) => {
            if (!this.isUnmounted) {
              response[1].entries[0].actors.forEach((actor) => {
                messagingHandler.publish({
                  channel: actor.id.replace('group:', ''),
                  action: 'alert',
                  initiatedBy: this.props.user.properties.username,
                  content: {
                    title: response[1].entries[0].name,
                    type: 'info',
                    dismissable: true,
                    text: response[1].entries[0].directive
                  }
                });
              });

              this.setState({
                runningworkflows: response[0].entries,
                tasks: response[1].entries
              });
            }

          });
      });
  }

  handleTaskSubmit(taskId, action, comments) {
    const body = {
      'entity-type': 'task',
      'id': taskId,
      'variables': comments
    };
    nuxeo
      .actionTask(taskId, action, body)
      .then(() => {
        return Promise.all([
          nuxeo.getRunningWorkflowsOnDoc(this.props.document.uid),
          nuxeo.getDocumentById(this.props.document.uid),
          nuxeo.getTasksByDocId(this.props.document.uid)
        ]);
      })
      .then((response2) => {
        this.setState({
          runningworkflows: response2[0].entries,
          document: response2[1],
          tasks: response2[2].entries
        });
      });
  }

  handleWorkflowCancel(workflowId) {
    nuxeo.cancelWorkflow(workflowId)
      .then(() => {
        return Promise.all([
          nuxeo.getRunningWorkflowsOnDoc(this.props.document.uid),
          nuxeo.getTasksByDocId(this.props.document.uid)
        ]);
      })
      .then((response) => {
        this.setState({
          runningworkflows: response[0].entries,
          tasks: response[1].entries
        });
      });
  }

  handlePublish(docId, sectionId, override) {
    nuxeo.publishDoc(docId, sectionId, override)
      .then((response) => {
        nuxeo.getDocumentInSection(response.contextParameters.publishDocEnricher)
          .then((response2) => {
            this.setState({
              publishedDoc: response,
              publishedList: response2
            });
          });
      });
  }

  handleUnpublish(pubDocPath, mainDoc, event) {
    event.preventDefault();
    nuxeo.deletePublishedDocument(pubDocPath)
      .then(() => {
        nuxeo.getDocumentById(mainDoc.uid)
          .then((response2) => {
            nuxeo.getDocumentInSection(response2.contextParameters.publishDocEnricher)
              .then((response3) => {
                this.setState({
                  publishedList: response3
                });
              });
          });

      });
  }

  checkModifications(originDoc, publishedDoc) {
    return originDoc.lastModified > publishedDoc.lastModified ? true : false;
  }

  displayPublished(publishedDocs) {
    if (publishedDocs !== undefined && publishedDocs.length >= 1) {
      const eachDoc = publishedDocs.map((item, index) => {

        const parentPath = item.path.replace(item.path.substr(item.path.lastIndexOf('/') + 1), '');
        const majorVersion = item.properties['uid:major_version'] !== null ? item.properties['uid:major_version'] : '0';
        const minorVersion = item.properties['uid:minor_version'] !== null ? item.properties['uid:minor_version'] : '0';
        const version = majorVersion + '.' + minorVersion;

        const permissions = utils.getUserRightsToDoc(item.contextParameters.acls, this.props.user);

        const modified = this.state.document.lastModified > item.lastModified ? true : false;
        let deleteButton;
        if (permissions === 'Everything') {
          deleteButton = (
            <button className="pull-left btn btn-danger" onClick={this.handleUnpublish.bind(this, item.uid, this.state.document)}>
              Unpublish
            </button>
          );
        }
        let republishButton;
        if (permissions === 'Everything' && modified && this.state.document.state === 'approved') {
          republishButton = (
            <button className="pull-left btn" onClick={this.handlePublish.bind(this, this.state.document.uid, parentPath, true)}>
              Republish
            </button>
          );
        }

        return (
          <li key={index}>
            <div className="doc-path">
              <a href={`#/section/${item.parentRef}`}>
                {parentPath}
              </a>
            </div>
            <div className="doc-version">
              Version:
              {version}
            </div>
            <form className="form-styled published-actions">
              {deleteButton}
              {republishButton}
            </form>
          </li>
          );
      });

      return (
        <div className="publishedList">
          <h4>Published in:</h4>
          <ul>
            {eachDoc}
          </ul>
        </div>
        );
    }
  }

  displayTreeDir(dirPath) {
    const treeObject = {};
    nuxeo.getSectionChildren(dirPath)
      .then((response) => {
        response.entries.map((entry, index) => {
          nuxeo.getSectionChildren(entry.path)
            .then((response2) => {
              const children = {};
              response2.entries.map((entry2, index2) => {
                children[index2] = {
                  'title': entry2.title,
                  'path': entry2.path,
                };
                return children;
              });
              treeObject[index] = {
                'title': entry.title,
                'path': entry.path,
                'children': children
              };
              this.setState({
                sectionChildren: treeObject
              });
            });
        });
      });
  }

  canPublish(userRights, published) {
    if (userRights === 'Everything' && !published) {
      if (this.state.document.state === 'approved' || this.state.publishedList.length > 0) {
        return true;
      }
    }
  }

  render() {
    if (!this.state || this.state.loading) {
      return <Spinner size="small" />;
    }

    const published = utils.checkPublishedDoc(this.state.document.facets);
    const userRights = utils.getUserRightsToDoc(this.state.document.contextParameters.acls, this.props.user);

    return (
      <div id="page-body">
        <div className="col-lg-8">
          <DocTypeMetaData document={this.state.document} />
          <DocSummary
            document={this.state.document}
            filesize={this.getContentSize(this.state.document.type)}
            documentLayout={this.state.documentLayout}
            published={published}
            userRights={userRights} />
          <If condition={this.state.tasks.length}>
            <DocTasks
              tasks={this.state.tasks}
              taskLayout={this.state.taskLayout}
              runningworkflows={this.state.runningworkflows}
              user={this.props.user}
              handleTaskSubmit={::this.handleTaskSubmit} />
          </If>
        </div>
        <div className="col-lg-4">
          {this.getAttachments(this.state.document.properties['files:files'])}
          <DocTags parentId={this.state.document.uid} tags={this.state.document.contextParameters.getDocumentTags} />
          <DocRelations parentId={this.state.document.uid} items={this.state.relations}/>
          {this.getProcessStatus(this.state.document.state, this.state.runningWorkflows, this.state.allRunningWorkflows)}
          <If condition={this.canPublish()}>
            <Publish
              document={this.state.document}
              handlePublish={this.handlePublish.bind(this)}
              displayPublished={this.displayPublished.bind(this)}
              displayTreeDir={this.displayTreeDir}
              sectionChildren={this.state.sectionChildren}
              publishedDoc={this.state.publishedDoc}
              publishedList={this.state.publishedList} />
          </If>
        </div>
      </div>

      );
  }

}

export default DocDetail;
