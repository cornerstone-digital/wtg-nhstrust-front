import React, {Component, PropTypes} from 'react';
import Select from 'react-select';
import nuxeo from 'common/nuxeo/nuxeo';
import utils from 'common/utils/utils';

// const APIATTR_NXQL_VALUE = 'path';
// const APIATTR_NXQL_LABEL = 'title';

// the initial source is now contextParameters (getDocumentTags enricher)
const APIATTR_NXQL_VALUE = 'label';
const APIATTR_NXQL_LABEL = 'label';

const APIATTR_AUTO_VALUE = 'id';
const APIATTR_AUTO_LABEL = 'displayLabel';


class DocTags extends Component {

  static propTypes = {
    parentId: PropTypes.string.isRequired,
    tags: PropTypes.array.isRequired,
  }

  componentWillMount() {
    this.setTagsStateFromProps(this.props.tags);
  }

  componentWillReceiveProps(nextProps) {
    this.setTagsStateFromProps(nextProps.tags);
  }

  setTagsStateFromProps(tags) {
    this.setState({
      tags: this.transformTagsFromNxql(tags)
    });
  }

  transformTagsFromNxql(tags) {
    return tags.map((tag) => {
      return {
        value: tag[APIATTR_NXQL_VALUE],
        label: tag[APIATTR_NXQL_LABEL]
      };
    });
  }

  transformTagsFromAuto(tags) {
    return tags.map(tag => {
      return {
        value: tag[APIATTR_AUTO_VALUE],
        label: tag[APIATTR_AUTO_LABEL]
      };
    });
  }

  onChange(value) {
    const curTags = this.state.tags;
    const newTags = this.transformFieldValue(value);

    const curTagsValues = utils.pluck(curTags, 'value');
    const newTagsValues = utils.pluck(newTags, 'value');

    const added = utils.difference(newTagsValues, curTagsValues);
    const removed = utils.difference(curTagsValues, newTagsValues);

    if (added.length) {
      this.addTags(added);
    }
    if (removed.length) {
      this.removeTags(removed);
    }

    this.setState({
      tags: newTags
    });
  }

  transformFieldValue(tags) {
    return tags
      .split(',') // react-select@0.9.1 - remove in 1.0.0
      .map(tag => { // react-select@0.9.1 - remove in 1.0.0
        return {
          value: tag,
          label: tag
        };
      })
      .filter(tag => {
        return tag.value.length > 0;
      });
  }

  addTags(tags) {
    return nuxeo.addDocumentTags(this.props.parentId, tags);
  }

  removeTags(tags) {
    return nuxeo.removeDocumentTags(this.props.parentId, tags);
  }

  loadOptions(query) {
    return Promise.resolve(true)
    .then(() => {
      if (Object.prototype.toString.call(query) === '[object Array]') { // react-select@0.9.1 - remove in 1.0.0
        return [];
      }

      if (query.length < 1) {
        return [];
      }

      return nuxeo.getTagSuggestions(query)
      .then(response => {
        return this.transformTagsFromAuto(response);
      })
      .then((suggestions) => {
        return this.mergeQueryWithSuggestions(query.toLowerCase(), suggestions);
      });
    })
    .then(options => {
      return {
        options: options,
        complete: false
      };
    });
  }

  mergeQueryWithSuggestions(query, suggestions) {
    const merged = suggestions;

    if (utils.pluck(merged, 'value').indexOf(query) === -1) {
      merged.unshift({
        value: query,
        label: query
      });
    }

    return merged;
  }

  render() {
    return (
      <div className="panel panel-primary panel-extra">
        <div className="panel-body">
          <h2>Tags</h2>
          <Select
            asyncOptions={::this.loadOptions}
            autoload={false}
            cacheAsyncResults={false}
            clearable={false}
            ignoreCase
            multi
            onChange={::this.onChange}
            placeholder="Add..."
            value={this.state.tags} />
        </div>
      </div>
    );
  }
}

export default DocTags;
