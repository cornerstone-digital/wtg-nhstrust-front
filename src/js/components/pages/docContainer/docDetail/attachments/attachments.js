import React, { PropTypes, Component } from 'react';
import numeral from 'numeral';
import utils from 'common/utils/utils.js';

class Attachments extends Component {

  static propTypes = {
     attachments: PropTypes.array.isRequired,
     layout: PropTypes.object.isRequired
  }

  /**
   * Fetches a label from a given layout type
   * @return {string} A label if found - or a default label
   */
  getLabel(widgets) {

    const filesWidget = widgets.filter(widget => widget.name === 'files')[0];

    if (!filesWidget) {
      return 'Attachments';
    }

    if (filesWidget) {
      return filesWidget.labels.any;
    }
  }

  render() {
    return (
      <div className="panel panel-primary panel-attachments">
        <div className="panel-heading">
          <h3 className="panel-title">{this.getLabel(this.props.layout.widgets)}</h3>
        </div>
        <div className="panel-body">
          <ul>
            {this.props.attachments.map((item, index) => (
             <li key={index}>
               <a href={item.file.data}>
                 <span className={utils.getFileIcon(item.filename)}></span>
                 {item.filename}
                 <span className="hint">{numeral(item.file.length).format('0 b')}</span>
               </a>
             </li>
             ))}
          </ul>
        </div>
      </div>
      );
  }
}

export default Attachments;
