import React, { Component, PropTypes } from 'react';

class CompleteProcess extends Component {
  
  static propTypes = {
     docstatus: PropTypes.string
  }

  render() {

    return (
      <div className="panel panel-primary panel-extra">
        <div className="panel-body">
          <h2>State</h2>
          <span className="status">{this.props.docstatus}</span>
          <h2>Process</h2>
          <form className="form-styled">
            <span>No process can be started on this document.</span>
          </form>
        </div>
      </div>
      );
  }
}

export default CompleteProcess;
