import React, { Component, PropTypes } from 'react';
import './process.scss';

class Process extends Component {

  static propTypes = {
    workflowmodels: PropTypes.array,
    runningworkflows: PropTypes.array,
    allWorkflows: PropTypes.array,
    docstatus: PropTypes.string,
    user: PropTypes.object,
    isAdmin: PropTypes.bool,
    handleWorkflowCancel: PropTypes.func
  }
  /**
   * Check if user has permission to cancel workflow, only the workflow initiator and admins have permission
   * @param  object workflow the current workflow
   * @return string          the cancel button html
   */
  checkCancelPermission(workflow) {

    if (this.props.user.isAdministrator === true || this.props.user.id === workflow.initiator) {
      return <button className="pull-left" onClick={this.handleCancel.bind(this, workflow.id)}>Cancel process</button>;
    }
  }

  handleCancel(workflowId) {
    event.preventDefault();
    this.props.handleWorkflowCancel(workflowId);

  }

  render() {
    // console.log(this.props.allWorkflows);
    const items = this.props.runningworkflows.map((item, index) => {
      return (
        <form key={index} className="form-styled">
          <span className="process-desc" key={index} >{item.title} has been started by {item.initiator}</span>
          {this.checkCancelPermission(item)}
          <br />
        </form>
      );
    });
    return (
      <div className="panel panel-primary panel-extra">
        <div className="panel-body">
          <h2>State</h2>
          <span className="status">{this.props.docstatus}</span>
          <h2>Process</h2>
          {items}
          </div>
      </div>
      );
  }
}

export default Process;
