/*eslint-disable*/
import React, { Component, PropTypes } from 'react';
import {configVars} from 'config/nuxeo';
import ReactDOM  from 'react-dom';
import LaddaButton from 'react-ladda';

class StartProcess extends Component {

  static propTypes = {
     workflowmodels: PropTypes.array,
     value: PropTypes.string,
     handleworkflowsubmit: PropTypes.func,
     docstatus: PropTypes.string,
     workflowFiles: PropTypes.array,
     document: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {
      selectedWorkflow: '',
      buttonLoading: false
    };
  }

  isPermitted(workflowFiles, docType) {
    let permitted = false;
    workflowFiles.forEach((workflowFile) => {
      if (workflowFile === docType) {
        permitted = true;
      }
    });
    return permitted;
  }

  checkPermissions() {
    const permitted = configVars.WorkflowFileRelation.map((item) => {
      if (this.isPermitted(item.docTypes, this.props.document.type)) {
       return item.name;
      }
    });
    return permitted;
  }

  /**
   * Sets The initial dropdown value to state
   */
  componentDidMount() {
    this.setState({
      selectedWorkflow: this.refs.workflowDropdown.value
    });
  }

  handleChange(event) {
    this.setState({
      selectedWorkflow: event.target.value
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({
      buttonLoading: !this.state.buttonLoading
    })
    this.props.handleworkflowsubmit(this.props.document.path, this.state.selectedWorkflow);
  }

  render() {

    const permitted = this.checkPermissions();
    const items = this.props.workflowmodels.map((item, index) => {
       if (permitted.indexOf(item.name) !== -1) {
        return (
           <option key={index} value={item.name}>{item.title}</option>
        );
       }
    });

    return (   
      <div className="panel panel-primary panel-extra">
        <div className="panel-body">
          <h2>State</h2>
          <span className="status">{this.props.docstatus}</span>
          <h2>Process</h2>
          <form action="" className="form-styled">
            <div className="input-group">
              <select ref="workflowDropdown" name="selectedWorkflow" id="selectedWorkflow" onChange={this.handleChange.bind(this)} value={this.props.value} >
                {items}
              </select>
            </div>
            <LaddaButton
              className="pull-left" 
              onClick={this.handleSubmit.bind(this)}
              loading={this.state.buttonLoading}
              progress={0}
              buttonStyle="slide-right"
              spinnerSize={30}
              spinnerColor="#fff">Start</LaddaButton>
          </form>
        </div>
      </div>
      );
  }
}

export default StartProcess;
