import React, {Component, PropTypes} from 'react';
import utils from 'common/utils/utils';

class DocRelation extends Component {

  static types = [
    {
      label: 'Document',
      field: 'relation:target',
    },
    {
      label: 'Text',
      field: 'relation:targetString',
    },
    {
      label: 'URI',
      field: 'relation:targetUri'
    }
  ]

  static predicates = {
    'http://purl.org/dc/terms/ConformsTo': ['conforms to', 'is conformed to by'],
    'http://purl.org/dc/terms/IsBasedOn': ['is based on', 'is based on by'],
    'http://purl.org/dc/terms/References': ['references', 'is referenced by'],
    'http://purl.org/dc/terms/Replaces': ['replaces', 'is replaced by'],
    'http://purl.org/dc/terms/Requires': ['requires', 'is required by'],
  }

  static propTypes = {
    parentId: PropTypes.string.isRequired,
    item: PropTypes.object.isRequired,
  }

  getType() {
    const properties = this.props.item.properties;

    return this.constructor.types.find(type => {
      const val = properties[type.field];
      return !utils.isEmpty(val);
    });
  }

  // incoming or outgoing relation
  getDirection() {
    const type = this.getType();

    if (type.label !== 'Document') {
      return 0;
    }

    return this.props.item.properties['relation:target'] === this.props.parentId ? 1 : 0;
  }

  getPredicate() {
    const uri = this.props.item.properties['relation:predicate'];
    const predicates = this.constructor.predicates;
    if (Object.keys(predicates).indexOf(uri) !== -1) {
      return predicates[uri][this.getDirection()];
    }
  }

  render() {
    const type = this.getType();
    const item = this.props.item;

    return (
      <div>
        <div className="relation__predicate">{this.getPredicate()}</div>
        {this['render' + type.label](item)}
        <div className="relation__comment">{item.properties['dc:description']}</div>
      </div>
    );
  }

  renderDocument(item) {
    const right = utils.getRelatedDocUid(item, this.props.parentId);

    return (
      <div className="relation__object">
        <a href={utils.getDocLinkFromUid(right)}>
          {right}
        </a>
      </div>
    );
  }

  renderText(item) {
    return (
      <div className="relation__object">
        {item.properties['relation:targetString']}
      </div>
    );
  }

  renderURI(item) {
    return (
      <div className="relation__object">
        <a href="{item.properties['relation:targetUri']}" target="_blank">{item.properties['relation:targetUri']}</a>
      </div>
    );
  }
}

export default DocRelation;
