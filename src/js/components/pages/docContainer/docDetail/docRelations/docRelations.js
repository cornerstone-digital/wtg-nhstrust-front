import React, {Component, PropTypes} from 'react';
import nuxeo from 'common/nuxeo/nuxeo';
import DocRelation from './docRelation';

class DocRelations extends Component {

  static propTypes = {
    parentId: PropTypes.string.isRequired,
    items: PropTypes.array,
  }

  componentWillMount() {
    this.setStateFromProps(this.props);
  }

  componentWillReceiveProps(nextProps) {
    this.setStateFromProps(nextProps);
  }

  setStateFromProps(props) {
    this.setState({
      items: props.items
    });
  }

  add(items) {
    return nuxeo.addDocumentRelations(this.props.parentId, items);
  }

  remove(items) {
    return nuxeo.removeDocumentRelations(this.props.parentId, items);
  }

  render() {
    return (
      <div className="panel panel-primary panel-extra">
        <div className="panel-body">
          <h2>Relations</h2>
          <ul>
            {this.state.items.map((item, key) => {
              return (
                <li key={key}>
                  <DocRelation
                    parentId={this.props.parentId}
                    item={item} />
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    );
  }
}

export default DocRelations;
