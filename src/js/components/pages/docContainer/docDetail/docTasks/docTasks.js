import React, {Component, PropTypes} from 'react';
import Time from 'react-time';
import {configVars} from 'config/nuxeo';
import LaddaButton from 'react-ladda';
// import nuxeo from 'common/nuxeo/nuxeo';

class DocTasks extends Component {

  static propTypes = {
    // yourtasks: PropTypes.object.isRequired,
    user: PropTypes.object,
    tasks: PropTypes.array,
    handleTaskSubmit: PropTypes.func,
    approverComment: PropTypes.string,
    value: PropTypes.string,
    taskLayout: PropTypes.object,
    runningworkflows: PropTypes.array
  }

  constructor() {
    super();
    this.state = {
      user: '',
      commentProperties: {},
      buttonLoading: {}
    };
  }

  getButtonClass(actionName) {

    switch (actionName) {
      case 'cancel':
        return 'btn btn-white';
      case 'reject':
        return 'btn btn-danger';
      default:
        return 'btn';
    }
  }

  getTitleFromWorkflow(taskInstanceId, currentWorkflows) {
    let title;
    currentWorkflows.map((item) => {
      if (item.id === taskInstanceId) { title = item.title; }
    });
    return title;
  }

  getInitiatorFromWorkflow(taskInstanceId, currentWorkflows) {
    let initiator;
    currentWorkflows.map((item) => {
      if (item.id === taskInstanceId) { initiator = item.initiator; }
    });
    return initiator;
  }

  handleChange(event) {
    event.preventDefault();
    const state = {
      commentProperties: {}
    };
    // state[event.target.id] = event.target.value;
    state.commentProperties[event.target.id] = event.target.value;
    this.setState(state);
  }

  handleSubmit(taskId, actionName, event) {
    event.preventDefault();
    this.setState({
      buttonLoading: {
        [actionName]: true
      }
    });

    this.props.handleTaskSubmit(taskId, actionName, this.state.commentProperties);
  }

  render() {
    const items = this.props.tasks.map((item, index) => {
      const actors = item.actors.map((actor, index2) => {
        return <a key={index2} href="profile-page.html"><i className="fa fa-user"></i> {actor.id}</a>;
      });
      const actions = item.taskInfo.taskActions.map((action, index3) => {
        return (<LaddaButton key={index3} name={action.name} loading={this.state.buttonLoading[action.name]} className={this.getButtonClass(action.name)} onClick={this.handleSubmit.bind(this, item.id, action.name)} progress={0}
          buttonStyle="slide-right"
          spinnerSize={30}
          spinnerColor="#fff">{action.label}</LaddaButton>);
      });

      return (

        <div key={index} className="panel-body">
              <div className="item">
                  <div className="label">Workflow step:</div>
                  <div className="info">{this.getTitleFromWorkflow(item.workflowInstanceId, this.props.runningworkflows)} - {item.name}</div>
              </div>
              <div className="item">
                  <div className="label">Actors:</div>
                  <div className="info">{actors} </div>
              </div>
              <div className="item">
                  <div className="label">Due date:</div>
                  <div className="info"><Time value={item.dueDate} format="Do MMM 'YY" /></div>
              </div>
              <div className="item">
                  <div className="label">Directive</div>
                  <div className="info">{item.directive}</div>
              </div>
              <form action="" key={index} className="form-styled">
              <Comments taskvariables={item.variables} tasklayout={this.props.taskLayout.widgets} initiator={this.getInitiatorFromWorkflow(item.workflowInstanceId, this.props.runningworkflows)} currentuser={this.props.user} handleChange={this.handleChange.bind(this)} taskName={item.name} />
              <div className="button-group">
              {actions}
              </div>
              </form>
          </div>
      );
    });

    return (
      <div className="panel panel-primary panel-task-info">
        <div className="panel-heading">
          <h3 className="panel-title">Your Tasks</h3>
        </div>
          {items}
      </div>
    );
  }
}


const Comments = ({taskvariables, tasklayout, handleChange, value, taskName}) => {

  function checkPermissions() {

    const permittedEdit = configVars.CommentPermissions.map((item) => {
      if (isPermitted(item.taskName, taskName)) {
        return item.commentType;
      }
      if (item.taskName === taskName) {
        return item.commentType;
      }
    });
    return permittedEdit;
  }

  function isPermitted(commentTypes) {
    let permitted = false;
      commentTypes.forEach((commentType) => {
        if (taskName === commentType) {
          permitted = true;
        }
      }); 
    return permitted;
  }

  function isEditable(commentType, permitted) {
    let editable = false;
    permitted.forEach((userCommentType) => {
      if (userCommentType === commentType) {
        editable = true;
      }
    });
    return editable;
  }

  function getLabelFromLayout(fieldName, layout) {
    let label;
    layout.map((item) => {
      if (item.name === fieldName) {
        label = item.labels.any;
      }
    });
    return label;
  }

  const permitted = checkPermissions();
  const comments = Object.keys(taskvariables).map((commentType, index) => {
    if (taskvariables.hasOwnProperty(commentType) && commentType.indexOf('Comment') >= 0) {
      if (isEditable(commentType, permitted)) {
        return (
          <div className="input-group" key={index}>
            <label htmlFor="task-comment">{getLabelFromLayout(commentType, tasklayout)}</label>
            <textarea name={commentType} id={commentType} onChange={handleChange} defaultValue={taskvariables[commentType]} value={value} ></textarea>
          </div>
        );
      }
      return (
        <div className="input-group" key={index}>
        <label htmlFor="task-comment">{getLabelFromLayout(commentType, tasklayout)}</label>
        {taskvariables[commentType]}
      </div>
      );
    }
  });

  return (
    <div>
    {comments}
    </div>
  );
};

export default DocTasks;
