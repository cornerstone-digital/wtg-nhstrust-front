import React, {Component} from 'react';
import './footer.scss';

class Footer extends Component {

  render() {
    return (
      <footer>
        <div className="col-md-9 col-xs-12">
          <ul>
            <li><a href="information-page.html">About</a></li>
            <li>|</li>
            <li><a href="information-page.html">Policies &amp; Procedures</a></li>
            <li>|</li>
            <li><a href="#">Training</a></li>
            <li>|</li>
            <li><a href="#">Performance</a></li>
            <li>|</li>
            <li><a href="#">Links</a></li>
            <li>|</li>
            <li><a href="#">Help</a></li>
          </ul>
        </div>
        <div className="col-md-3 col-xs-12">
          <img src="assets/img/lozenges.jpg" alt="TEWV Intranet" />
        </div>
      </footer>
    );
  }
}

export default Footer;
