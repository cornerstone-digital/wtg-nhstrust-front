import React, { Component, PropTypes } from 'react';

class SearchBar extends Component {

  static propTypes = {
    history: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
  };

  state = {}

  componentWillReceiveProps(nextProps) {
    this.setState(this.getStateFromProps(nextProps));
  }

  getStateFromProps(nextProps) {
    const props = nextProps || this.props;

    return {
      keywords: props.location.query.search
    };
  }

  handleInputChange(event) {
    this.setState({
      keywords: event.target.value
    });
  }

  /**
   * Redirect to search page if search query exists
   * @param  {object} event form submit event
   */
  handleSubmit(event) {
    event.preventDefault();
    const query = this.state.keywords;

    if (!query) {
      return false;
    }

    this.setQuery(query);
  }

  setQuery(query) {
    return this.props.history.push({
      pathname: '/search',
      query: {
        search: query
      }
    });
  }

  render() {
    return (
      <form action="" onSubmit={this.handleSubmit.bind(this)}>
        <div className="input-group">
          <div className="input-group-btn">
            <button
              type="button"
              disabled
              className="btn btn-default dropdown-toggle"
              data-toggle="dropdown">
              All
            </button>
          </div>
          <input
            type="text"
            className="form-control"
            aria-label="search box"
            placeholder="Search for documents..."
            value={this.state.keywords}
            onChange={::this.handleInputChange} />
          <span className="input-group-btn"><button className="btn btn-default submit-btn" type="submit"> Search </button></span>
        </div>
      </form>
      );
  }
}

export default SearchBar;
