import React, { PropTypes } from 'react';

/**
 * toggles if sidebar
 */
const ToggleSidebarButton = ({onClick}) => (
  <button
    type="button"
    id="menu-toggle"
    onClick={onClick}>
    <div>
      <span className="line one"></span>
      <span className="line two"></span>
      <span className="line three"></span>
    </div>
  </button>
);

ToggleSidebarButton.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default ToggleSidebarButton;
