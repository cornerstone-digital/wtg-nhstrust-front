import React, { PropTypes, Component } from 'react';
import ReactDOMServer from 'react-dom/server';
import DropzoneComponent from 'react-dropzone-component';

import jquery from 'jquery';
import nuxeo from 'common/nuxeo/nuxeo';
import { allowedMediaTypes } from 'config/nuxeo';
import { Button, ButtonToolbar } from 'react-bootstrap';
import './fileUploader.scss';

class FileUploader extends Component {

  static propTypes = {
    docPath: PropTypes.string,
    docType: PropTypes.string,
    docProperties: PropTypes.object
  }

  state = {
    files: [],
    isUploading: false,
    uploaded: false
  }

  /**
   * If we change the docType in the selection box, we
   * need to determine what type of file that docType accepts.
   */
  componentWillReceiveProps(nextProps) {
    if (this.props.docType !== nextProps.docType) {
      this.templateConfig.acceptedFiles = allowedMediaTypes[nextProps.docType.toLowerCase()] || allowedMediaTypes.file;
    }
  }

  /**
   * Defines the styles when we are uploading a doc
   * @type {Object}
   */
  templateConfig = {
      autoProcessQueue: false,
      uploadMultiple: false,
      acceptedFiles: allowedMediaTypes[this.props.docType.toLowerCase()] || allowedMediaTypes.file,
      previewTemplate: ReactDOMServer.renderToStaticMarkup(
        <div className="files" id="previews">
          <div id="template" className="file-row">
            <div className="info">
              <p className="name" data-dz-name></p>
              <p className="size hint" data-dz-size></p>
              <strong className="error text-danger" data-dz-errormessage></strong>
            </div>
            <div className="upload-bar">
              <div
                className="progress"
                role="progressbar"
                aria-valuemin="0"
                aria-valuemax="100"
                aria-valuenow="0">
                <div className="progress-bar progress-bar-success" data-dz-uploadprogress></div>
              </div>
            </div>
            <div className="buttons">
              <button data-dz-remove className="btn btn-danger delete">
                <i className="fa fa-trash-o"></i>
                <span> Remove</span>
              </button>
            </div>
          </div>
        </div>
      )
    };

  /**
   * Generic Thumbnail config
   * @type {Object}
   */
  componentConfig = {
      postUrl: 'http://jsonplaceholder.typicode.com/posts',
      thumbnailWidth: 80,
      thumbnailHeight: 80,
      parallelUploads: 20
    };

  /**
   * Main Event Handlers for DropZone
   * @type {Object}
   */
  eventHandlers = {
      init: dropzone => {

        //Expose dropzone to window for E2E testing
        window.dropzone = window.dropzone || {};
        window.dropzone.bulkUploader = dropzone;

        this.dropzone = dropzone;
      },
      addedfile: (file) => {
        file.id = `_${Math.random().toString(36).substr(2, 9)}`;
        this.setState({
          files: this.state.files.concat(file)
        });
    },
      removedfile: (file) => {
        this.setState({
          files: this.state.files.filter(stateFile => stateFile.id !== file.id)
        });
      },
      error: file => this.handleDropzoneError(file),
      maxfilesexceeded: file => this.handleDropzoneError(file)
    };

  /**
   * Handles a file upload error
   * @param  {object} file the errored native file object
   */
  handleDropzoneError(file) {
    this.setState({
      hasError: true
    });
    jquery(file.previewElement)
      .find('.progress-bar')
      .html('Upload Failed')
      .width(0);
  }

  /**
   * Processes a Queue of native files
   * Uploads and creates them!
   */
  startBulkUploader(event) {
    event.preventDefault();
    this.setState({
      isUploading: true
    });

    this.processFileQueue(this.state.files);
  }


  /**
   * Generates a batchID from nuxeo
   * Upload a file to nuxeo with the given batchID
   * Recurse if there are files to be processed
   * @param  {array} files  queue
   * @param  {number} count count in the queue
   * @return {function}     recurse or finish
   */
  processFileQueue(files, count = 0) {
    nuxeo
      .generateBatch()
      .then(generatedBatchId => this.uploadFile(files[count], generatedBatchId))
      .then(() => {

        if (count < files.length - 1) {
          return this.processFileQueue(files, count + 1);
        }

        return this.finishUpload();

      });
  }

  /**
   * Resets the state, ready for more
   * uploads
   */
  finishUpload() {
    this.setState({
      files: [],
      uploaded: true,
      isUploading: false
    });
  }

  /**
   * Uploads a single file to Nuxeo
   * On complete - create a document
   * @param  {object} file    native file object
   * @param  {string} batchId nuxeo batch we upload to
   */
  uploadFile(file, batchId) {

    const $progressBar = jquery(file.previewElement).find('.progress-bar');
    jquery(file.previewElement).find('.buttons').hide();
    jquery(file.previewElement).find('.upload-bar').width('100%');

    nuxeo
      .uploadFiles({
        name: file.name,
        fileData: file,
        index: 0,
        batchId: batchId,
        onProgress: progress => {

          if (this.state.hasError) {
            $progressBar.width(0);
          }

          if (!this.state.hasError) {
            $progressBar.width(`${progress.upload * 100}%`);
          }
        }

      })
      .then((batch) => {
        this.createDocumentFromFile(file, batch, this.props.docPath, this.props.docType);
        $progressBar.width(`100%`);
        $progressBar
          .one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',
            () => $progressBar.html('Upload Complete'));
      });
  }

  /**
   * Creates a document and attach a file batch
   * @param  {object} file      native file object
   * @param  {object} fileBatch file referenced in nuxeo
   * @param  {string} docPath   The parent folder docpath
   * @param  {string} docType   the type of doc to be created
   */
  createDocumentFromFile(file, fileBatch, docPath, docType) {
    nuxeo.createDocument({
      type: docType,
      path: docPath,
      properties: {
        'dc:title': file.name.substr(0, file.name.lastIndexOf('.')),
        'file:content': fileBatch ? fileBatch[0].file : null,
        'file:filename': fileBatch ? fileBatch[0].filename : null,
        ...this.props.docProperties
      }
    });
  }

  render() {

    return (

      <div className="input-group">
        <label htmlFor="files">
          Files
        </label>
        <div id="actions" className="row">
          <div className="col-lg-12">
            <ButtonToolbar>
              <Button
                type="submit"
                className="btn btn-primary start"
                disabled={this.state.isUploading || !this.state.files.length}
                onClick={this.startBulkUploader.bind(this)}>
                <i className="fa fa-upload"></i>
                <span> {this.state.isUploading ? 'Uploading...' : 'Start upload'}</span>
              </Button>
            </ButtonToolbar>
          </div>
        </div>
        <div className="col-lg-12">
          <span className="fileupload-process">
            <div
              id="total-progress"
              className="progress"
              role="progressbar">
              <div className="progress-bar progress-bar-success"></div>
            </div>
          </span>
        </div>
        <DropzoneComponent
          key={this.props.docType}
          ref="dropzone"
          config={this.componentConfig}
          eventHandlers={this.eventHandlers}
          djsConfig={this.templateConfig}
          style={{overflow: 'auto'}}
          className="quick-dropzone dz-clickable dz-started">
          <div data-dz-message="Drop files to upload." className={this.state.files.length || this.state.uploaded ? 'dz-message' : 'dz-message isVisible'}>
            Drop files to upload.
            <br/>
            <span className="note">( or click )</span>
          </div>
        </DropzoneComponent>
      </div>
      );
  }

}

export default FileUploader;
