import React, { PropTypes } from 'react';
import TextTruncate from 'common/text-truncate/text-truncate';
import './pathSelect.scss';

/**
 * Renders a docPath tag to a disabled input
 * @param  {string} props.docPath the given path
 * @return {jsx}                  static input
 */
const PathSelect = ({docPath}) => (
<div className="input-group pathSelect">
  <label htmlFor="where">
    Where
  </label>
  <input
    name="where"
    disabled
    type="text"
    className="form-control disabled"
  />
  <span className="label label-success label-path">
    <TextTruncate
      line={1}
      truncateText="…"
      text={docPath}
      showTitle
    />
  </span>
</div>
);

PathSelect.propTypes = {
  docPath: PropTypes.string
};

export default PathSelect;
