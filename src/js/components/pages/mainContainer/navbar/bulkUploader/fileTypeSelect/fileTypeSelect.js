import React, { PropTypes } from 'react';
import { Input } from 'react-bootstrap';

/**
 * Renders a select box that switches docTypes
 * @param  {array}     props.allowedDocTypes  list of allowed docTypes
 * @param  {function}  props.onChange         onChange callback
 * @return {object}                           JSX
 */
const FileTypeSelect = ({allowedDocTypes, onChange}) => {
  return (
    <Input
      type="select"
      label="Document Type"
      onChange={onChange}
      placeholder="select">
      {allowedDocTypes
        .filter(docType => ['Folder', 'Workspace', 'PolicyWorkspace', 'Note'].indexOf(docType.id) === -1)
        .map((docType, index) => <option key={index} value={docType.id}> {docType.label} </option>)}
    </Input>
  );
};

FileTypeSelect.propTypes = {
  allowedDocTypes: PropTypes.array
};

export default FileTypeSelect;
