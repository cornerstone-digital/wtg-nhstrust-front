import React, { Component, PropTypes } from 'react';
import { Modal, Button } from 'react-bootstrap';
import PathSelect from './pathSelect/pathSelect';
import FileTypeSelect from './fileTypeSelect/fileTypeSelect';
import FileUploader from './fileUploader/fileUploader';
import RequiredFields from './requiredFields/requiredFields';
import nuxeo from 'common/nuxeo/nuxeo';

class BulkUploader extends Component {

  static propTypes = {
    onClose: PropTypes.func.isRequired,
    isVisible: PropTypes.bool.isRequired,
    doc: PropTypes.object
  };

  state = {
    docType: 'File'
  }

  componentDidMount() {
    this.fetchMandatoryFields(this.state.docType);
  }

  /**
   * Fetches any fields required to create
   * a document
   * @param  {string} docType the target docType
   */
  fetchMandatoryFields(docType) {
    nuxeo
      .getLayout(`${docType}-create`)
      .then(docLayout => this.setState({
          docType: docType,
          docLayout: docLayout
        }));
  }

  /**
   * Fetch required field when we change the
   * docType select box
   * @param  {object} event select onChange event
   */
  handleFileTypeSelect(event) {
    this.fetchMandatoryFields(event.target.value);
  }

  /**
   * Sets document properties on inputs taken from
   * required fields
   * @param  {string} value        property value
   * @param  {string} propertyName the document property
   */
  handleRequiredFields(value, propertyName) {
    const requiredPoperties = {};
    requiredPoperties[propertyName] = value;
    this.setState({docProperties: requiredPoperties});
  }

  /**
   * Resets the docType and call close on the parent modal
   */
  handleClose() {
    this.fetchMandatoryFields();
    this.props.onClose();
  }

  render() {
    return (
      <Modal
        id="quickupload"
        show={this.props.isVisible}
        onHide={this.props.onClose}>
        <Modal.Header closeButton>
          <Modal.Title>
            Upload
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <FileTypeSelect
              allowedDocTypes={this.props.doc.contextParameters && this.props.doc.contextParameters.allowedDocumentTypes}
              initialValue={this.state.docType}
              onChange={this.handleFileTypeSelect.bind(this)} />
            <RequiredFields layout={this.state.docLayout} onChange={this.handleRequiredFields.bind(this)} />
            <PathSelect docPath={this.props.doc.path} />
            <FileUploader
              docType={this.state.docType}
              docPath={this.props.doc.path}
              docProperties={this.state.docProperties} />
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.handleClose.bind(this)}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
      );
  }
}

export default BulkUploader;
