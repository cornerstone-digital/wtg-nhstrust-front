import React, { PropTypes } from 'react';
import RequiredFieldItem from './requiredFieldItem/requiredFieldItem';

/**
 * Filter ONLY fields that are required
 * Filter Required fields that does not
 *     relate to the title or 'content'
 *     as we generate these automatically
 *
 * @param  {object} layoutWidget a widget defined in nuxeo studio
 * @return {bool}            true if we want to include this widget
 */
function filterRequiredFields(layoutWidget) {

  //do not include file content or a title widgets
  if (layoutWidget.name === 'content' || layoutWidget.name === 'title') {
    return false;
  }

  //Do no include widgets that are NOT required
  if (!layoutWidget.propertiesByWidgetMode || !layoutWidget.propertiesByWidgetMode.edit.required) {
    return false;
  }

  return true;

}

/**
 * A list of layout Widgets required to create a document
 * @param  {object} props.layout the form layout to create a document
 * @return {object}              JSX of input fields
 */
const RequiredFields = ({layout, onChange}) => (
<div>
  {layout.widgets
     .filter(filterRequiredFields)
     .map((layoutWidget, index) =>
      <RequiredFieldItem
        onChange={onChange}
        widget={layoutWidget}
        key={index}
      />
      )}
</div>
);

RequiredFields.propTypes = {
  docLayout: PropTypes.object,
  onChange: PropTypes.func
};

export default RequiredFields;
