import React, { Component, PropTypes } from 'react';
import { Input } from 'react-bootstrap';


/**
 * Renders a input field
 * TODO make sure this renders more than a select box in the future
 */
class RequiredFieldItem extends Component {

  static propTypes = {
  widget: PropTypes.object,
  key: PropTypes.number,
  onChange: PropTypes.func
  }

  /**
   * Give an inital value to a callback on mount
   */
  componentDidMount() {
    this.props.onChange(this.props.widget.selectOptions[0].itemValue, this.props.widget.fields[0].propertyName);
  }

  render() {
    return (
      <Input
        type="select"
        key={this.props.key}
        onLoad={this.props.onChange}
        label={this.props.widget.labels.any}
        defaultValue={this.props.widget.selectOptions[0].itemValue}
        onChange={event => this.props.onChange(event.target.value, this.props.widget.fields[0].propertyName)}
        name={this.props.widget.fields[0].propertyName}>
      {this.props.widget.selectOptions.map((opt, optIndex) => (
       <option key={optIndex} value={opt.itemValue}>
         {opt.itemLabel}
       </option>)
       )}
      </Input>
      );
  }

}


export default RequiredFieldItem;
