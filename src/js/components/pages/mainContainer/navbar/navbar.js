import React, {Component, PropTypes} from 'react';
import SearchBar from './searchbar/searchbar';
import { connect } from 'react-redux';
import ToggleSidebarButton from './toggleSidebarButton/toggleSidebarButton';
import BulkUploader from './bulkUploader/bulkUploader';
import { Button } from 'react-bootstrap';
import utils from 'utils';

@connect(state => ({
    document: state.document,
}))
class Navbar extends Component {

  static propTypes = {
    handleMenuCollapse: PropTypes.func.isRequired,
    document: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
  };

  state = {
    showBulkUploader: false
  }

  close() {
    this.setState({showBulkUploader: false});
    this.props.history.replaceState(null, this.props.location.pathname);
  }

  open() {
    this.setState({showBulkUploader: true});
  }

  render() {
    return (
      <header className="top-bar">
        <div className="col-xs-9 col-sm-3 pull-right">
          <a href="/intranet" className="home"><img src="assets/img/logo-tewv.jpg" alt="TEWV Intranet" /></a>
        </div>
        <div className="col-sm-1 col-xs-3 toggle-container">
          <ToggleSidebarButton onClick={this.props.handleMenuCollapse} />
        </div>
        <div className="col-xs-12 col-sm-7">
        <SearchBar
          history={this.props.history}
          location={this.props.location} />
        </div>
        <div className="col-sm-1 pull-right hidden-sm hidden-xs">
          <Button
            disabled={!utils.canCreate(this.props.document)}
            className="quickuploadbutton"
            onClick={this.open.bind(this)}>
            <img src="assets/img/i-quick-upload.png" alt="Quick upload" />
          </Button>
          <BulkUploader
            doc={this.props.document}
            isVisible={this.state.showBulkUploader}
            onClose={this.close.bind(this)} />
        </div>
      </header>
      );
  }
}

export default Navbar;
