import React, {Component, PropTypes} from 'react';
import Navbar from 'pages/mainContainer/navbar/navbar';
import SideBar from 'pages/mainContainer/sidebar/sidebar';
import Footer from 'pages/mainContainer/footer/footer';
import { logException, raiseException } from 'common/errors/ExceptionHandler';
import NotificationSystem from 'react-notification-system';
import ErrorCodes from 'common/errors/constants/errorCodes';
import nuxeo from 'common/nuxeo/nuxeo';
import { connect } from 'react-redux';
import AlertHandler from 'common/alerts/AlertHandler';
import { throwError } from 'actions/error';
import { addAlert } from 'actions/alerts';
import utils from 'utils';
import messagingHandler from 'common/messaging/MessagingHandler';

@connect(state => ({
  user: state.user,
  error: state.error,
  alert: state.alert
}), { throwError, addAlert })

class MainContainer extends Component {

  static propTypes = {
    route: PropTypes.object.isRequired,
    children: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    error: PropTypes.any,
    throwError: PropTypes.func,
    alert: PropTypes.any,
    addAlert: PropTypes.func,
    user: PropTypes.object
  };

  state = {
    isMenuExpanded: true,
    sectionLinks: [],
    tasks: []
  };

  pollTasks() {
    nuxeo.getTasksWaiting()
      .then((response) => {
        const fetchedTaskIds = utils.pluck(response.entries, 'id');
        const currentTaskIds = utils.pluck(this.state.tasks, 'id');
        const newTaskIds = fetchedTaskIds.filter(taskId => currentTaskIds.indexOf(taskId) === -1);

        if (newTaskIds.length > 0) {

          this.alertHandler.addAlert({
            type: 'danger',
            title: 'Uncompleted Tasks',
            text: 'You have ' + newTaskIds.length + ' new uncompleted tasks',
            buttons: [
              {style: 'danger', size: 'small', 'text': 'Take Action', href: '/#/yourtasks'}
            ]
          });

          this.setState({
            tasks: response.entries
          });
        }
      })
      .catch((error) => {
        logException(raiseException(error.message));
      });
  }

  replaceTokens(string) {
    let str = string;
    str = str.replace('$username', this.props.user.properties.username);

    return str;
  }

  componentDidMount() {
    this.notificationSystem = this.refs.notificationSystem;
    this.alertHandler = this.refs.alertHandler;
    const notificationChannels = ['global', this.props.user.properties.username].concat(this.props.user.properties.groups);

    messagingHandler.identify({
      username: this.props.user.properties.username,
      name: this.props.user.properties.firstName + ' ' + this.props.user.properties.lastName,
      notificationChannels: notificationChannels
    });

    notificationChannels.forEach((channel) => {
      messagingHandler.on(channel, (response) => {
        if (response.initiatedBy !== this.props.user.properties.username.toLowerCase()) {
          switch (response.action) {
            case 'alert':
            {
              this.alertHandler.addAlert(response.content);
              break;
            }
            case 'notification': {
              response.content.level = response.content.type.toLowerCase();
              response.content.message = response.content.text;
              this.notificationSystem.addNotification(response.content);
              break;
            }
            default: {
              break;
            }
          }
        }
      });
    });

    nuxeo.getSectionIds()
      .then((response) => {
        this.setState({
          sectionLinks: response.entries
        });
      });

    // Poll every 30 seconds
    setInterval(() => {
      this.pollTasks();
    }, 30000);

    this.pollTasks();
  }

  handleMenuCollapse() {
    this.setState({
      isMenuExpanded: !this.state.isMenuExpanded
    });
  }

  componentWillReceiveProps(nextProps) {

    if (!nextProps.error && !nextProps.alert) {
      return false;
    }

    if (this.props.error !== nextProps.error) {
      if (typeof nextProps.error === 'object') {
        const CustomError = ErrorCodes.get(nextProps.error.message);

        if (nextProps.error.logError !== false) {
          logException(raiseException(nextProps.error.message));
        }

        if (CustomError) {
          this.notificationSystem.addNotification(CustomError);

          if (CustomError.redirect) {
            location.hash = CustomError.redirect;
          }
        }
      }
    }
  }

  render() {
    return (
      <div className={this.state.isMenuExpanded ? 'wrapper' : 'wrapper menu-collapse'} id="wrapper">
        <SideBar
          userDetails={this.state.userDetails}
          currentParent={this.props.children.props.route.parent}
          links={this.state.sectionLinks}
          tasks={this.state.tasks}
        />
        <div className="nopadding page" id="main-page">
          <Navbar
            handleMenuCollapse={this.handleMenuCollapse.bind(this)}
            history={this.props.history}
            location={this.props.location} />
          <AlertHandler ref="alertHandler" />

          {this.props.children}
          <div className="clearfix"></div>
          <NotificationSystem ref="notificationSystem" />
          <Footer />
        </div>
      </div>
    );
  }
}

export default MainContainer;
