import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { throwError } from 'actions/error';

@connect(state => ({
    firstName: state.user.properties.firstName,
    lastName: state.user.properties.lastName
}), {throwError})
class Sidebar extends Component {

  static propTypes = {
    currentParent: PropTypes.string,
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    links: PropTypes.array.isRequired,
    throwError: PropTypes.func,
    tasks: PropTypes.array
  };

  state = {
    isToggled: false,
    tasks: [],
    overdueTooltip: ''
  };

  isActive(routeName) {
    return this.props.currentParent === routeName;
  }

  getSectionsPathsByName(sectionName, sectionLinks) {
    let uid;
    sectionLinks.forEach((item) => {
      if (item.title === sectionName) {
        uid = item.uid;
      }
    });
    return uid;
  }

  istoggled() {
    return this.state.isToggled;
  }

  toggle(event) {
    event.preventDefault();
    this.setState({
      isToggled: !this.state.isToggled
    });
  }

  renderRoomBookerNavigation() {
    return (
      <li className={`dropdown ${this.istoggled() && 'dropdown-expand'}`} >
        <span className="toggle-dropdown" onClick={this.toggle.bind(this)}><i className={`fa ${this.istoggled() ? 'fa-chevron-up' : 'fa-chevron-down'}`}></i></span>
        <a href="room-booker-month.html" onClick={this.toggle.bind(this)}>
          <i className="fa fa-calendar"></i>
          <p>Room Booker</p>
        </a>
        <ul>
          <li><a href="#/roombooker/cal">Schedule</a></li>
          <li><a href="#/roombooker/bookingreport">Reporting</a></li>
          <li><a href="#/roombooker/admin">Administration</a></li>
        </ul>
      </li>
      );
  }

  render() {

    const taskCount = this.props.tasks.length;

    const profileImagePlaceholder = {
      backgroundImage: 'url(assets/img/profile-empty-lg.jpg)'
    };

    const policiesPath = this.getSectionsPathsByName('Policies', this.props.links);

    return (
      <div className="nopadding" id="sidebar-menu">
        <a href="#/profile" className="profile-icon top-bar">
          <span className="img" style={profileImagePlaceholder}></span> <span><p> {this.props.firstName} </p> <p> {this.props.lastName} </p>
          </span>
        </a>
        <div className="sidebar ps-container" id="menu">

          <span className="header">Main</span>
          <ul>
            <li className={this.isActive('dashboard') ? 'active' : ''}>
              <a href="#/dashboard"><i className="fa fa-home"></i> <p> Dashboard </p></a>
            </li>
            <li className={this.isActive('yourtasks') ? 'active' : ''}>
              <a href="#/yourtasks" id="tasks"><i className="fa fa-list-ol"><span className="badge">{taskCount}</span></i> <p> Tasks </p> </a>
              {/*<ReactTooltip place="right" type="error" effect="solid" />*/}
            </li>
            <li className={this.isActive('documentmanagement') ? 'active' : ''}>
              <a href="#/documentmanagement"><i className="fa fa-files-o"></i> <p> Document Management </p></a>

            </li>
            <li className={this.isActive('policies') ? 'active' : ''}>
              <a href={`#/policies/${policiesPath}`}><i className="fa fa-files-o"></i> <p> Policies & Procedures </p></a>
            </li>
            {this.renderRoomBookerNavigation()}
            <li>
              <a href="https://www.google.co.uk/" target="_blank"> <i className="fa fa-globe"></i> <p>Internet</p> </a>
            </li>
          </ul>
        </div>
      </div>
      );
  }
}

export default Sidebar;
