import React, { Component } from 'react';

class Colleague extends Component {


  render() {

    const colleagueProfileImage01 = {
      backgroundImage: 'url(assets/img/profile-colleague01.jpg)'
    };

    const colleagueProfileImage02 = {
      backgroundImage: 'url(assets/img/profile-colleague02.jpg)'
    };

    const colleagueProfileImage03 = {
      backgroundImage: 'url(assets/img/profile-colleague03.jpg)'
    };

    const colleagueProfileImage04 = {
      backgroundImage: 'url(assets/img/profile-colleague04.jpg)'
    };

    return (
        <div className="panel panel-primary panel-colleagues">
          <div className="panel-heading">
            <h3 className="panel-title">Joe's Colleagues</h3>
          </div>
          <div className="panel-body no-padding">
            <div className="no-padding col-md-3 col-sm-3 clear">
              <a href="#">
                <div className="profile-image" style={colleagueProfileImage01}></div>
                <h2>Persons Name</h2>
                <p> job title </p>
              </a>
            </div>
            <div className="no-padding col-md-3 col-sm-3">
              <a href="#">
                <div className="profile-image" style={colleagueProfileImage02}></div>
                <h2>Persons Name</h2>
                <p> job title </p>
              </a>
            </div>
            <div className="no-padding col-md-3 col-sm-3">
              <a href="#">
                <div className="profile-image" style={colleagueProfileImage03}></div>
                <h2>Persons Name</h2>
                <p> job title </p>
              </a>
            </div>
            <div className="no-padding col-md-3 col-sm-3">
              <a href="#">
                <div className="profile-image" style={colleagueProfileImage04}></div>
                <h2>Persons Name</h2>
                <p> job title </p>
              </a>
            </div>
            <div className="no-padding col-md-3 col-sm-3 clear hide-me">
              <a href="#">
                <div className="profile-image" style={colleagueProfileImage03}></div>
                <h2>Persons Name</h2>
                <p> job title </p>
              </a>
            </div>
            <div className="no-padding col-md-3 col-sm-3 hide-me">
              <a href="#">
                <div className="profile-image" style={colleagueProfileImage01}></div>
                <h2>Persons Name</h2>
                <p> job title </p>
              </a>
            </div>
            <div className="no-padding col-md-3 col-sm-3 hide-me">
              <a href="#">
                <div className="profile-image" style={colleagueProfileImage04}></div>
                <h2>Persons Name</h2>
                <p> job title </p>
              </a>
            </div>
            <div className="no-padding col-md-3 col-sm-3 hide-me">
              <a href="#">
                <div className="profile-image" style={colleagueProfileImage02}></div>
                <h2>Persons Name</h2>
                <p> job title </p>
              </a>
            </div>
            <div className="no-padding col-md-3 col-sm-3 clear hide-me">
              <a href="#">
                <div className="profile-image" style={colleagueProfileImage04}></div>
                <h2>Persons Name</h2>
                <p> job title </p>
              </a>
            </div>
            <div className="no-padding col-md-3 col-sm-3 hide-me">
              <a href="#">
                <div className="profile-image" style={colleagueProfileImage03}></div>
                <h2>Persons Name</h2>
                <p> job title </p>
              </a>
            </div>
          </div>
          <div className="panel-footer">
            <button id="toggle-colleagues">
              show more <span className="fa fa-chevron-down"></span>
            </button>
          </div>
        </div>
      );
  }

}

export default Colleague;
