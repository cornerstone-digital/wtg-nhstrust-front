import React, { Component, PropTypes } from 'react';

class Profile extends Component {

  static propTypes = {
    profileData: PropTypes.object
  };

  render() {

    const profileImagePlaceholder = {
      backgroundImage: 'url(assets/img/profile-thumb.jpg)'
    };

    const profile = this.props.profileData;

    return (
      <section>
            <div className="panel panel-primary panel-profile-info">
              <div className="panel-body">
                <div className="profile-img" style={profileImagePlaceholder}></div>
                <h1>{profile.firstname} {profile.lastname}</h1>
                <span className="job-title">job title</span>
                <a href="mailto:joe.bloggs@tewv.nhs.uk" className="email-button">email now</a>
                <a href="tel:0191 123 456"><span className="fa fa-phone"></span>0191 123 456</a>
                <a href="mailto:joe.bloggs@tewv.nhs.uk"><span className="fa fa-envelope-o"></span>{profile.email}</a>
              </div>
              <div className="panel-footer">
                <h2>About me</h2>
                <p>
                  cies erat. Nullam a nulla at metus pretium porta. Suspendisse vitae velit massa. Quisque non turpis nec nunc efficitur volutpat sed sed neque. Vivamus vehicula a
                  nisl id ornare. Fusce gravida lobortis sapien vel congue. Maecenas vel justo ut tortor dictum
                </p>
              </div>
            </div>
        <div className="clearfix"></div>
      </section>
      );
  }

}

export default Profile;
