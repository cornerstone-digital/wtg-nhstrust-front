import React, { Component, PropTypes } from 'react';
import Breadcrumbs from 'common/breadcrumbs/breadcrumbs';
import nuxeo from 'common/nuxeo/nuxeo';
import Profile from './profilePanel/profilePanel';
import Document from './documentsPanel/documentsPanel';
import Spinner from 'common/spinner/spinner.js';
import { connect } from 'react-redux';
import { throwError } from 'actions/error';

@connect(state => ({
    profile: state.user.properties
}), {throwError})
class ProfilePage extends Component {

  static propTypes = {
    route: PropTypes.object.isRequired,
    profile: PropTypes.object.isRequired,
    throwError: PropTypes.func
  }

  componentDidMount() {
    nuxeo
      .getAllDocuments()
      .then(response => this.setState({
          document: response.entries
        }))
      .catch((error) => {
        this.props.throwError(error);
      });
  }

  render() {

    if (!this.state) {
      return <Spinner />;
    }

    const firstName = this.props.profile.firstName;
    const lastName = this.props.profile.lastName;

    return (
      <section>
        <Breadcrumbs breadcrumbData={[{route: this.props.route.path, title: `${firstName} ${lastName}`}]} breadcrumbRoot={this.props.route.parent} />
        <div id="page-body">
          <div className="profile-sidebar">
            <Profile profileData={this.props.profile} />
          </div>
          <div className="profile-content">
            <div className="col-lg-12">
              <Document documentData={this.state.document} profileData={this.props.profile} />
            </div>
          </div>
        </div>
        <div className="clearfix"></div>
      </section>
      );
  }

}

export default ProfilePage;
