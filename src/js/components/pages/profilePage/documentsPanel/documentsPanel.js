import React, { Component, PropTypes } from 'react';
import utils from 'utils';

class Document extends Component {

  static propTypes = {
    documentData: PropTypes.array.isRequired,
    profileData: PropTypes.object.isRequired
  }

  render() {
    const documents = this.props.documentData;
    const profile = this.props.profileData;
    const firstname = profile.firstName;


    return (
          <div className="panel panel-primary panel-profile-documents">
            <div className="panel-heading">
              <h3 className="panel-title">{firstname}</h3>
              <a href="#/yourdocuments" className="view-all" title="view all"></a>
            </div>
            <div className="panel-body">
              <span className="hint">These are all of joe's documents that you currently have access to</span>
              <div className="document-list slick-initialized slick-slider" role="toolbar">
                <div aria-live="polite" className="slick-list draggable">
                  <div className="slick-track" role="listbox">
                    <div
                      className="panel-page slick-slide slick-cloned"
                      data-slick-index="-1"
                      aria-hidden="true"
                      tabIndex="-1">
                      <If condition={documents.length}>
                      <ul>
                        {documents
                          .filter((item, index) => index < 18)
                          .map((item, index) => (
                        <li className="col-md-4 col-sm-6 col-xs-12" key={index}>
                          <a href={`${utils.getDocLink(item)}`}><i className={utils.getDocIcon(item.type)}></i>{item.title}</a>
                        </li>
                        ))}

                      </ul>
                      </If>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      );
  }

}

export default Document;
