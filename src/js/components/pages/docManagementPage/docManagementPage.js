import React, {Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Breadcrumbs from 'common/breadcrumbs/breadcrumbs';
import PageHeader from 'common/pageHeader/pageHeader';
import PinnedWorkspaces from 'pages/docManagementPage/pinnedWorkspaces/pinnedWorkspaces';
import Favorites from 'pages/docManagementPage/favorites/favorites';
import Spinner from 'common/spinner/spinner.js';
import YourDocuments from 'pages/docManagementPage/yourdocuments/yourdocuments';
import YourTasks from 'pages/docManagementPage/yourtasks/yourtasks';
import RecentActivity from 'pages/docManagementPage/recentActivity/recentActivity';
import nuxeo from 'common/nuxeo/nuxeo';
import { throwError } from 'actions/error';

@connect(state => ({
  user: state.user
}), {throwError})

class DocManagementPage extends Component {

  static propTypes = {
    route: PropTypes.object.isRequired,
    throwError: PropTypes.func
  };

  state = {

  }

  componentDidMount() {
      Promise.all([
        nuxeo.getFavorites().then(response => this.setState({ favorites: response.entries })),
        nuxeo.getAllDocuments().then(response => this.setState({ yourdocuments: response.hits })),
        nuxeo.getTasksWaiting().then(response => this.setState({ taskswaiting: response.entries })),
        nuxeo.getLastModified().then(response => this.setState({ lastmodifiedfiles: response.hits })),
      ])
      .catch((error) => {
        this.props.throwError(error);
      });
  }

  render() {
    return (
      <section>
        <Breadcrumbs breadcrumbData={[]} breadcrumbRoot={this.props.route.parent} disabled/>
        <PageHeader pageTitle="Document Management" />
        <div id="page-body">
          <div className="col-lg-8">
            <If condition={this.state.favorites}>
              <div>
                <PinnedWorkspaces favorites={this.state.favorites} />
                <Favorites favorites={this.state.favorites} />
              </div>
            <Else />
              <div><Spinner /></div>
            </If>
            <If condition={this.state.yourdocuments}>
              <YourDocuments yourdocuments={this.state.yourdocuments} />
            <Else />
            <div><Spinner /></div>
            </If>
          </div>
          <div className="col-lg-4">
            <If condition={this.state.taskswaiting}>
              <YourTasks tasks={this.state.taskswaiting} />
            <Else />
            <div><Spinner /></div>
            </If>
            <If condition={this.state.lastmodifiedfiles}>
              <RecentActivity lastmodifiedfiles={this.state.lastmodifiedfiles} />
            <Else />
            <div><Spinner /></div>
            </If>
          </div>
        </div>
        <div className="clearfix"></div>
      </section>
    );
  }
}

export
default DocManagementPage;
