import React from 'react';
import utils from 'utils';

const Favorites = ({favorites}) => {

  const items = favorites.filter(item => item.type !== 'Workspace' && item.type !== 'Favorites');

  return (
    <div className="panel panel-primary panel-documents">
      <div className="panel-heading">
        <h3 className="panel-title">Your Favourite Documents &amp; Folders</h3>
        <a href="#/yourFavorite" className="view-all" title="view all"></a>
      </div>
      <div className="panel-body">
        <div className="panel-page">
          <If condition={items.length}>
            <ul>
              {items.map((item, index) => (
               <li className="col-md-4 col-sm-6 col-xs-12" key={index}>
                 <a href={utils.getDocLink(item)}><i className={utils.getDocIcon(item.type)}></i>{item.title}</a>
               </li>)
               )}
            </ul>
            <Else/>
            <h4 className="text-muted text-center state-empty"><em>Favorites contains no documents</em></h4>
          </If>
        </div>
      </div>
    </div>
    );
};

export default Favorites;
