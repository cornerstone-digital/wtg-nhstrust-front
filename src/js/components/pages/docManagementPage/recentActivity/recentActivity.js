import React, { PropTypes, Component } from 'react';
import Time from 'react-time';
import './recentActivity.scss';


class RecentActivity extends Component {

  static propTypes = {
     taskswaiting: PropTypes.array,
     lastmodifiedfiles: PropTypes.array
  };

  render() {

    const items = this.props.lastmodifiedfiles
      .filter((item, index) => index < 2)
      .map((item, index) => (
      <li key={index}>
        <span className="name pull-left">{item._source['dc:lastContributor']}</span>
        <span className="date pull-right"><Time value={item._source['dc:modified']} format="Do MMM 'YY" /></span>
        <p>
          {item._source['dc:title']}
          {' '}
          ({item._source['ecm:path']})
        </p>
      </li>
      )
    );

    return (
      <div className="panel panel-primary panel-recent-activity">
        <div className="panel-heading">
          <h3 className="panel-title">Recent Activity</h3>
        </div>
        <div className="panel-body">
          <If condition={items.length}>
            <ul className="list-activity">
              {items}
            </ul>
            <Else/>
            <h4 className="text-muted text-center state-empty"><em>No recent activity</em></h4>
          </If>
        </div>
      </div>
      );
  }
}

export default RecentActivity;
