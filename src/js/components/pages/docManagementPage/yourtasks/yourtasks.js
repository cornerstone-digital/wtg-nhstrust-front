import React, {Component, PropTypes} from 'react';
import Time from 'react-time';
import './yourtasks.scss';

class YourTasks extends Component {

  static propTypes = {
    tasks: PropTypes.array.isRequired,
    docType: PropTypes.string
  };

  getLink(item) {
    switch (item.type) {
      case 'File':
        return `#doc/${item.type}/${item.uid}`;
      case 'Folder':
        return `#/folder/${item.uid}`;
      case 'Workspace':
        return `#/workspace/${item.uid}`;
      default:
        return `#doc/${item.type}/${item.uid}`;
    }
  }

  isDue(dueDate) {
    const currentDate = new Date().getTime();
    const dueDateUTC = new Date(dueDate).getTime();
    if (dueDateUTC >= currentDate) {
      return false;
      }
    return true;
  }

  render() {
      const totalNumber = this.props.tasks.length;

      const items = this.props.tasks.map((item, index) => {
        if (index <= 2) {
          return (
            <div className={this.isDue(item.dueDate) ? 'item clearfix due' : 'item clearfix'} key={index}>
              <div className="top">
                <h2>{item.documentTitle}</h2>
                <p> {item.directive} </p>
              </div>
              <div className="bottom">
                <span className="date pull-left">Due: <Time value={item.dueDate} format="Do MMM 'YY" /></span>
                <a className="pull-right" href={`#/doc/${item.docref}`}>Take Action</a>
              </div>
            </div>

          );  
        }
    });

    return (
      <div className="panel panel-primary panel-tasks">
        <div className="panel-heading">
          <h3 className="panel-title">Your Tasks ({totalNumber})</h3>
          <a href="#/yourtasks" className="view-all" title="view all"></a>
        </div>
        <div className="panel-body">
            <If condition={items.length}>
              <ul>
                {items}
              </ul>
              <Else/>
              <h4 className="text-muted text-center state-empty"><em>No tasks available</em></h4>
            </If>
        </div>
      </div>
    );
  }
}

export default YourTasks;
