import React from 'react';
import utils from 'utils';
import TextTruncate from 'common/text-truncate/text-truncate';
import './yourDocuments.scss';

const YourDocuments = ({yourdocuments}) => (
<div className="panel panel-primary panel-documents max-height">
  <div className="panel-heading">
    <h3 className="panel-title">Your Documents</h3>
    <a href="#/yourdocuments" className="view-all" title="view all"></a>
  </div>
  <div className="panel-body">
    <div className="panel-page">
      <If condition={yourdocuments.length}>
        <ul>
          {yourdocuments
             .concat().sort(::utils.sortByFolderish)
             .map((item, index) => (
             <li className="col-md-4 col-sm-6 col-xs-12" key={index}>
               <a href={`${utils.getDocLink(item)}`}>

                <TextTruncate icon={utils.getDocIcon(item._source['ecm:primaryType'])} line={1} truncateText="…"text={item._source['dc:title']} showTitle /></a>

             </li>))}
        </ul>
        <Else/>
        <h4 className="text-muted text-center state-empty"><em>No documents found</em></h4>
      </If>
    </div>
  </div>
</div>
);

export default YourDocuments;
