import React from 'react';

const PinnedWorkspaces = ({favorites}) => {

  const items = favorites
    .filter(item => item.type === 'Workspace' && item.type !== 'PolicyWorkspace')
    .map((item, index) => (
    <li className="col-sm-4 col-xs-12" key={index}>
      <a href={`#/workspace/${item.uid}`}>
        {item.title}
      </a>
    </li>
    ));

  return (
    <div className={`panel panel-primary panel-workspace ${items.length && 'no-bg'}`}>
      <div className="panel-heading">
        <h3 className="panel-title">Your Pinned Workspaces</h3>
        <a href="#/yourPinnedWorkspaces" className="view-all" title="view all"></a>
      </div>
      <div className="panel-body">
        <div className="panel-page">
          <If condition={items.length}>
            <ul>
              {items}
            </ul>
            <Else/>
            <h4 className="text-muted text-center state-empty"><em>No pinned workspaces</em></h4>
          </If>
        </div>
      </div>
    </div>
    );

};

export default PinnedWorkspaces;
