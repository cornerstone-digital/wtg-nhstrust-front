import React from 'react';
import RecentNewsItem from './recentNewsItem';

/**
 * Takes a list of news items and renders them
 * @param  {array} {newsList}) an list of documents
 *                             inside the news section

 * @return {object}            the news panel
 */
const RecentNews = ({newsList}) => (
<div className="panel panel-primary">
  <div className="panel-heading">
    <h3 className="panel-title">Latest News</h3>
  </div>
  <div className="panel-body no-padding">
    <ul className="list-group">
      {newsList
         .sort((prev, next) => new Date(next.created) - new Date(prev.created))
         .filter((item, index) => index < 4)
         .map((newsItem, index) => <RecentNewsItem key={index} item={newsItem} />)}
    </ul>
  </div>
</div>
);

export default RecentNews;
