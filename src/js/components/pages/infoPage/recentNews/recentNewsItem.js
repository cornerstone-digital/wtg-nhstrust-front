import React from 'react';
import Time from 'react-time';
/**
 * Template for a basic news item
 * @type {object}
 */
const NewsItem = ({item}) => (
<a className="list-group-item" href={`#/info/${item.uid}`}>
  <h4>{item.title}</h4>
  <p><Time value={item.created} format="Do MMMM 'YY" /></p>
</a>
);

export default NewsItem;
