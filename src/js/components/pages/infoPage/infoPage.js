import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Breadcrumbs from 'common/breadcrumbs/breadcrumbs';
import Spinner from 'common/spinner/spinner.js';
import RecentNews from './recentNews/recentNews';
import { throwError } from 'actions/error';

import nuxeo from 'common/nuxeo/nuxeo';
import './infoPage.scss';

@connect(state => ({
  user: state.user
}), {throwError})

class InfoPage extends Component {

  static propTypes = {
    params: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
    throwError: PropTypes.func
  };

  state = {
    loading: true
  };

  componentDidMount() {
    this.fetchData(this.props.params.docId);
  }

  /**
   * We need to reset the component when we recieve a new docId from
   * The router
   *
   * @param  {[type]} nextProps the docId for the target news article
   */
  componentWillReceiveProps(nextProps) {
    this.setState({loading: true});
    this.fetchData(nextProps.params.docId);
  }

  /**
   * Fetches all data and sets state
   * @param  {[type]} docId Id for a news article
   */
  fetchData(docId) {
    Promise.all([
      nuxeo.getDocumentById(docId),
      nuxeo.getNews(),
    ])
      .then(response => this.setState({
          document: response[0],
          news: response[1],
          loading: false
        }))
      .catch((error) => {
        this.props.throwError(error);
      });
  }

  render() {

    if (this.state.loading) {
      return <Spinner />;
    }

    return (
      <section>
        <Breadcrumbs breadcrumbData={[{route: this.props.route.path, title: this.state.document.title}]} breadcrumbRoot={this.props.route.parent} />
        <div id="page-title">
          <div className="col-lg-7">
            <h1>{this.state.document.title}</h1>
          </div>
        </div>
        <div id="page-body" className="page-info">
          <div className="col-lg-7">
            <div className="panel panel-info">
              <div className="panel-body panel-content">
                <div dangerouslySetInnerHTML={{__html: this.state.document.properties['note:note']}} />
              </div>
            </div>
          </div>
          <div className="col-lg-4 col-lg-offset-1">
            <RecentNews newsList={this.state.news} />
          </div>
        </div>
        <div className="clearfix"></div>
      </section>
      );
  }

}

export default InfoPage;
