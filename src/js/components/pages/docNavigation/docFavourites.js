import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import utils from 'common/utils/utils';

@connect(state => ({
    user: state.user,
}), {

})
class DocFavourites extends Component {
  static propTypes = {
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
  }

  componentWillMount() {
    const a = window.document.createElement('a');

    a.href = `/path/${this.getPath()}`;
    a.search = utils.queryToString(this.props.location.query);

    this.props.history.push(a);
  }

  getPath() {
    return `/Intranet/UserWorkspaces/${this.props.user.id}/Favorites`;
  }

  render() {
    return (<div />);
  }
}

export default DocFavourites;
