import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import {throwError} from 'actions/error';
import Spinner from 'common/spinner/spinner.js';
import utils from 'common/utils/utils';
import { requestDocument } from 'actions/document';

@connect(state => ({
    user: state.user,
    document: state.document,
}), {requestDocument, throwError})
class DocPath extends Component {
  static propTypes = {
    params: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    list: PropTypes.object.isRequired,
    single: PropTypes.object.isRequired,
    routes: PropTypes.array.isRequired,
    requestDocument: PropTypes.func.isRequired,
    document: PropTypes.object,
  }

  componentDidMount() {
    this.fetch(this.props);
  }

  componentWillReceiveProps(nextProps) {
    const oldSplat = this.props.params.splat;
    const newSplat = nextProps.params.splat;
    const oldDoc = this.props.document.current;
    const newDoc = nextProps.document.current;
    const safeDocPath = utils.getUrlSafeDocPath((newDoc || {}).path);

    if (oldSplat !== newSplat) {
      //path has changed, does it match the current doc?
      if (!newDoc || safeDocPath !== newSplat) {
        // it doesn't, must be user navigation, fetch new doc
        this.fetch(nextProps);
      }
    } else if (newDoc && (!oldDoc || oldDoc.path !== newDoc.path)) {
      // document has changed, does the path need normalising?
      if (safeDocPath !== newSplat) {
        this.props.history.replace(`/path/${safeDocPath}`);
      }
    }
  }

  componentWillUnmount() {
    delete this.request;
  }

  fetch(props) {
    const splat = props.params.splat;

    return props.requestDocument(splat);
  }

  isFolderish() {
    const mixins = this.props.document.current.facets || this.props.document.current._source['ecm:mixinType'];

    return utils.isFolderish(mixins);
  }

  isCollection() {
    return (this.props.document.current.type || this.props.document.current._source['ecm:primaryType']) === 'Favorites';
  }

  isViewing() {
    const tip = this.props.routes[this.props.routes.length - 1];

    // todo: perform this check in a better way
    return tip.path === '**';
  }

  render() {
    const document = this.props.document.current;

    return (
      <div>
        <If condition={!document}>
          <Spinner />
        <Else />
          <If condition={this.isViewing() && (this.isFolderish() || this.isCollection())}>
            {React.cloneElement(this.props.list, {
              title: document.title,
              document: document
            })}
          <Else />
            {React.cloneElement(this.props.single, {
              document: document
            })}
          </If>
        </If>
      </div>
    );
  }
}

export default DocPath;
