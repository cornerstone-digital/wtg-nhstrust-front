import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import { uncacheDocument } from 'actions/document';
import {throwError} from 'actions/error';

@connect(state => ({
    user: state.user,
}), {
  uncacheDocument,
  throwError
})
class DocSearch extends Component {
  static propTypes = {
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    list: PropTypes.object.isRequired,
    routes: PropTypes.array.isRequired,
    uncacheDocument: PropTypes.func.isRequired,
  }

  componentWillMount() {
    this.props.uncacheDocument();
  }

  componentWillReceiveProps() {
    this.props.uncacheDocument();
  }

  render() {
    const search = this.props.location.query.search;

    return (
      <div>
        {React.cloneElement(this.props.list, {
          title: (search && search.length) ? `Search results for "${search}"` : 'All documents',
          breadcrumbs: [
            {
              title: 'Search'
            }
          ]
        })}
      </div>
    );
  }
}

export default DocSearch;
