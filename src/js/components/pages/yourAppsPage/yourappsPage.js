import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Breadcrumbs from 'common/breadcrumbs/breadcrumbs';
import PageHeader from 'common/pageHeader/pageHeader';
import Spinner from 'common/spinner/spinner.js';
import AppList from './appList/appList';
import nuxeo from 'common/nuxeo/nuxeo';
import { throwError } from 'actions/error';

@connect(state => ({
  user: state.user
}), {throwError})

class YourAppsPage extends Component {

  static propTypes = {
    route: PropTypes.object.isRequired,
    throwError: PropTypes.func
  }

  state = {
    apps: []
  }

  componentDidMount() {
    nuxeo.getAllApps()
      .then((response) => {
        this.setState({
          apps: response.entries
        });
      })
      .catch((error) => {
        this.props.throwError(error);
      });
  }

  render() {

    if (!this.state) {
      return <Spinner />;
    }

    return (
      <section>
        <Breadcrumbs breadcrumbData={[{route: this.props.route.path, title: 'Your Apps'}]} breadcrumbRoot={this.props.route.parent} />
        <PageHeader pageTitle="Your Apps" />
        <div id="page-body" className="app-list-page">
          <div className="fluid-container">
            <div className="col-lg-12">
              <AppList items={this.state.apps} />
            </div>
          </div>
        </div>
        <div className="clearfix"></div>
      </section>
      );
  }

}

export default YourAppsPage;
