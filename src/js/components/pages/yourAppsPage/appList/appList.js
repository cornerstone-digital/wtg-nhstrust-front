import React, { Component, PropTypes } from 'react';
import './appList.scss';

class AppList extends Component {
  static propTypes = {
    items: PropTypes.array
  }

  render() {
    return (
      <ul>
        {this.props.items.map((item, index) => {
          const target = item.title !== 'Room Booker' ? '_blank' : '_self';
          return (
            <li className="app col-md-3 col-xs-6" key={index}>
              <a href={item.properties['application:Invocation']} target={target} >
                <img src={((item.properties || {})['file:content'] || {}).data}/>
              </a>
            </li>
          );
        })}
      </ul>
    );
  }
}

export default AppList;
