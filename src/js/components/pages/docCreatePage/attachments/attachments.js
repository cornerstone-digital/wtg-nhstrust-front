import React, { PropTypes, Component } from 'react';
import Dropzone from 'common/dropzone/dropzone';

class Attachments extends Component {

  static propTypes = {
    uploadHandler: PropTypes.func.isRequired,
    layout: PropTypes.object.isRequired
  }

  /**
   * Fetches a label from a given layout type
   * @return {string} A label if found - or a default label
   */
  getLabel(widgets) {

    const filesWidget = widgets.filter(widget => widget.name === 'files')[0];

    if (!filesWidget) {
      return 'Add Attachments';
    }

    if (filesWidget) {
      return filesWidget.labels.any;
    }

  }

  render() {
    return (
      <div className="panel panel-primary">
        <div className="panel-heading">
          <h3 className="panel-title">
            {this.getLabel(this.props.layout.widgets)}
          </h3>
        </div>
        <div className="panel-body">
          <Dropzone name="attachments" uploadHandler={this.props.uploadHandler} />
        </div>
      </div>
      );
  }

}

export default Attachments;
