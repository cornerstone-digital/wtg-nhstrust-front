import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Breadcrumbs from 'common/breadcrumbs/breadcrumbs';
import PageHeader from 'common/pageHeader/pageHeader';
import Attachments from './attachments/attachments';
import Spinner from 'common/spinner/spinner.js';
import FormBuilder from 'common/formBuilder/formBuilder.js';
import utils from 'utils';

import nuxeo from 'common/nuxeo/nuxeo';
import './docCreatePage.scss';
import { throwError } from 'actions/error';

@connect(state => ({
  user: state.user
}), { throwError })

class DocumentCreateNewPage extends Component {

  static propTypes = {
    params: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
    throwError: PropTypes.func
  };

  state = {
    properties: {},
    loading: true
  };

  componentDidMount() {
    Promise.all([
      nuxeo.getDocumentById(this.props.params.parentId),
      nuxeo.getLayout(`layout@${this.props.params.docType}-create`),
      nuxeo.getSchemas(this.props.params.docType),
      nuxeo.getWorkspaceTemplates()
    ])
      .then(data => this.setState({
          path: data[0].path,
          type: this.props.params.docType,
          layout: data[1],
          schemas: data[2].schemas.map(schema => schema.name),
          breadcrumbs: data[0].contextParameters.breadcrumb.entries,
          workspaceTemplates: data[3].entries,
          loading: false
        }))
      .catch((error) => {
        this.props.throwError(error);
      });
  }

  /**
   * Checkes if this Filetype has the given schema
   * @param  {string}  schema a single schema
   * @return {Boolean}        returns true if there is a match
   */
  hasSchema(schema) {
    return this.state.schemas.indexOf(schema) !== -1;
  }

  /**
   * Adds a file batch to the `Attachments` panel
   * @param  {array} fileBatch a object containing the
   *                             batchId and fileIndex
   */
  handleAttachments(fileBatch) {
    this.setState({
      properties: Object.assign(this.state.properties, {
        'files:files': fileBatch
      })
    });
  }

  /**
   * On form submit - lets create a document!
   * @param  {objext} formData data from FormBuilder
   */
  handleSubmit(formData) {

    this.setState({
      properties: Object.assign(this.state.properties, formData),
      loading: true
    });

    if (formData.createFromTemplate) {
      return nuxeo
        .createDocumentFromTemplate(this.state)
        .then((doc) => this.props.history.replaceState(null, utils.getDocLink(doc).substring(1)));
    }

    return nuxeo
      .createDocument(this.state)
      .then((doc) => this.props.history.replaceState(null, utils.getDocLink(doc).substring(1)));
  }

  render() {

    if (this.state.loading) {
      return <Spinner />;
    }

    const breadcrumbData = this.state.breadcrumbs.concat();
    breadcrumbData.push({
      route: this.props.route.path,
      title: `Create a new ${utils.getDocTypeLabel(this.props.params.docType)}`
    });

    return (
      <section className="page-doc-create">
        <Breadcrumbs breadcrumbData={breadcrumbData} breadcrumbRoot={this.props.route.parent} />
        <PageHeader pageTitle={`Create a new ${utils.getDocTypeLabel(this.props.params.docType)}`} />
        <div id="page-body">
          <div className="col-lg-8">
            <div className="panel panel-primary panel-main">
              <div className="panel-heading">
                <h3 className="panel-title">Create {this.props.params.docType}</h3>
              </div>
              <div className="panel-body">
                <FormBuilder
                  workspaceTemplates={this.hasSchema('workspace_schema') ? this.state.workspaceTemplates : null}
                  onSubmit={this.handleSubmit.bind(this)}
                  onCancel={this.props.history.goBack}
                  schemas={this.state.schemas}
                  layout={this.state.layout}
                  buttonLabel="Create" />
              </div>
            </div>
          </div>
          <div className="col-lg-4">
            <If condition={this.hasSchema('files')}>
              <Attachments layout={this.state.layout} uploadHandler={this.handleAttachments.bind(this)} />
            </If>
          </div>
        </div>
        <div className="clearfix"></div>
      </section>
      );
  }
}

export default DocumentCreateNewPage;
