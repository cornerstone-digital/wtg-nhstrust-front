import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import Router from 'react-router';
import routes from 'config/routes';
import nuxeo from 'common/nuxeo/nuxeo';
import isIE8 from 'is-ie8';
import bowser from 'bowser';
import ga from 'react-ga';
import { logException, raiseException } from 'common/errors/ExceptionHandler';

ga.initialize('UA-72320439-1');

/**
 * If in IE enviroment or in production
 * Load the Promise & Babel polyfills
 */
if (process.env.NODE_ENV === 'production' || bowser.msie) {
  require('babel/polyfill');
}

/**
 * Initialise Nuxeo and Render the app to the
 * DOM - Disable rendering if IE8
 */
if (!isIE8()) {
  nuxeo.init().then(renderApp);
}

/**
 * Add a global Redux store to our app
 * Set the user and render
 */
function renderApp(userData) {
  const store = require('./redux/create')({user: userData});

  ReactDOM.render(
    <Provider store={store}>
      <Router routes={routes} onUpdate={logPageView} />
    </Provider>, document.getElementById('app')
  );
}

/**
 * Logs a basic pageView to google Analytics
 */
function logPageView() {
    ga.pageview(this.state.location.pathname);
}

window.onerror = function onError(errorMsg, url, lineNumber) {
  const error = 'Error: ' + errorMsg + ' Script: ' + url + ' Line: ' + lineNumber;
  logException(raiseException(error));
};
