/**
 * Sets a user object on the Redux store
 * We don't actually modify the user here
 * instead we set the user at runtime only once
 */
function userReducer(state = {user: null}) {
  return state;
}

export default userReducer;
