const THROW = 'nhstrust/error/THROW';
const RESET = 'nhstrust/error/RESET';

function errorReducer(state = {error: null}, action = {}) {
  switch (action.type) {
    case THROW:
      return action.data;
    case RESET:
      return null;
    default:
      return state;
  }
}

function throwError(errorData) {
  return {
    type: THROW,
    data: errorData
  };
}

function resetError() {
  return {
    type: RESET,
    data: null
  };
}

export default errorReducer;
export { throwError };
export { resetError };
