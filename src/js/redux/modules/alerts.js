const ADD = 'nhstrust/alert/ADD';
const RESET = 'nhstrust/alert/RESET';

function alertReducer(state = {alert: null}, action = {}) {
  switch (action.type) {
    case ADD:
      return action.data;
    case RESET:
      return null;
    default:
      return state;
  }
}

function addAlert(alert) {
  return {
    type: ADD,
    data: alert
  };
}

function resetAlerts() {
  return {
    type: RESET,
    data: null
  };
}

export default alertReducer;
export { addAlert };
export { resetAlerts };
