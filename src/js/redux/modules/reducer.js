import { combineReducers } from 'redux';
import userReducer from './user';
import roomBookerReducer from 'nhstrustRoombooker/src/components/redux/reducers_intranet.rb';
import docReducer from './document';
import listReducer from './list';
import errorReducer from './error';
import alertReducer from './alerts';

/**
 * Combines all reducers to be injected into a Redux store
 */
const reducers = combineReducers({
  user: userReducer,
  roomBooker: roomBookerReducer,
  document: docReducer,
  list: listReducer,
  error: errorReducer,
  alert: alertReducer
});

export default reducers;
