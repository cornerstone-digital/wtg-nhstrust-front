import nuxeo from 'common/nuxeo/nuxeo';
import utils from 'common/utils/utils';

const REQUEST = 'nhstrust/list/request';
const RECEIVE = 'nhstrust/list/receive';
const DELETEDOCUMENTS = 'nhstrust/list/deletedocuments';

function reducer(state = {}, action = {}) {
  switch (action.type) {
    case REQUEST:
      return {
        ...state,
        ready: false,
        query: action.query,
      };
    case RECEIVE:
      return {
        ...state,
        ready: true,
        entries: action.entries,
      };
    case DELETEDOCUMENTS:
      return {
        ...state,
        entries: state.entries.filter(item => action.ids.indexOf(item._id) === -1),
      };
    default:
      return state;
  }
}

function refreshList() {
  return (dispatch, getState) => {
    const query = getState().list.query;

    return dispatch(requestList(query));
  };
}

function requestList(query) {
  return (dispatch, getState) => {
    dispatch({
      type: REQUEST,
      query: query,
    });

    return nuxeo.search(query)
    .then(response => {
      const currentQuery = getState().list.query;

      if (JSON.stringify(query) === JSON.stringify(currentQuery)) {
        dispatch(receiveList(response.hits));
      }
    });
  };
}

function receiveList(entries) {
  return {
    type: RECEIVE,
    entries: entries
  };
}

function deleteDocuments(docIds) {
  return (dispatch) => {
    dispatch({
      type: DELETEDOCUMENTS,
      ids: docIds
    });

    return nuxeo
    .deleteDocuments(docIds)
    .then(() => utils.delay(3000))
    .then(() => {
      return dispatch(refreshList());
    });
  };
}

export default reducer;
export {
  refreshList,
  requestList,
  receiveList,
  deleteDocuments,
};
