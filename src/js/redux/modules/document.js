import nuxeo from 'common/nuxeo/nuxeo';
import utils from 'common/utils/utils';

// action constants
const UNCACHE = 'nhstrust/document/uncache';
const REQUEST = 'nhstrust/document/request';
const RECEIVE = 'nhstrust/document/receive';
const ADDPERMISSIONS = 'nhstrust/document/addpermissions';
const DELPERMISSION = 'nhstrust/document/delpermission';
const TOGGLEFAVOURITE = 'nhstrust/document/togglefavourite';

/**
 * Reducer for the current Document in view
 * @return {object}        transformed state
 */
function reducer(state = {}, action = {}) {
  switch (action.type) {
    case UNCACHE:
      return {
        ...state,
        ready: false,
        query: undefined,
        current: undefined,
      };
    case REQUEST:
      return {
        ...state,
        ready: false,
        query: action.query,
      };
    case RECEIVE:
      return {
        ...state,
        ready: true,
        current: {
          ...action.data,
          contextParameters: {
            ...(state.current && state.current.uid === action.data.uid ? state.current.contextParameters : {}),
            ...action.data.contextParameters
          }
        }
      };
    default:
      return state;
  }
}

function uncacheDocument() {
  return {
    type: UNCACHE
  };
}

function refreshDocument() {
  return (dispatch, getState) => {
    const query = getState().document.query;

    return dispatch(requestDocument(query));
  };
}

/**
 * Shows the document
 * @param {object} docData Document Data
 */
function requestDocument(query) {
  return (dispatch, getState) => {
    dispatch({
      type: REQUEST,
      query: query
    });

    const request = utils.isUuid(query) ? ::nuxeo.getDocumentById : ::nuxeo.getDocumentByUrlPath;
    return request(query)
    .then(response => {
      const currentQuery = getState().document.query;
      if (query === currentQuery) {
        dispatch(receiveDocument(response));
      }
    });
  };
}

function addPermissions(users, level) {
  return (dispatch, getState) => {
    dispatch({
      type: ADDPERMISSIONS,
      data: {
        users,
        level
      }
    });

    const docId = getState().document.current.uid;

    return nuxeo.addMultiplePermissions(docId, users, level)
    .then(responses => {
      const currentDocId = getState().document.current.uid;
      if (currentDocId === docId) {
        // dispatch with the last response, it being the latest one which will contain all added permissions
        dispatch(receiveDocument(responses[responses.length - 1]));
      }
    });
  };
}

function delPermission(user) {
  return (dispatch, getState) => {
    dispatch({
      type: DELPERMISSION,
      data: {
        user,
      }
    });

    const docId = getState().document.current.uid;

    return nuxeo.deletePermission(docId, user)
    .then(response => {
      const currentDocId = getState().document.current.uid;
      if (currentDocId === docId) {
        dispatch(receiveDocument(response));
      }
    });
  };
}

function toggleFavourite(toggle) {
  return (dispatch, getState) => {
    dispatch({
      type: TOGGLEFAVOURITE,
      data: toggle,
    });

    const docId = getState().document.current.uid;
    const method = toggle ? 'addToFavorites' : 'removeFromFavorites';

    return nuxeo[method](docId)
    .then(response => {
      const currentDocId = getState().document.current.uid;
      if (currentDocId === docId) {
        dispatch(receiveDocument(response));
      }
    });
  };
}

/**
 * Shows the document
 * @param {object} docData Document Data
 */
function receiveDocument(document) {
  return {
    type: RECEIVE,
    data: document
  };
}

export default reducer;
export {
  uncacheDocument,
  refreshDocument,
  requestDocument,
  receiveDocument,
  addPermissions,
  delPermission,
  toggleFavourite,
};
