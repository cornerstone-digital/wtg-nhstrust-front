import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from './modules/reducer';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';

function configureStore(initialState) {
  let middleware = [
    thunkMiddleware
  ];

  //Add Your Dev Middleware here (if any)
  if (process.env.NODE_ENV === 'development') {
      middleware = [
        ...middleware,
        createLogger()
      ];
  }

  const finalCreateStore = compose(
    applyMiddleware(...middleware),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )(createStore);
  
  const store = finalCreateStore(rootReducer, initialState);

  //Hot Reload our reducers
  if (module.hot && process.env.NODE_ENV === 'development') {
    module.hot.accept('./modules/reducer', () => {
      const nextRootReducer = require('./modules/reducer');
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
}

export default configureStore;
