#!/usr/bin/env bash

echo "Provision VM START"
echo "=========================================="

sudo apt-get update

#install nodejs
sudo apt-get install -y python-software-properties python g++ make curl
curl -sL https://deb.nodesource.com/setup_5.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo ln -s /usr/bin/nodejs /usr/bin/node
sudo apt-get install -y build-essential

sudo npm cache clean -f
sudo npm install -g n
sudo n stable

#install GIT
sudo apt-get install -y git-all

#install mongo db
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list
sudo apt-get install mongodb-10gen

#install base npm packages
sudo npm install -g bower
sudo npm install -g grunt
sudo npm install -g grunt-cli
sudo npm install -g express
sudo npm install -g yo
sudo npm install -g node-sass
sudo npm install -g sass-loader
sudo npm install -g file-loader

echo ""
echo "=========================================="
echo "Node setup:"
node -v
echo "Provision VM finished"
echo "=========================================="
