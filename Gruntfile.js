'use strict';

var mountFolder = function(connect, dir) {
  return connect.static(require('path').resolve(dir));
};

var webpackDistConfig = require('./webpack.dist.config.js'),
  webpackDevConfig = require('./webpack.config.js');

module.exports = function(grunt) {
  // Let *load-grunt-tasks* require everything
  require('load-grunt-tasks')(grunt);

  // Read configuration from package.json
  var pkgConfig = grunt.file.readJSON('package.json');

  grunt.initConfig({
    pkg: pkgConfig,

    webpack: {
      options: webpackDistConfig,
      dist: {
        cache: false
      }
    },

    'webpack-dev-server': {
      options: {
        hot: true,
        port: 8000,
        webpack: webpackDevConfig,
        publicPath: '/assets/',
        contentBase: './<%= pkg.src %>/',
        watchOptions: {
          aggregateTimeout: 300,
          poll: 1000
        },
      },
      start: {
        keepAlive: true
      }
    },


  removelogging: {
    dist: {
      src: "<%= pkg.src %>/js/**/*.js" // Each file will be overwritten with the output!
    }
  },

    connect: {
      options: {
        port: 8000
      },

      dist: {
        options: {
          keepalive: true,
          middleware: function(connect) {
            return [
              mountFolder(connect, pkgConfig.dist)
            ];
          }
        }
      }
    },

    open: {
      options: {
        delay: 500
      },
      dev: {
        path: 'http://localhost:<%= connect.options.port %>/webpack-dev-server/'
      },
      dist: {
        path: 'http://localhost:<%= connect.options.port %>/'
      }
    },

    karma: {
      unit: {
        configFile: 'karma.conf.js'
      }
    },

    release: {
      options: {
        push: true,
        npm: false,
        pushTags: true,
        afterBump: ['execute:release:call'],
        beforeRelease: ['gitadd:maven']
      }
    },

    gitadd: {
      maven: {
        files: {
          src: [
            '../../../../pom.xml',
            '../../../../nhstrust-front/pom.xml',
            '../../../../nhstrust-deploy/pom.xml',
            '../../../../nhstrust-deploy/marketplace/pom.xml'
          ]
        }
      }
    },

    execute: {
      release: {
        call: function() {
          require('pom-version-changer')
            .process({
              'projects': {
                'nuxeo-nhstrust-intranet-parent': {
                  'pom': '../../../../pom.xml'
                },
                'nhstrust-front': {
                  'pom': '../../../../nhstrust-front/pom.xml',
                  'parent': 'nuxeo-nhstrust-intranet-parent'
                },
                'marketplace-parent': {
                  'pom': '../../../../nhstrust-deploy/pom.xml',
                  'parent': 'nuxeo-nhstrust-intranet-parent'
                },
                'nhstrust-deploy': {
                  'pom': '../../../../nhstrust-deploy/marketplace/pom.xml',
                  'parent': 'marketplace-parent'
                }
              },
              'projects-version': {
                'nuxeo-nhstrust-intranet-parent': grunt.file.readJSON('package.json').version,
                'marketplace-parent': grunt.file.readJSON('package.json').version
              },
            });
        }
      }
    },

    copy: {
      dist: {
        files: [{
          flatten: true,
          expand: true,
          src: ['<%= pkg.src %>/*'],
          dest: '<%= pkg.dist %>/',
          filter: 'isFile'
        },{
          expand: true,
          src: '**',
          cwd: '<%= pkg.src %>/assets/',
          dest: '<%= pkg.dist %>/assets'
        }]
      }
    },

    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '<%= pkg.dist %>'
          ]
        }]
      }
    }
  });

  grunt.registerTask('serve', function(target) {
    if (target === 'dist') {
      return grunt.task.run(['build', 'open:dist', 'connect:dist']);
    }

    grunt.task.run([
      'open:dev',
      'webpack-dev-server'
    ]);
  });

  grunt.registerTask('test', ['karma']);

  grunt.registerTask('build', ['clean', 'copy', 'webpack']);

  grunt.registerTask('default', []);
};
