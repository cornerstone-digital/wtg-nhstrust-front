/*
 * Webpack distribution configuration
 *
 * This file is set up for serving the distribution version. It will be compiled to dist/ by default
 */
var webpack = require('webpack');

module.exports = {

  debug: false,

  devtool: 'sourcemap',

  entry: {
    vendor: [
      'react',
      'react-router',
      'is-ie8'
    ],
    app: [
      './src/js/app.js'
    ]
  },

  output: {
    publicPath: '/assets/',
    path: '../../../target/classes/web/nuxeo.war/nhstrust/assets/',
    filename: 'js/app.js'
  },

  stats: {
    colors: true,
    reasons: false
  },

  plugins: [
    new webpack.optimize.CommonsChunkPlugin('vendor', 'js/vendors.js', Infinity),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.AggressiveMergingPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production'),
      }
    })
  ],

  resolve: {
    extensions: ['', '.js', '.jsx'],
    alias: {
      'styles': __dirname + '/src/assets/styles',
      'mixins': __dirname + '/src/js/mixins',
      'components': __dirname + '/src/js/components/',
      'pages': __dirname + '/src/js/components/pages',
      'common': __dirname + '/src/js/components/common',
      'stores': __dirname + '/src/js/stores/',
      'actions': __dirname + '/src/js/redux/modules/',
      'constants': __dirname + '/src/js/constants/',
      'reducers': __dirname + '/src/js/reducers/',
      'config': __dirname + '/src/js/config/',
      'utils': __dirname + '/src/js/components/common/utils/utils.js',
      'promise-js': __dirname + '/src/js/components/common/utils/promise-js-proxy.js'
    },
    root: 'nhstrust-front/src/main/nhstrust/src/js'
  },

  module: {
    preLoaders: [{
      test: /\.(js|jsx)$/,
      exclude: /node_modules/,
      loader: 'eslint-loader'
    }],
    loaders: [{
      test: /\.(js|jsx)$/,
      exclude: /node_modules/,
      loader: 'babel-loader'
    }, {
      test: /\.(rb.js|rb.jsx)$/,
      loader: 'react-hot!babel-loader'
    },
    {
      test: /\.scss/,
      loader: 'style-loader!css-loader!sass-loader?outputStyle=expanded'
    }, {
      test: /\.css$/,
      loader: 'style-loader!css-loader'
    }, {
      test: /\.(png|jpg|woff|woff2|eot|ttf|svg)$/,
      loader: 'url-loader?limit=8192'
    }]
  }
};
